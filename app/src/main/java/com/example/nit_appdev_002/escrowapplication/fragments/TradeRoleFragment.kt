package com.example.nit_appdev_002.escrowapplication.fragments


import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.trade.CreateTradeActivity
import com.example.nit_appdev_002.escrowapplication.utilities.PREF_ROLE_SELECTED
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.set
import kotlinx.android.synthetic.main.fragment_trade_role.*

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//private const val ARG_PARAM1 = "param1"
//private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class TradeRoleFragment : Fragment(), View.OnClickListener {

    private var mListener:OnTradeRoleListener? = null
//    private var mSelected:OnTradePartySelectedListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_trade_role, container, false)
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//
//    }

    override fun onResume() {
        super.onResume()
        btn_agent_role.setOnClickListener(this)
        btn_seller_role.setOnClickListener(this)
        btn_buyer_role.setOnClickListener(this)

    }

    override fun onPause() {
        super.onPause()
        btn_agent_role.setOnClickListener(null)
        btn_seller_role.setOnClickListener(null)
        btn_buyer_role.setOnClickListener(null)
    }

    override fun onClick(v: View?) {
        val prefs = PreferenceHelper.defaultPrefs(activity!!)
        when (v?.id){
            R.id.btn_agent_role -> {
                if (mListener != null){
                    val string = "Agent"
                    prefs[PREF_ROLE_SELECTED] = string
//                    CreateTradeActivity.tradeMap["role"] = string
                    CreateTradeActivity.createTradeRequest.userRoleName = string
                    mListener?.onNextPressed(this)
                }
            }
            R.id.btn_seller_role -> {
                if (mListener != null){
                    val string = "Seller"
                    prefs[PREF_ROLE_SELECTED] = string
//                    CreateTradeActivity.tradeMap["role"] = string
                    CreateTradeActivity.createTradeRequest.userRoleName = string
                    mListener?.onNextPressed(this)
                }
            }
            R.id.btn_buyer_role -> {
                if (mListener != null){
                    val string = "Buyer"
                    prefs[PREF_ROLE_SELECTED] = string
//                    CreateTradeActivity.tradeMap["role"] = string
                    CreateTradeActivity.createTradeRequest.userRoleName = string
                    mListener?.onNextPressed(this)
                }
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnTradeRoleListener){
            mListener = context
        }else{
            throw RuntimeException("$context must implement OnTradeBeneficiaryListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    interface OnTradeRoleListener {
        fun onNextPressed(fragment: Fragment)

//        fun onTradeRoleSelected(role:String)
    }

}
