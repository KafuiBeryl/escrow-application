package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.google.gson.annotations.SerializedName

data class SentBeneficiary (
        @SerializedName("email")
        var email:String,

        @SerializedName("beneficiary_type")
        var beneficiaryType:String,

        @SerializedName("beneficiary_fee_type")
        var beneficiaryFeeType: String,

        @SerializedName("pre_beneficiary_fee")
        var unCalculatedBeneficiaryFee:Long,

        @SerializedName("beneficiary_fee")
        var beneficiaryFee:Long,

        @SerializedName("beneficiary_payee")
        var beneficiaryPayee:String
)