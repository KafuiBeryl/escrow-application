package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "bank_account_type")
data class BankAccountType (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "bankAccountTypeId")
        @SerializedName("id")
        var bankAccountTypeId:Int,

        @ColumnInfo(name = "account_type")
        @SerializedName("account_type")
        var accountType:String
)