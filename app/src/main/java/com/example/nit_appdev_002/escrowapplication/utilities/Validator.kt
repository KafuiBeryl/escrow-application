package com.example.nit_appdev_002.escrowapplication.utilities

import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.EditText

class Validator {

    companion object {
        @JvmStatic
        fun traverseEditTexts(viewGroup: ViewGroup): EditText?{
            var invalid: EditText? = null
            for (i in 0 until viewGroup.childCount)
            {
                val child = viewGroup.getChildAt(i)
                if (child is EditText){
                    // Whatever logic here to determine if valid.
                    if (TextUtils.isEmpty(child.text)){
                        return child // Stops at first invalid one. But you could add this to a list.
                    }
                }
                else if (child is ViewGroup && child.visibility == View.VISIBLE){
                    invalid = traverseEditTexts(child) // Recursive call.
                    if (invalid != null)
                    {
                        break
                    }
                }
            }

            return invalid
        }
    }
//
//    private fun createCheckbox(): CheckBox {
//        val checkBox = CheckBox(activity)
//        checkBox.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
//        val viewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
//        viewLayoutParams.gravity = Gravity.CENTER_HORIZONTAL
//        viewLayoutParams.bottomMargin = R.dimen.terms_margin_side
//        viewLayoutParams.topMargin = R.dimen.terms_small_margin
//        viewLayoutParams.marginEnd = resources.getDimensionPixelOffset(R.dimen.terms_margin_side)
//        viewLayoutParams.marginStart = resources.getDimensionPixelOffset(R.dimen.terms_margin_side)
//        checkBox.layoutParams = viewLayoutParams
//        val viewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_regular)
//        checkBox.typeface = viewTypeface
//        checkBox.textSize = resources.getDimension(R.dimen.small_text)
//        val colors = intArrayOf(
//                ResourcesCompat.getColor(resources, R.color.colorPrimary, null),
//                ResourcesCompat.getColor(resources, R.color.colorPrimary, null),
//                ResourcesCompat.getColor(resources, R.color.colorPrimary, null),
//                ResourcesCompat.getColor(resources, R.color.colorPrimary, null)
//        )
//        val states = arrayOf(
//                intArrayOf(android.R.attr.state_enabled), // enabled
//                intArrayOf(-android.R.attr.state_enabled), // disabled
//                intArrayOf(-android.R.attr.state_checked), // unchecked
//                intArrayOf(android.R.attr.state_pressed)  // pressed
//        )
//        checkBox.buttonTintList = ColorStateList(states, colors)
//        checkBox.text = resources.getString(R.string.trade_agent_query)
//
//        return checkBox
//    }
}