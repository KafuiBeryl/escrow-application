package com.example.nit_appdev_002.escrowapplication.repositories

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.nit_appdev_002.escrowapplication.dao.TransactionDao
import com.example.nit_appdev_002.escrowapplication.database.EscrowRoomDatabase
import com.example.nit_appdev_002.escrowapplication.entities.*

class TransactionRepository(application: Application) {
    private var mTransactionDao:TransactionDao

    init {
        val db = EscrowRoomDatabase.getDatabase(application)!!
        mTransactionDao = db.transactionDao()
    }

    fun logoutThings(){
        LogoutAsync(mTransactionDao).execute()
    }

    fun nukeExtras(){
        NukeExtrasTablesAsync(mTransactionDao).execute()
    }

    fun insertTrade(trades: List<Trade>){
        InsertTradesAsync(mTransactionDao).execute(trades)
    }

    fun updateTrade(trade: Trade){
        UpdateTradeAsync(mTransactionDao).execute(trade)
    }

    fun insertTradeMileStones(mileStones: List<TradeMileStones>){
        InsertMileStonesAsync(mTransactionDao).execute(mileStones)
    }

    fun updateTradeMileStone(mileStones: TradeMileStones){
        UpdateMileStoneAsync(mTransactionDao).execute(mileStones)
    }

    fun insertTradeNotifications(notificationGang: List<NotificationGang>){
        InsertTradeNotificationsAsync(mTransactionDao).execute(notificationGang)
    }

    fun updateTradeNotification(notificationGang: NotificationGang){
        UpdateTradeNotificationsAsync(mTransactionDao).execute(notificationGang)
    }

    fun insertTradeBeneficiaries(beneficiaries: List<Beneficiary>){
        InsertTradeBeneficiariesAsync(mTransactionDao).execute(beneficiaries)
    }

    fun updateTradeBeneficiaries(beneficiary: Beneficiary){
        UpdateTradeBeneficiariesAsync(mTransactionDao).execute(beneficiary)
    }

    fun insertTradeDeliveryAddress(deliveryAddress: DeliveryAddress){
        InsertTradeDeliveryAddressAsync(mTransactionDao).execute(deliveryAddress)
    }

    fun updateTradeDeliveryAddress(deliveryAddress: DeliveryAddress){
        UpdateTradeDeliveryAddressAsync(mTransactionDao).execute(deliveryAddress)
    }

    fun getSingleTrade(id:Int): LiveData<Trade> {
        return mTransactionDao.getSingleTrade(id)
    }

    fun getTrades(): LiveData<List<Trade>>{
        return mTransactionDao.getTrades()
    }

    fun getTradeMilestones(tradeId:Int):LiveData<List<TradeMileStones>>{
        return mTransactionDao.getTradeMilestones(tradeId)
    }

    fun getTradeNotificationGang(tradeId:Int):LiveData<List<NotificationGang>>{
        return mTransactionDao.getTradeNotificationGang(tradeId)
    }

    fun getTradBeneficiaries(tradeId:Int):LiveData<List<Beneficiary>>{
        return mTransactionDao.getTradBeneficiaries(tradeId)
    }

    fun getTradeDeliveryAddress(tradeId:Int):LiveData<DeliveryAddress>{
        return mTransactionDao.getTradeDeliveryAddress(tradeId)
    }

    class NukeExtrasTablesAsync(dao: TransactionDao) :AsyncTask<Void, Void, Void>(){
        private var mTransactionDao:TransactionDao
        init {
            mTransactionDao = dao
        }
        override fun doInBackground(vararg p0: Void?): Void? {
            mTransactionDao.nukeTradeBeneficiariesTable()
            mTransactionDao.nukeTradeDeliveryAddressTable()
            mTransactionDao.nukeTradeNotificationsTable()
            mTransactionDao.nukeTradeMileStonesTable()
            return null
        }
    }

    class LogoutAsync(dao: TransactionDao) :AsyncTask<Void, Void, Void>(){
        private var mTransactionDao:TransactionDao
        init {
            mTransactionDao = dao
        }
        override fun doInBackground(vararg p0: Void?): Void? {
            mTransactionDao.nukeTradeTable()
            mTransactionDao.nukeTradeBeneficiariesTable()
            mTransactionDao.nukeTradeDeliveryAddressTable()
            mTransactionDao.nukeTradeNotificationsTable()
            mTransactionDao.nukeTradeMileStonesTable()
            return null
        }
    }

    class InsertTradesAsync(dao:TransactionDao) : AsyncTask<List<Trade>, Void, Void>(){
        private var mTransactionDao:TransactionDao
        init {
            mTransactionDao = dao
        }

        override fun doInBackground(vararg params: List<Trade>): Void? {
            for (trade in params[0]){
                mTransactionDao.insertTrade(trade)
            }
            return null
        }
    }

    class UpdateTradeAsync(dao:TransactionDao) : AsyncTask<Trade, Void, Void>(){
        private var mTransactionDao:TransactionDao
        init {
            mTransactionDao = dao
        }

        override fun doInBackground(vararg params: Trade): Void? {
            mTransactionDao.updateTrade(params[0])
            return null
        }
    }

    class InsertMileStonesAsync(dao:TransactionDao) : AsyncTask<List<TradeMileStones>, Void, Void>(){
        private var mTransactionDao:TransactionDao
        init {
            mTransactionDao = dao
        }

        override fun doInBackground(vararg params: List<TradeMileStones>): Void? {
            for (trade in params[0]){
                mTransactionDao.insertMilestone(trade)
            }
            return null
        }
    }

    class UpdateMileStoneAsync(dao:TransactionDao) : AsyncTask<TradeMileStones, Void, Void>(){
        private var mTransactionDao:TransactionDao
        init {
            mTransactionDao = dao
        }

        override fun doInBackground(vararg params: TradeMileStones): Void? {
            mTransactionDao.updateMilestone(params[0])
            return null
        }
    }

    class InsertTradeNotificationsAsync(dao:TransactionDao) : AsyncTask<List<NotificationGang>, Void, Void>(){
        private var mTransactionDao:TransactionDao
        init {
            mTransactionDao = dao
        }

        override fun doInBackground(vararg params: List<NotificationGang>): Void? {
            for (trade in params[0]){
                mTransactionDao.insertTradeNotification(trade)
            }
            return null
        }
    }

    class UpdateTradeNotificationsAsync(dao:TransactionDao) : AsyncTask<NotificationGang, Void, Void>(){
        private var mTransactionDao:TransactionDao
        init {
            mTransactionDao = dao
        }

        override fun doInBackground(vararg params: NotificationGang): Void? {
            mTransactionDao.updateTradeNotification(params[0])
            return null
        }
    }

    class InsertTradeBeneficiariesAsync(dao:TransactionDao) : AsyncTask<List<Beneficiary>, Void, Void>(){
        private var mTransactionDao:TransactionDao
        init {
            mTransactionDao = dao
        }

        override fun doInBackground(vararg params: List<Beneficiary>): Void? {
            for (trade in params[0]){
                mTransactionDao.insertTradeBeneficiaries(trade)
            }
            return null
        }
    }

    class UpdateTradeBeneficiariesAsync(dao:TransactionDao) : AsyncTask<Beneficiary, Void, Void>(){
        private var mTransactionDao:TransactionDao
        init {
            mTransactionDao = dao
        }

        override fun doInBackground(vararg params: Beneficiary): Void? {
            mTransactionDao.updateTradeBeneficiary(params[0])
            return null
        }
    }

    class InsertTradeDeliveryAddressAsync(dao:TransactionDao) : AsyncTask<DeliveryAddress, Void, Void>(){
        private var mTransactionDao:TransactionDao
        init {
            mTransactionDao = dao
        }

        override fun doInBackground(vararg params: DeliveryAddress): Void? {
            mTransactionDao.insertTradeDeliveryAddress(params[0])
            return null
        }
    }

    class UpdateTradeDeliveryAddressAsync(dao:TransactionDao) : AsyncTask<DeliveryAddress, Void, Void>(){
        private var mTransactionDao:TransactionDao
        init {
            mTransactionDao = dao
        }

        override fun doInBackground(vararg params: DeliveryAddress): Void? {
            mTransactionDao.updateTradeDeliveryAddress(params[0])
            return null
        }
    }
}