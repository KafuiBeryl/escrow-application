package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.example.nit_appdev_002.escrowapplication.entities.Trade
import com.example.nit_appdev_002.escrowapplication.entities.UserTradeRoles
import com.google.gson.annotations.SerializedName

data class GetInvolvedTradesResult (
        @SerializedName("user_invited_trades")
        var userInvitedTrades: List<Trade>?,

        @SerializedName("user_trade_roles")
        var userTradeRoles: List<UserTradeRoles>?,

        @SerializedName("error")
        var error:String?
)