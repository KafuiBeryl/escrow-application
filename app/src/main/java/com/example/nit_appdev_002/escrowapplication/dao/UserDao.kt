package com.example.nit_appdev_002.escrowapplication.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Update
import com.example.nit_appdev_002.escrowapplication.entities.User

@Dao
interface UserDao {
    @Insert(onConflict = REPLACE)
    fun insert(user:User)

    @Update
    fun updateUser(user:User)

    @Query("update user set image_path = :imagePath where id = :uid")
    fun addUserImage(imagePath:String, uid: Int)

    @Query("select * from user where id = :id")
    fun getUser(id:Int):LiveData<User>

    @Query("select image_path from user where id = :id")
    fun getUserImage(id:Int):LiveData<String>

    @Query("select email from user where id = :id")
    fun getUserEmail(id:Int): LiveData<String>

    @Query("delete from user")
    fun nukeTable()
}