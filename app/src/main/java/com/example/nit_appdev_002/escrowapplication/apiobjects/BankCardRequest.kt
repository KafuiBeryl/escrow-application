package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.google.gson.annotations.SerializedName

data class BankCardRequest (
        @SerializedName("trade_id")
        var tradeId:String,

        @SerializedName("description")
        var description:String,

        @SerializedName("card_number")
        var cardNumber:String,

        @SerializedName("card_type")
        var cardType:String,

        @SerializedName("cvv")
        var cvvNumber:String,

        @SerializedName("exp_month")
        var expiryMonth:String,

        @SerializedName("exp_year")
        var expiryYear:String
)