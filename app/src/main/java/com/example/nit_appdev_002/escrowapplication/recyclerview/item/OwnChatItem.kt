package com.example.nit_appdev_002.escrowapplication.recyclerview.item

import android.content.Context
import android.net.Uri
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.entities.Message
import com.example.nit_appdev_002.escrowapplication.utilities.DateUtils
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_own_message.*

class OwnChatItem(val message: Message,
                  val context: Context): Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.txtMyMessage.text = message.message
        viewHolder.txtMyMessageTime.text = DateUtils.fromMillisToTimeString(message.time)
        if (message.imagePath != null){
            Glide.with(context)
                    .load(Uri.parse(message.imagePath))
                    .apply(RequestOptions()
                            .fitCenter()
                            .placeholder(R.drawable.ic_image_black_24dp))
                    .into(viewHolder.imageMyMessage)
        }
    }

    override fun getLayout(): Int {
        return R.layout.item_own_message
    }
}