package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.google.gson.annotations.SerializedName

data class PersonalDetailsRequest (
        @SerializedName("first_name")
        var firstName:String,

        @SerializedName("last_name")
        var lastName:String,

        @SerializedName("mobile_dialing_code")
        var mobileDialingCode:String,

        @SerializedName("mobile_number")
        var mobileNumber:String,

        @SerializedName("home_dialing_code")
        var homeDialingCode:String,

        @SerializedName("home_number")
        var homeNumber:String,

        @SerializedName("identification")
        var identityType:String,

        @SerializedName("id_number")
        var identityNumber:String,

//        @SerializedName("device_id")
//        var deviceId:String,
//
        @SerializedName("citizenship")
        var citizenship:String,

        @SerializedName("occupation")
        var occupation:String
)