package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "trade_process")
data class TradeProcess (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "tradeProcessId")
        @SerializedName("id")
        var tradeProcessId:Int,

        @ColumnInfo(name = "step_number")
        @SerializedName("step_number")
        var stepNumber:Int,

        @ColumnInfo(name = "process_name")
        @SerializedName("process_name")
        var processName:String
)