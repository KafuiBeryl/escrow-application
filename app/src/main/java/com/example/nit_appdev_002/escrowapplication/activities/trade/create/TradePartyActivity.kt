package com.example.nit_appdev_002.escrowapplication.activities.trade.create

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.ViewStub
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.utilities.INTENT_TRADE_PARTY
import kotlinx.android.synthetic.main.activity_trade_party.*
import kotlinx.android.synthetic.main.partial_buyer_role.*
import kotlinx.android.synthetic.main.partial_seller_role.*
import kotlinx.android.synthetic.main.toolbar.*

class TradePartyActivity : AppCompatActivity() {

    companion object {
        @JvmStatic
        private var isAgentAdded:Boolean = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trade_party)
        setSupportActionBar(custom_toolbar)

        //get the intent extra
        if (savedInstanceState == null){
            val roleString = intent.getStringExtra(INTENT_TRADE_PARTY)
            if (!TextUtils.isEmpty(roleString)){
                setRoleLayout(roleString)
            }
        }else{
            val role = savedInstanceState.getString(INTENT_TRADE_PARTY)
            setRoleLayout(role!!)
        }

        btn_back.setOnClickListener {
            isAgentAdded = false
            finish()
        }

        btn_next.setOnClickListener {
            startActivity(Intent(this, TradeDescriptionActivity::class.java))
        }
    }

    private fun setRoleLayout(role:String){
        when (role){
            "Agent" -> {
                //set is agent added to true
                isAgentAdded = true
                checkbox_agent_added.visibility = View.GONE
                //set the partial view
                val stub = ViewStub(this)
                val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                stub.layoutParams = secondLayoutParams
                stub.layoutResource = R.layout.partial_agent_role
                trade_role_linear_layout.addView(stub)
                stub.inflate()
            }

            "Buyer" -> {
                //set the partial view
                val stub = ViewStub(this)
                val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                stub.layoutParams = secondLayoutParams
                stub.layoutResource = R.layout.partial_buyer_role
                trade_role_linear_layout.addView(stub)
                stub.inflate()

                 checkbox_agent_added.setOnCheckedChangeListener { _, isChecked ->
                    when (isChecked){
                        true -> {
                            isAgentAdded = true
                            val agentStub = ViewStub(this)
                            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                            agentStub.layoutParams = layoutParams
                            agentStub.layoutResource = R.layout.partial_agent_added
                            buyer_agent_added_linear_layout.addView(agentStub)
                            agentStub.inflate()
                        }
                        false -> {
                            isAgentAdded = false
                            buyer_agent_added_linear_layout.removeAllViews()
                        }
                    }
                }
            }

            "Seller" -> {
                //set the partial view
                val stub = ViewStub(this)
                val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                stub.layoutParams = secondLayoutParams
                stub.layoutResource = R.layout.partial_seller_role
                trade_role_linear_layout.addView(stub)
                stub.inflate()

                checkbox_agent_added.setOnCheckedChangeListener { _, isChecked ->
                when (isChecked){
                    true -> {
                        isAgentAdded = true
                        val agentStub = ViewStub(this)
                        val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                        agentStub.layoutParams = layoutParams
                        agentStub.layoutResource = R.layout.partial_agent_added
                        seller_agent_added_linear_layout.addView(agentStub)
                        agentStub.inflate()
                    }
                    false -> {
                        isAgentAdded = false
                        seller_agent_added_linear_layout.removeAllViews()
                    }
                }
            }
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        if (intent != null){
            setIntent(intent)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
