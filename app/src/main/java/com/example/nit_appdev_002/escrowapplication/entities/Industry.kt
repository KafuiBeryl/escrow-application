package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "industry")
data class Industry(
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "industry_id")
        @SerializedName("id")
        var industryId:Int,

        @ColumnInfo(name = "industry_classification")
        @SerializedName("industry_classification")
        var classification:String
)