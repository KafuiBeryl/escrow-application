package com.example.nit_appdev_002.escrowapplication.utilities

import android.content.Context
import android.os.Handler
import android.os.Looper
import com.example.nit_appdev_002.escrowapplication.listeners.IImageTaskListener
import java.io.IOException

class ImageCompressTask : Runnable {

    private val mContext: Context
    private val imagePath:String
    private var imageString:String? = null
    private val mIImageTaskListener:IImageTaskListener
    private val mHandler = Handler(Looper.getMainLooper())

    constructor(context:Context, path: String, iImageTaskListener: IImageTaskListener){
        mContext = context
        imagePath = path
        mIImageTaskListener = iImageTaskListener
    }

    override fun run() {
        try {
            //get compressed file from ImageUtil
            imageString = ImageUtil.compressImage(imagePath, mContext)
            //use handler to post the result back to the main thread
            val runnable = Runnable {
                mIImageTaskListener.onComplete(imageString!!)
            }
            mHandler.post(runnable)
        }catch (ex:IOException){
            val runnable = Runnable{
                mIImageTaskListener.onError(ex)
            }
            mHandler.post(runnable)
        }
    }
}