package com.example.nit_appdev_002.escrowapplication.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.nit_appdev_002.escrowapplication.entities.User
import com.example.nit_appdev_002.escrowapplication.entities.UserTradeRoles
import com.example.nit_appdev_002.escrowapplication.miscobjects.InsertUserImage
import com.example.nit_appdev_002.escrowapplication.repositories.MiscRepository
import com.example.nit_appdev_002.escrowapplication.repositories.TransactionRepository
import com.example.nit_appdev_002.escrowapplication.repositories.UserRepository

class UserViewModel(application: Application) : AndroidViewModel(application) {
    private var mRepository:UserRepository
    private var mTradeRepository:TransactionRepository
    private var mMiscRepository:MiscRepository
//    private var mAllUserTradeRoles:LiveData<List<UserTradeRoles>>

    init {
        mRepository = UserRepository(application)
        mTradeRepository = TransactionRepository(application)
        mMiscRepository = MiscRepository(application)
//        mAllUserTradeRoles = mRepository.getAllUserTradeRoles()
    }

    fun logoutThings(){
        mRepository.logoutThings()
        mTradeRepository.logoutThings()
        mMiscRepository.logOut()
    }

    fun countUserTradeRoles(): LiveData<Int> {
        return mRepository.countUserTradeRoles()
    }

    fun getAllUserTradeRoles():LiveData<List<UserTradeRoles>>{
        return mRepository.getAllUserTradeRoles()
    }

    fun getUser(id:Int):LiveData<User>{
        return mRepository.getUser(id)
    }

    fun getUserImage(id: Int):LiveData<String>{
        return mRepository.getUserImage(id)
    }

    fun getUserEmail(id: Int):LiveData<String>{
        return mRepository.getUserEmail(id)
    }

    fun updateUser(user:User?){
        if (user != null){
            mRepository.updateUser(user)
        }
    }

    fun addUserImage(insertUserImage: InsertUserImage){
        mRepository.addUserImage(insertUserImage)
    }

    fun insertUser(user:User?){
        if (user != null){
            mRepository.insertUser(user)
        }
    }

    fun insertUserTradeRoles(userTradeRoles:List<UserTradeRoles>?){
        if (userTradeRoles != null){
            mRepository.insertUserRole(userTradeRoles)
        }
    }
}