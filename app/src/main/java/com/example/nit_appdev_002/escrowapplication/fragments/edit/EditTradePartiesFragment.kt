package com.example.nit_appdev_002.escrowapplication.fragments.edit

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import android.widget.LinearLayout
import android.widget.Toast

import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.trade.view.EditTradeActivity
import com.example.nit_appdev_002.escrowapplication.entities.Trade
import com.example.nit_appdev_002.escrowapplication.utilities.PREF_HAS_AGENT
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.set
import com.example.nit_appdev_002.escrowapplication.utilities.Validator
import kotlinx.android.synthetic.main.fragment_edit_trade_parties.*
import kotlinx.android.synthetic.main.partial_agent_added.*
import kotlinx.android.synthetic.main.partial_agent_role.*
import kotlinx.android.synthetic.main.partial_buyer_role.*
import kotlinx.android.synthetic.main.partial_seller_role.*


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [EditTradePartiesFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 *
 */
class EditTradePartiesFragment : Fragment(), View.OnClickListener, EditTradeDescriptionFragment.OnFragmentInteractionListener {
    companion object {
        @JvmStatic
        private var selectedRole:String? = null

        @JvmStatic
        private var isAgentAdded:Boolean = false

        @JvmStatic
        private var isBackPressed:Boolean = false
    }
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_trade_parties, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectedRole = EditTradeActivity.selectedTrade?.userRole?.role

        checkbox_agent_added.setOnCheckedChangeListener { _, isChecked ->
            when (isChecked){
                true -> {
                    when (selectedRole){
                        "Buyer" -> {
                            isAgentAdded = true
                            val agentStub = ViewStub(activity)
                            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                            agentStub.layoutParams = layoutParams
                            agentStub.layoutResource = R.layout.partial_agent_added
                            buyer_agent_added_linear_layout.addView(agentStub)
                            agentStub.inflate()
                        }

                        "Seller" -> {
                            isAgentAdded = true
                            val agentStub = ViewStub(activity)
                            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                            agentStub.layoutParams = layoutParams
                            agentStub.layoutResource = R.layout.partial_agent_added
                            seller_agent_added_linear_layout.addView(agentStub)
                            agentStub.inflate()
                        }
                    }
                }
                false -> {
                    when (selectedRole){
                        "Buyer" -> {
                            isAgentAdded = false
                            buyer_agent_added_linear_layout.removeAllViews()
                        }

                        "Seller" -> {
                            isAgentAdded = false
                            seller_agent_added_linear_layout.removeAllViews()
                        }
                    }
                }
            }
        }

        if (!isBackPressed){
            when (selectedRole){
                "Agent" -> {
                    //set is agent added to true
                    isAgentAdded = true
                    checkbox_agent_added.visibility = View.GONE
                    //set the partial view
                    val stub = ViewStub(activity)
                    val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    stub.layoutParams = secondLayoutParams
                    stub.layoutResource = R.layout.partial_agent_role
                    trade_role_linear_layout.addView(stub)
                    stub.inflate()
                }

                "Buyer" -> {
                    //set the partial view
//                            checkbox_agent_added.isChecked = false

                    val stub = ViewStub(activity)
                    val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    stub.layoutParams = secondLayoutParams
                    stub.layoutResource = R.layout.partial_buyer_role
                    trade_role_linear_layout.addView(stub)
                    stub.inflate()
                }

                "Seller" -> {
//                            checkbox_agent_added.isChecked = false

                    //set the partial view
                    val stub = ViewStub(activity)
                    val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    stub.layoutParams = secondLayoutParams
                    stub.layoutResource = R.layout.partial_seller_role
                    trade_role_linear_layout.addView(stub)
                    stub.inflate()
                }
            }
            setOldValues()
        }else{
            setNewValues()
        }
    }

    private fun setOldValues(){
        isAgentAdded = EditTradeActivity.selectedTrade?.agentEmail != null
        val addedAlready: Trade? = EditTradeActivity.selectedTrade
        when(selectedRole){
            "Agent" -> {
                seller_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready?.sellerEmail ?: "")
                seller_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready?.sellerEmail ?: "")
                buyer_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready?.buyerEmail ?: "")
                buyer_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready?.buyerEmail ?: "")
            }
            "Buyer" -> {
                buyer_seller_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready?.sellerEmail ?: "")
                buyer_seller_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready?.sellerEmail ?: "")
                if (isAgentAdded){
                    checkbox_agent_added.isChecked = true
                    agent_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready?.agentEmail ?: "")
                    agent_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready?.agentEmail ?: "")
                }
            }
            "Seller" -> {
                seller_buyer_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready?.buyerEmail ?: "")
                seller_buyer_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready?.buyerEmail ?: "")
                if (isAgentAdded){
                    checkbox_agent_added.isChecked = true
                    agent_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready?.agentEmail ?: "")
                    agent_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready?.agentEmail ?: "")
                }
            }
        }
    }

    private fun setNewValues(){
        isAgentAdded = EditTradeActivity.editTradeRequest.agentEmail != null
        val addedAlready = EditTradeActivity.editTradeRequest
        when(selectedRole){
            "Agent" -> {
                seller_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.sellerEmail)
                seller_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.sellerEmail)
                buyer_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.buyerEmail)
                buyer_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.buyerEmail)
            }
            "Buyer" -> {
                buyer_seller_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.sellerEmail)
                buyer_seller_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.sellerEmail)
                if (isAgentAdded){
                    checkbox_agent_added.isChecked = true
                    agent_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.agentEmail)
                    agent_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.agentEmail)
                }
            }
            "Seller" -> {
                seller_buyer_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.buyerEmail ?: "")
                seller_buyer_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.buyerEmail ?: "")
                if (isAgentAdded){
                    checkbox_agent_added.isChecked = true
                    agent_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.agentEmail ?: "")
                    agent_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.agentEmail ?: "")
                }
            }
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser){
            if (selectedRole != null){

            }
        }
    }

    private fun validateTheEditTexts():Boolean{
        var validate = false
        val emptyText = Validator.traverseEditTexts(trade_role_linear_layout)
        if (emptyText == null){
            when (selectedRole){
                "Agent" -> {
                    if (seller_email_txt.text.trim().toString().equals(seller_confirm_email_txt.text.trim().toString(), false)
                            && buyer_email_txt.text.toString().contentEquals(buyer_confirm_email_txt.text.toString())){
                        validate = true
                    }
                }
                "Buyer" -> {
                    if (buyer_seller_email_txt.text.toString().contentEquals(buyer_seller_confirm_email_txt.text.toString())){
                        when (isAgentAdded){
                            true -> {
                                if (agent_email_txt.text.toString().contentEquals(agent_confirm_email_txt.text.toString())){
                                    validate = true
                                }
                            }
                            false -> {
                                validate = true
                            }
                        }
                    }
                }
                "Seller" -> {
                    if (seller_buyer_email_txt.text.toString().contentEquals(seller_buyer_confirm_email_txt.text.toString())){
                        when (isAgentAdded){
                            true -> {
                                if (agent_email_txt.text.toString().contentEquals(agent_confirm_email_txt.text.toString())){
                                    validate = true
                                }
                            }
                            false -> {
                                validate = true
                            }
                        }
                    }
                }
            }
        }

        return validate
    }

    private fun passEntriesToVariable(){
        var sellerEmail = ""
        var buyerEmail = ""
        var agentEmail = ""

        when(selectedRole){
            "Agent" -> {
                sellerEmail = seller_email_txt.text.toString()
                buyerEmail = buyer_email_txt.text.toString()
                agentEmail = EditTradeActivity.selectedTrade?.agentEmail!!
            }
            "Buyer" -> {
                sellerEmail = buyer_seller_email_txt.text.toString()
                buyerEmail = EditTradeActivity.selectedTrade?.buyerEmail!!
                if (isAgentAdded){
                    agentEmail = agent_email_txt.text.toString()
                }
            }
            "Seller" -> {
                sellerEmail = EditTradeActivity.selectedTrade?.sellerEmail!!
                buyerEmail = seller_buyer_email_txt.text.toString()
                if (isAgentAdded){
                    agentEmail = agent_email_txt.text.toString()
                }
            }
        }
        EditTradeActivity.editTradeRequest.buyerEmail = buyerEmail
        EditTradeActivity.editTradeRequest.sellerEmail = sellerEmail
        EditTradeActivity.editTradeRequest.agentEmail = agentEmail
    }

    override fun onResume() {
        super.onResume()
        btn_next.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btn_next.setOnClickListener(null)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onClick(v: View?) {
        when (v?.id){
            R.id.btn_next -> {
                if (listener != null && selectedRole != null){
                    //set preferences
                    val prefs = PreferenceHelper.defaultPrefs(activity!!.applicationContext)
                    val boolean = isAgentAdded
                    prefs[PREF_HAS_AGENT] = boolean
                    if (validateTheEditTexts()){
                        passEntriesToVariable()
                        listener?.onNextPressed(this)
                    }else{
                        Toast.makeText(activity, R.string.trade_empty_text, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    override fun onNextPressed(fragment: Fragment) {
        TODO("not necessary")
    }

    override fun onBackButtonPressed(fragment: Fragment) {
        when (fragment) {
            is EditTradeDescriptionFragment -> {
                isBackPressed = true
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        fun onNextPressed(fragment: Fragment)
    }

}
