package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

@Entity(tableName = "trade")
data class Trade (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "id")
        @SerializedName("id")
        var id:Int?,

        @ColumnInfo(name = "user_id")
        @SerializedName("user_id")
        var userId:Int?,

//        @ColumnInfo(name = "user_role")
        @SerializedName("user_trade_role")
        @Embedded
        var userRole:TradeRole?,

        @ColumnInfo(name = "seller_email")
        @SerializedName("seller_email")
        var sellerEmail:String?,

        @ColumnInfo(name = "buyer_email")
        @SerializedName("buyer_email")
        var buyerEmail:String?,

        @ColumnInfo(name = "trade_name")
        @SerializedName("trade_name")
        var tradeName:String?,

        @ColumnInfo(name = "description")
        @SerializedName("description")
        var description:String?,

        @ColumnInfo(name = "trade_amount")
        @SerializedName("trade_amount")
        var tradeAmount:Long?,

        @ColumnInfo(name = "delivery_time")
        @SerializedName("delivery_time")
        var deliveryTime:String?,

        @ColumnInfo(name = "inspection_time")
        @SerializedName("inspection_time")
        var inspectionTime:String?,

        @ColumnInfo(name = "isDelivery")
        @SerializedName("isDelivery")
        var isDelivery:Boolean?,

        @ColumnInfo(name = "is_progress_payment")
        @SerializedName("is_progress_payment")
        var isProgress:Boolean?,

        @ColumnInfo(name = "hasBeneficiary")
        @SerializedName("hasBeneficiary")
        var hasBeneficiary:Boolean?,

        @ColumnInfo(name = "isConfidential")
        @SerializedName("isConfidential")
        var isConfidential:Boolean?,

        @ColumnInfo(name = "hasDocuments")
        @SerializedName("hasDocuments")
        var hasDocuments:Boolean?,

        @ColumnInfo(name = "hasNotifications")
        @SerializedName("hasNotifications")
        var hasNotifications:Boolean?,

        @ColumnInfo(name = "escrow_fee")
        @SerializedName("escrow_fee")
        var escrowFee:Long?,

//        @ColumnInfo(name = "escrow_fee_allocation")
        @SerializedName("admin_payee_allocation")
        @Embedded
        var escrowFeeAllocation:AdminFeeAllocation?,

        @ColumnInfo(name = "agent_email")
        @SerializedName("agent_email")
        var agentEmail:String?,

//        @ColumnInfo(name = "company_type")
        @SerializedName("fee_type")
        @Embedded
        var agentFeeType:FeeType?,

//        @ColumnInfo(name = "company_type")
        @SerializedName("standard_payee_allocation")
        @Embedded
        var agentFeeAllocation:StandardFeeAllocation?,

        @ColumnInfo(name = "pre_agent_fee")
        @SerializedName("pre_agent_fee")
        var unCalculatedAgentFee:Long?,

        @ColumnInfo(name = "agent_fee")
        @SerializedName("agent_fee")
        var agentFee:Long?,

        @ColumnInfo(name = "buyer_fee")
        @SerializedName("buyer_fee")
        var buyerFee:Long?,

        @ColumnInfo(name = "seller_fee")
        @SerializedName("seller_fee")
        var sellerFee:Long?,

//        @ColumnInfo(name = "bill_address_city")
        @SerializedName("country")
        @Embedded
        var currency:Country?,

//        @ColumnInfo(name = "bill_address_region")
        @SerializedName("trade_status")
        @Embedded
        var tradeStatus:TradeStatus?,

        @ColumnInfo(name = "created_at")
        @SerializedName("created_at")
        var createdDate: Date,

        @ColumnInfo(name = "updated_at")
        @SerializedName("updated_at")
        var updatedDate: Date,

//        @ColumnInfo(name = "phone_verified")
        @SerializedName("trade_step")
        @Embedded
        var tradeProcessStep: TradeProcess?,

//        @ColumnInfo(name = "tradeIndustry")
        @SerializedName("trade_industry")
        @Embedded
        var tradeIndustry:Industry?
): Serializable