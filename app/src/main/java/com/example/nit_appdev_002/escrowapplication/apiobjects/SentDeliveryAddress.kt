package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.google.gson.annotations.SerializedName

data class SentDeliveryAddress (
        @SerializedName("house_number")
        var houseNumber:String,

        @SerializedName("street_line_1")
        var streetLine1:String,

        @SerializedName("street_line_2")
        var streetLine2: String,

        @SerializedName("suburb")
        var suburb:String,

        @SerializedName("town_or_city")
        var townOrCity:String,

        @SerializedName("country_name")
        var countryName:String,

        @SerializedName("digital_address")
        var digitalAddress:String,

        @SerializedName("comments")
        var comments:String
)