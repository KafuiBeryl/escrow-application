package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.example.nit_appdev_002.escrowapplication.entities.Trade
import com.google.gson.annotations.SerializedName

data class InvitationResult (
        @SerializedName("trade")
        var trade: Trade,

        @SerializedName("unfortunately")
        var unfortunatelyText:String?,

        @SerializedName("success")
        var successText:String,

        @SerializedName("error")
        var error:String?
)