package com.example.nit_appdev_002.escrowapplication.recyclerview.item

import android.content.Context
import android.net.Uri
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.entities.Message
import com.example.nit_appdev_002.escrowapplication.utilities.DateUtils
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_other_message.*

class OtherChatItem(val message:Message,
                    val context: Context): Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.txtOtherUser.text = message.user
        viewHolder.txtOtherMessageTime.text = DateUtils.fromMillisToTimeString(message.time)
        viewHolder.txtOtherMessage.text = message.message
        if (message.imagePath != null){
            Glide.with(context)
                    .load(Uri.parse(message.imagePath))
                    .apply(RequestOptions()
                            .fitCenter()
                            .placeholder(R.drawable.ic_image_black_24dp))
                    .into(viewHolder.imageOtherMessage)
        }
    }

    override fun getLayout() = R.layout.item_other_message
}