package com.example.nit_appdev_002.escrowapplication.activities.home

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.nit_appdev_002.escrowapplication.R
import kotlinx.android.synthetic.main.activity_start.*

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        start_btn.setOnClickListener {
            startActivity(Intent(this, TermsActivity::class.java))
            finish()
        }
    }
}
