package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "trade_milestones")
data class TradeMileStones (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "id")
        @SerializedName("id")
        var id:Int?,

        @ColumnInfo(name = "trade_id")
        @SerializedName("trade_id")
        var tradeId:Int?,

        @ColumnInfo(name = "milestone_name")
        @SerializedName("milestone_name")
        var milestoneName:String,

        @ColumnInfo(name = "description")
        @SerializedName("description")
        var description:String,

        //how much the the milestone costs in total
        @ColumnInfo(name = "gross_amount")
        @SerializedName("gross_amount")
        var grossAmount: Long,

        @ColumnInfo(name = "delivery_time")
        @SerializedName("delivery_time")
        var deliveryTime:String,

        @ColumnInfo(name = "inspection_period")
        @SerializedName("inspection_period")
        var inspectionPeriod:String,

        //how much the the trade costs in total
        @ColumnInfo(name = "running_total")
        @SerializedName("running_total")
        var runningTotal:Long,

        //how much the seller is owes. In terms of admin fees and agent fees
        @ColumnInfo(name = "seller_fees")
        @SerializedName("seller_fees")
        var sellerFees:Long,

        //how much the seller gets after being deducted in this particular milestone
        @ColumnInfo(name = "payable_to_seller")
        @SerializedName("payable_to_seller")
        var payableToSeller:Long,

        //how much the seller gets after being deducted in total. taken from
        // all milestones applicable to this trade
        @ColumnInfo(name = "cumulative_payable")
        @SerializedName("cumulative_payable")
        var cumulativePayable:Long,

        @ColumnInfo(name = "created_at")
        @SerializedName("created_at")
        var createdDate: Date?,

        @ColumnInfo(name = "updated_at")
        @SerializedName("updated_at")
        var updatedDate: Date?
)