package com.example.nit_appdev_002.escrowapplication.activities.trade

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import com.bhargavms.podslider.PodSlider
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.adapters.TradePagerAdapter
import com.example.nit_appdev_002.escrowapplication.apiobjects.CreateTradeRequest
import com.example.nit_appdev_002.escrowapplication.apiobjects.SentBeneficiary
import com.example.nit_appdev_002.escrowapplication.entities.NotificationGang
import com.example.nit_appdev_002.escrowapplication.entities.TradeMileStones
import com.example.nit_appdev_002.escrowapplication.fragments.*
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import kotlinx.android.synthetic.main.activity_create_trade.*
import kotlinx.android.synthetic.main.content_create_trade.*
import kotlinx.android.synthetic.main.toolbar.*
import android.graphics.drawable.Drawable
import android.util.Base64
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.nit_appdev_002.escrowapplication.activities.authentication.LoginActivity
import com.example.nit_appdev_002.escrowapplication.utilities.*
import com.example.nit_appdev_002.escrowapplication.viewmodels.UserViewModel
import com.github.omadahealth.lollipin.lib.PinCompatActivity
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.nav_header_home.*
import kotlinx.android.synthetic.main.please_wait.*


class CreateTradeActivity : PinCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
        TradeRoleFragment.OnTradeRoleListener, TradePartyFragment.OnTradePartyListener,
        TradeDescriptionFragment.OnTradeDescriptionListener, TradeBeneficiaryFragment.OnTradeBeneficiaryListener,
        TradeNotificationFragment.OnTradeNotificationListener, TradeProgressFragment.OnTradeProgressListener,
        TradeDeliveryFragment.OnTradeDeliveryListener {


    companion object {
        @JvmStatic
        private lateinit var mUserViewModel: UserViewModel

        @JvmStatic
        val createTradeRequest = CreateTradeRequest(null, null,
                null, null, null, 0,
                null, null, false, false,
                false, false, false, false,
                0, null, null, null,
                null, 0, 0, 0, 0, "GHS",
                null)

        @JvmStatic
        val beneficiaries = ArrayList<SentBeneficiary>()

        @JvmStatic
        val mileStones = ArrayList<TradeMileStones>()

        @JvmStatic
        val notificationGang = ArrayList<NotificationGang>()
    }

    private val adapter = TradePagerAdapter(supportFragmentManager)
    private var id:Int? = null
    private var prefs:SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_trade)
        setSupportActionBar(custom_toolbar)

        //initialize the view model
        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        prefs = PreferenceHelper.defaultPrefs(this)
        id = prefs!![PREF_USER_ID]

        val toggle = ActionBarDrawerToggle(
                this, create_trade_drawer_layout, custom_toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        create_trade_drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        //hide please wait
        please_wait_group.visibility = View.GONE


        //set viewpager to podslider
        setSliderToViewPager()

        //make the podslider unclickable
        pod_slider.setPodClickListener {

        }
//        btn_agent_role.setOnClickListener {
//            val string = "Agent"
//            val intent = Intent(this, TradePartyActivity::class.java)
//            intent.putExtra(INTENT_TRADE_PARTY, string)
//            startActivity(intent)
//        }
//
//        btn_seller_role.setOnClickListener {
//            val string = "Seller"
//            val intent = Intent(this, TradePartyActivity::class.java)
//            intent.putExtra(INTENT_TRADE_PARTY, string)
//            startActivity(intent)
//        }
//
//        btn_buyer_role.setOnClickListener {
//            val string = "Buyer"
//            val intent = Intent(this, TradePartyActivity::class.java)
//            intent.putExtra(INTENT_TRADE_PARTY, string)
//            startActivity(intent)
//        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        setDrawerValues()
    }

    private fun setDrawerValues(){
        mUserViewModel.getUser(id!!).observe(this, Observer {
            user->
            if (user != null){
                val string = "${user.firstName} ${user.lastName}"
                if (user.imagePath != null){
                    Glide.with(this)
                            .load(Base64.decode(user.imagePath, Base64.URL_SAFE))
                            .apply(RequestOptions()
                                    .fitCenter()
                                    .placeholder(R.drawable.profile_placeholder))
                            .into(drawer_imageView)
                }
                drawer_main_textView.text = string
                drawer_sub_textView.text = user.email
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == PIN_FAILED || resultCode == PIN_FORGOT){
            mUserViewModel.logoutThings()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    private fun setSliderToViewPager(){
        //set total number of pods
        pod_slider.setNumberOfPods(7)
        val person:Drawable = makeImage(R.drawable.add_person_icon)
        val beneficiary:Drawable = makeImage(R.drawable.beneficiaries_icon)
        val briefcase:Drawable = makeImage(R.drawable.briefcase_icon)
        val delivery:Drawable =  makeImage(R.drawable.delivery_icon)
        val tradeDescription:Drawable = makeImage(R.drawable.trade_description_icon)
        val notificationIcon:Drawable = makeImage(R.drawable.notification_icon)
        val progressIcon:Drawable = makeImage(R.drawable.progress_icon)

        val drawables = arrayOf<Drawable>(briefcase, person, tradeDescription,
                progressIcon, beneficiary, notificationIcon, delivery)

        //set it up with the viewpager
        create_trade_view_pager.adapter = adapter
        pod_slider.setUpWithViewPager(create_trade_view_pager)
        //set the drawables to the podslider
        pod_slider.setPodDrawables(drawables, PodSlider.DrawableSize.FIT_LARGE_CIRCLE)
    }

    private fun makeImage(id: Int):Drawable{
        val drawable = ResourcesCompat.getDrawable(resources, id, null)!!
        drawable.setTint(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        return drawable
    }

    override fun onNextPressed(fragment: Fragment) {
        when (fragment){
            is TradeRoleFragment -> {
                create_trade_view_pager.setCurrentItem(1, true)
            }
            is TradePartyFragment -> {
                create_trade_view_pager.setCurrentItem(2, true)
            }
            is TradeDescriptionFragment -> {
//                val prefs = PreferenceHelper.defaultPrefs(this)
                val isProcess:Boolean? = prefs!![PREF_IS_PROCESS_PAYMENT]
                if (isProcess!!){
                    create_trade_view_pager.setCurrentItem(3, true)
                }else{
                    create_trade_view_pager.setCurrentItem(4, true)
                }
            }
            is TradeProgressFragment -> {
                create_trade_view_pager.setCurrentItem(4, true)
            }
            is TradeBeneficiaryFragment -> {
                create_trade_view_pager.setCurrentItem(5, true)
            }
            is TradeNotificationFragment -> {
                create_trade_view_pager.setCurrentItem(6, true)
            }
        }
    }

    override fun onBackButtonPressed(fragment: Fragment) {
        when (fragment){
            is TradePartyFragment -> {
                finish()
            }
            is TradeDescriptionFragment -> {
                adapter.onBackButtonPressed(TradeDescriptionFragment())
                create_trade_view_pager.setCurrentItem(1, true)
            }
            is TradeProgressFragment -> {
                adapter.onBackButtonPressed(TradeProgressFragment())
                create_trade_view_pager.setCurrentItem(2, true)
            }
            is TradeBeneficiaryFragment -> {
//                val prefs = PreferenceHelper.defaultPrefs(this)
                val isProcess:Boolean? = prefs!![PREF_IS_PROCESS_PAYMENT]
                if (isProcess!!){
                    create_trade_view_pager.setCurrentItem(3, true)
                }else{
                    adapter.onBackButtonPressed(TradeBeneficiaryFragment())
                    create_trade_view_pager.setCurrentItem(2, true)
                }
            }
            is TradeNotificationFragment -> {
                create_trade_view_pager.setCurrentItem(4, true)
            }
            is TradeDeliveryFragment -> {
                create_trade_view_pager.setCurrentItem(5, true)
            }
        }
    }

    override fun onBackPressed() {
        if (create_trade_drawer_layout.isDrawerOpen(GravityCompat.START)) {
            create_trade_drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                // Handle the camera action
            }
            R.id.nav_new_trade -> {

            }
            R.id.nav_view_trade -> {

            }
            R.id.nav_profile -> {

            }
            R.id.nav_personal -> {

            }
            R.id.nav_billing -> {

            }
        }

        create_trade_drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
