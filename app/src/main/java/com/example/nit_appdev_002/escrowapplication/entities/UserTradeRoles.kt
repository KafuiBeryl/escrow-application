package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "user_trade_roles")
data class UserTradeRoles (
        @PrimaryKey
        @ColumnInfo(name = "id")
        @SerializedName("id")
        var id:Int,
        @ColumnInfo(name = "trade_id")
        @SerializedName("trade_id")
        var tradeId:Int,
        @ColumnInfo(name = "trade_name")
        @SerializedName("trade_name")
        var tradeName:String,
        @ColumnInfo(name = "user_email")
        @SerializedName("user_email")
        var userEmail:String,
//        @ColumnInfo(name = "userTradeRole")
        @SerializedName("user_trade_role")
        @Embedded
        var userRole:TradeRole?,
        @ColumnInfo(name = "created_at")
        @SerializedName("created_at")
        var createdDate:Date,
        @ColumnInfo(name = "updated_at")
        @SerializedName("updated_at")
        var updatedDate:Date
)