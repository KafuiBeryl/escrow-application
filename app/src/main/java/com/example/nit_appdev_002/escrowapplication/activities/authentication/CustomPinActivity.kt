package com.example.nit_appdev_002.escrowapplication.activities.authentication

import android.content.Intent
import com.example.nit_appdev_002.escrowapplication.utilities.PIN_FAILED
import com.example.nit_appdev_002.escrowapplication.utilities.PIN_FORGOT
import com.example.nit_appdev_002.escrowapplication.utilities.PIN_SUCCEEDED
import com.github.omadahealth.lollipin.lib.managers.AppLockActivity

class CustomPinActivity : AppLockActivity() {


    override fun showForgotDialog() {
        val replyIntent = Intent()
        setResult(PIN_FORGOT, replyIntent)
        finish()
    }

    override fun onPinSuccess(attempts: Int) {
        val reply = Intent()
        setResult(PIN_SUCCEEDED, reply)
        finish()
    }

    override fun onPinFailure(attempts: Int) {
        if (attempts > 3){
            val replyIntent = Intent()
            setResult(PIN_FAILED, replyIntent)
            finish()
        }
    }
}
