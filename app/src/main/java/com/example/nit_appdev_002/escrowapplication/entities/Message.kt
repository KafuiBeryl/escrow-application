package com.example.nit_appdev_002.escrowapplication.entities

import com.google.gson.annotations.SerializedName

data class Message(
        @SerializedName("user")
        var user:String,
        @SerializedName("message")
        var message:String?,
        @SerializedName("image")
        var imagePath:String?,
        @SerializedName("time")
        var time:Long
)