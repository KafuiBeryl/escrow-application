package com.example.nit_appdev_002.escrowapplication.repositories

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.nit_appdev_002.escrowapplication.dao.UserDao
import com.example.nit_appdev_002.escrowapplication.dao.UserTradeRoleDao
import com.example.nit_appdev_002.escrowapplication.database.EscrowRoomDatabase
import com.example.nit_appdev_002.escrowapplication.entities.User
import com.example.nit_appdev_002.escrowapplication.entities.UserTradeRoles
import com.example.nit_appdev_002.escrowapplication.miscobjects.InsertUserImage

class UserRepository(application: Application) {
    private var mUserDao:UserDao

    private var mUserTradeRoleDao:UserTradeRoleDao

//    private var mAllUserTradeRoles:LiveData<List<UserTradeRoles>>

    init {
        val db = EscrowRoomDatabase.getDatabase(application)!!
        mUserDao = db.userDao()
        mUserTradeRoleDao = db.userTradeDao()
//        mAllUserTradeRoles = mUserTradeRoleDao.getAllUserTradeRoles()
    }

    fun logoutThings(){
        LogoutUserAsync(mUserDao, mUserTradeRoleDao).execute()
    }

    fun insertUser(user: User){
        InsertUserAsync(mUserDao).execute(user)
    }

    fun getUser(id:Int): LiveData<User> {
        return mUserDao.getUser(id)
    }

    fun addUserImage(insertUserImage: InsertUserImage){
        InsertUserImageAsync(mUserDao).execute(insertUserImage)
    }

    fun updateUser(user: User){
        UpdateUserAsync(mUserDao).execute(user)
    }

    fun insertUserRole(userRoles:List<UserTradeRoles>){
        InsertUserTradeRolesAsync(mUserTradeRoleDao).execute(userRoles)
    }

    fun countUserTradeRoles():LiveData<Int>{
        return mUserTradeRoleDao.countUserTradeRoles()
    }

    fun getAllUserTradeRoles():LiveData<List<UserTradeRoles>>{
        return mUserTradeRoleDao.getAllUserTradeRoles()
    }

    fun getUserImage(id:Int):LiveData<String>{
        return mUserDao.getUserImage(id)
    }

    fun getUserEmail(id:Int):LiveData<String>{
        return mUserDao.getUserEmail(id)
    }

    class InsertUserImageAsync(dao:UserDao) : AsyncTask<InsertUserImage, Void, Void>(){
        private var mUserDao:UserDao
        init {
            mUserDao = dao
        }

        override fun doInBackground(vararg params: InsertUserImage): Void? {
            mUserDao.addUserImage(params[0].image, params[0].id)
            return null
        }
    }

    class LogoutUserAsync(dao: UserDao, userTradeRoleDao: UserTradeRoleDao): AsyncTask<Void, Void, Void>(){
        private var mUserDao:UserDao
        private var mUserTradeRoleDao:UserTradeRoleDao
        init {
            mUserDao = dao
            mUserTradeRoleDao = userTradeRoleDao
        }

        override fun doInBackground(vararg p0: Void?): Void? {
            mUserDao.nukeTable()
            mUserTradeRoleDao.nukeTable()
            return null
        }
    }

    class InsertUserAsync(dao:UserDao) : AsyncTask<User, Void, Void>(){
        private var mUserDao:UserDao
        init {
            mUserDao = dao
        }

        override fun doInBackground(vararg params: User): Void? {
            mUserDao.insert(params[0])
            return null
        }
    }

    class UpdateUserAsync(dao:UserDao) : AsyncTask<User, Void, Void>(){
        private var mUserDao:UserDao
        init {
            mUserDao = dao
        }

        override fun doInBackground(vararg params: User): Void? {
            mUserDao.updateUser(params[0])
            return null
        }
    }

    class InsertUserTradeRolesAsync(dao:UserTradeRoleDao):AsyncTask<List<UserTradeRoles>, Void, Void>(){
        private var mUserTradeRoleDao:UserTradeRoleDao
        init {
            mUserTradeRoleDao = dao
        }

        override fun doInBackground(vararg params: List<UserTradeRoles>): Void? {
            for(userRole in params[0]){
                mUserTradeRoleDao.insert(userRole)
            }
            return null
        }
    }
}