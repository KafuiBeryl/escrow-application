package com.example.nit_appdev_002.escrowapplication.fragments.edit

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.trade.view.EditTradeActivity
import com.example.nit_appdev_002.escrowapplication.entities.FeeSchedule
import com.example.nit_appdev_002.escrowapplication.entities.TradeMileStones
import com.example.nit_appdev_002.escrowapplication.utilities.GeneralHelper
import com.example.nit_appdev_002.escrowapplication.utilities.Validator
import com.example.nit_appdev_002.escrowapplication.viewmodels.MiscViewModel
import com.example.nit_appdev_002.escrowapplication.viewmodels.TransactionViewModel
import kotlinx.android.synthetic.main.fragment_edit_trade_progress.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [EditTradeProgressFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 *
 */
class EditTradeProgressFragment : Fragment(), View.OnClickListener {
    companion object {
        @JvmStatic
        private lateinit var miscViewModel: MiscViewModel

        @JvmStatic
        private lateinit var mTransactionViewModel: TransactionViewModel

        @JvmStatic
        private var progressNumber: Int = 0

        @JvmStatic
        private var progressNameNumber: Int = 700

        @JvmStatic
        private var progressDescriptionNumber: Int = 800

        @JvmStatic
        private var progressSellerAmountNumber: Int = 900

        @JvmStatic
        private var progressInspectionNumber: Int = 1000

        @JvmStatic
        private var progressDeliveryNumber: Int = 1100
    }
    private var listener: OnFragmentInteractionListener? = null
    //declare fee schedule array
    private val schedule = mutableListOf<FeeSchedule>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_trade_progress, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //instantiate ViewModel object
        miscViewModel = ViewModelProviders.of(this).get(MiscViewModel::class.java)
        //instantiate ViewModel object
        mTransactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel::class.java)

        miscViewModel.getFeeSchedule().observe(this, Observer {
            feeList ->
            if (!feeList!!.isEmpty()){
                for (item in feeList){
                    schedule.add(item)
                }
            }
        })

        mTransactionViewModel.getTradeMilestones(EditTradeActivity.selectedTrade?.id!!).observe( this, Observer {
            milestones ->
            if (!milestones.isEmpty()){
                progressNumber = milestones.count()
                for (i in 1 .. milestones.count()){
                    addOldLayout(milestones[i-1], i)
                }
                if (milestones.count() == 3){
                    btn_add_progress.visibility = View.GONE
                }
            }
        })

        btn_add_progress.setOnClickListener {
            addLayout()
        }
    }

    private fun addOldLayout(mileStone: TradeMileStones, stepNumber:Int){
        //craft the UI in code. This should be good again. Fingers crossed
        //well it was. I've probably got new greys because of this
        val bigLinearLayout = LinearLayout(activity)
        val linearLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        bigLinearLayout.layoutParams = linearLayoutParams
        bigLinearLayout.orientation = LinearLayout.VERTICAL

        val progressNameTextView = createBigTextView()
        progressNameTextView.text = resources.getString(R.string.trade_progress_name)
        progressNameTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val progressNameEdit = createBigEditText()
        progressNameEdit.id = progressNameNumber + stepNumber
        progressNameEdit.text = Editable.Factory.getInstance().newEditable(mileStone.milestoneName)

        val progressDescriptionTextView = createBigTextView()
        progressDescriptionTextView.text = resources.getString(R.string.trade_progress_description)
        progressDescriptionTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val progressDescEdit = createHugeEditText()
        progressDescEdit.id = progressDescriptionNumber + stepNumber
        progressDescEdit.text = Editable.Factory.getInstance().newEditable(mileStone.description)

        val progressAmountTextView = createBigTextView()
        progressAmountTextView.text = resources.getString(R.string.trade_progress_seller_amount)
        progressAmountTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val progressAmountEdit = createNumberEditText()
        progressAmountEdit.id = progressSellerAmountNumber + stepNumber
        progressAmountEdit.text = Editable.Factory.getInstance().newEditable(GeneralHelper.formatAmount(mileStone.grossAmount))

        val firstSmallLinearLayout = LinearLayout(activity)
        val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        firstSmallLinearLayout.layoutParams = layoutParams
        firstSmallLinearLayout.orientation = LinearLayout.HORIZONTAL

        val progressInspectionTextView = createSmallTextView()
        progressInspectionTextView.text = resources.getString(R.string.trade_inspection_period)
        progressInspectionTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val progressDeliveryTextView = createSmallTextView()
        progressDeliveryTextView.text = resources.getString(R.string.trade_delivery_time)
        progressDeliveryTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        firstSmallLinearLayout.addView(progressInspectionTextView, 0)
        firstSmallLinearLayout.addView(progressDeliveryTextView, 1)

        val firstSmallEditLinearLayout = LinearLayout(activity)
        val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layoutParams.gravity = Gravity.CENTER
        firstSmallEditLinearLayout.layoutParams = secondLayoutParams
        firstSmallEditLinearLayout.orientation = LinearLayout.HORIZONTAL

        val inspectionEdit = createSmallNumberEditText()
        inspectionEdit.id = progressInspectionNumber + stepNumber
        inspectionEdit.text = Editable.Factory.getInstance().newEditable(mileStone.inspectionPeriod)

        val deliveryEdit = createSmallNumberEditText()
        deliveryEdit.id = progressDeliveryNumber + stepNumber
        deliveryEdit.text = Editable.Factory.getInstance().newEditable(mileStone.deliveryTime)

        firstSmallEditLinearLayout.addView(inspectionEdit, 0)
        firstSmallEditLinearLayout.addView(deliveryEdit, 1)

        bigLinearLayout.addView(progressNameTextView)
        bigLinearLayout.addView(progressNameEdit)
        bigLinearLayout.addView(progressDescriptionTextView)
        bigLinearLayout.addView(progressDescEdit)
        bigLinearLayout.addView(progressAmountTextView)
        bigLinearLayout.addView(progressAmountEdit)
        bigLinearLayout.addView(firstSmallLinearLayout)
        bigLinearLayout.addView(firstSmallEditLinearLayout)

        val removeButton = createRemoveBeneficiaryButton()

        bigLinearLayout.addView(removeButton)

        progress_linear_layout.addView(bigLinearLayout)
    }

    private fun addLayout(){
        if (progressNumber < 3){
            //basically a = a+b
            progressNumber ++
            //craft the UI in code. This should be good again. Fingers crossed
            //well it was. I've probably got new greys because of this
            val bigLinearLayout = LinearLayout(activity)
            val linearLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
            bigLinearLayout.layoutParams = linearLayoutParams
            bigLinearLayout.orientation = LinearLayout.VERTICAL

            val progressNameTextView = createBigTextView()
            progressNameTextView.text = resources.getString(R.string.trade_progress_name)
            progressNameTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val progressNameEdit = createBigEditText()
            progressNameEdit.id = progressNameNumber + progressNumber

            val progressDescriptionTextView = createBigTextView()
            progressDescriptionTextView.text = resources.getString(R.string.trade_progress_description)
            progressDescriptionTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val progressDescEdit = createHugeEditText()
            progressDescEdit.id = progressDescriptionNumber + progressNumber

            val progressAmountTextView = createBigTextView()
            progressAmountTextView.text = resources.getString(R.string.trade_progress_seller_amount)
            progressAmountTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val progressAmountEdit = createNumberEditText()
            progressAmountEdit.id = progressSellerAmountNumber + progressNumber

            val firstSmallLinearLayout = LinearLayout(activity)
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            firstSmallLinearLayout.layoutParams = layoutParams
            firstSmallLinearLayout.orientation = LinearLayout.HORIZONTAL

            val progressInspectionTextView = createSmallTextView()
            progressInspectionTextView.text = resources.getString(R.string.trade_inspection_period)
            progressInspectionTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val progressDeliveryTextView = createSmallTextView()
            progressDeliveryTextView.text = resources.getString(R.string.trade_delivery_time)
            progressDeliveryTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            firstSmallLinearLayout.addView(progressInspectionTextView, 0)
            firstSmallLinearLayout.addView(progressDeliveryTextView, 1)

            val firstSmallEditLinearLayout = LinearLayout(activity)
            val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            layoutParams.gravity = Gravity.CENTER
            firstSmallEditLinearLayout.layoutParams = secondLayoutParams
            firstSmallEditLinearLayout.orientation = LinearLayout.HORIZONTAL

            val inspectionEdit = createSmallNumberEditText()
            inspectionEdit.id = progressInspectionNumber + progressNumber

            val deliveryEdit = createSmallNumberEditText()
            deliveryEdit.id = progressDeliveryNumber + progressNumber

            firstSmallEditLinearLayout.addView(inspectionEdit, 0)
            firstSmallEditLinearLayout.addView(deliveryEdit, 1)

            bigLinearLayout.addView(progressNameTextView)
            bigLinearLayout.addView(progressNameEdit)
            bigLinearLayout.addView(progressDescriptionTextView)
            bigLinearLayout.addView(progressDescEdit)
            bigLinearLayout.addView(progressAmountTextView)
            bigLinearLayout.addView(progressAmountEdit)
            bigLinearLayout.addView(firstSmallLinearLayout)
            bigLinearLayout.addView(firstSmallEditLinearLayout)

            val removeButton = createRemoveBeneficiaryButton()

            bigLinearLayout.addView(removeButton)

            progress_linear_layout.addView(bigLinearLayout)

        }else{
            btn_add_progress.visibility = View.GONE
            Toast.makeText(activity, R.string.trade_too_many_text, Toast.LENGTH_LONG).show()
        }
    }

    private fun createRemoveBeneficiaryButton(): Button {
        val button = Button(activity, null, android.R.attr.borderlessButtonStyle)
        button.setTextColor(ResourcesCompat.getColor(resources, android.R.color.holo_red_dark, null))
        button.textSize = 15f
        val textViewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_semibold)
        button.typeface = textViewTypeface
        button.setAllCaps(false)
        button.gravity = Gravity.END
        val buttonLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        buttonLayoutParams.gravity = Gravity.END
//        buttonLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.terms_small_margin)
        button.layoutParams = buttonLayoutParams
        button.text = resources.getString(R.string.trade_progress_remove_button)
        button.setOnClickListener {
            view ->
            // same as subtracting 1
            progressNumber --
            if (progressNumber < 3){
                btn_add_progress.visibility = View.VISIBLE
            }
            progress_linear_layout.removeView(view.parent as View)
        }
        return button
    }

    private fun createSmallTextView(): TextView {
        //craft small text view
        val textView = TextView(activity)
        textView.setPadding(10, 0, 10, 0)
        //not setting text here because it doesn't reflect in the UI.
        //have to call it at the top
//        textView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        if (Build.VERSION.SDK_INT < 23){
            textView.setTextAppearance(activity, android.R.style.TextAppearance_Small)
        }else{
            textView.setTextAppearance(android.R.style.TextAppearance_Small)
        }
        val textViewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_semibold)
        textView.typeface = textViewTypeface
        val textViewLayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        textViewLayoutParams.weight = 1f
        textViewLayoutParams.bottomMargin = 5
        textView.layoutParams = textViewLayoutParams
        textView.setAllCaps(true)
        return textView
    }

    private fun createBigTextView(): TextView {
        //craft normal text view
        val textView = TextView(activity)
        textView.setPadding(10, 0, 10, 0)
        //not setting text here because it doesn't reflect in the UI.
        //have to call it at the top
//        textView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        if (Build.VERSION.SDK_INT < 23){
            textView.setTextAppearance(activity, android.R.style.TextAppearance_Small)
        }else{
            textView.setTextAppearance(android.R.style.TextAppearance_Small)
        }
        val textViewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_semibold)
        textView.typeface = textViewTypeface
        val textViewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        textViewLayoutParams.bottomMargin = 5
        textView.layoutParams = textViewLayoutParams
        textView.setAllCaps(true)
        return textView
    }

    private fun createBigEditText(): EditText {
        //craft edit Text
        val editText = EditText(activity)
        editText.setPadding(20, 10, 20, 10)
        editText.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        editText.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        editText.maxLines = 1
        val viewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        viewLayoutParams.gravity = Gravity.CENTER
        viewLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.edit_text_margin_top)
        viewLayoutParams.bottomMargin = resources.getDimensionPixelSize(R.dimen.terms_margin_side)
        editText.layoutParams = viewLayoutParams
        val viewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_regular)
        editText.typeface = viewTypeface
        editText.background = ResourcesCompat.getDrawable(resources, R.drawable.profile_edit_shape, null)
        return editText
    }

    //input type in number for the days inspection and delivery
    private fun createSmallNumberEditText(): EditText {
        //craft edit Text
        val editText = EditText(activity)
        editText.setPadding(20, 10, 20, 10)
        editText.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        editText.inputType = InputType.TYPE_CLASS_PHONE
        editText.maxLines = 1
        val viewLayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        viewLayoutParams.weight = 1f
        viewLayoutParams.gravity = Gravity.CENTER
        viewLayoutParams.marginEnd = resources.getDimensionPixelSize(R.dimen.terms_margin)
        viewLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.edit_text_margin_top)
        viewLayoutParams.bottomMargin = resources.getDimensionPixelSize(R.dimen.terms_margin)
        editText.layoutParams = viewLayoutParams
        val viewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_regular)
        editText.typeface = viewTypeface
        editText.background = ResourcesCompat.getDrawable(resources, R.drawable.profile_edit_shape, null)
        return editText
    }

    //input type in number hence used for the amount side as well
    private fun createNumberEditText(): EditText {
        //craft edit Text
        val editText = EditText(activity)
        editText.setPadding(20, 10, 20, 10)
        editText.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        editText.inputType = InputType.TYPE_CLASS_PHONE
        editText.maxLines = 1
        val viewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        viewLayoutParams.gravity = Gravity.CENTER
        viewLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.edit_text_margin_top)
        viewLayoutParams.bottomMargin = resources.getDimensionPixelSize(R.dimen.terms_margin_side)
        editText.layoutParams = viewLayoutParams
        val viewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_regular)
        editText.typeface = viewTypeface
        editText.background = ResourcesCompat.getDrawable(resources, R.drawable.profile_edit_shape, null)
        return editText
    }

    private fun createHugeEditText(): EditText {
        //craft edit Text
        val editText = EditText(activity)
        editText.setPadding(20, 10, 20, 10)
        editText.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        editText.inputType = InputType.TYPE_TEXT_FLAG_AUTO_CORRECT
        editText.setSingleLine(false)
        val viewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 200)
        viewLayoutParams.gravity = Gravity.CENTER
        viewLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.edit_text_margin_top)
        viewLayoutParams.bottomMargin = resources.getDimensionPixelSize(R.dimen.terms_margin_side)
        editText.layoutParams = viewLayoutParams
        val viewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_regular)
        editText.typeface = viewTypeface
        editText.background = ResourcesCompat.getDrawable(resources, R.drawable.profile_edit_shape, null)
        return editText
    }

    private fun validateTheEditTexts():Boolean{
        var validate = false

        val emptyText = Validator.traverseEditTexts(progress_linear_layout)
        //if null then it didn't find any edit text without text
        if (emptyText == null){
            validate = true
        }

        return validate
    }

    private fun calculateEscrowFee(amount: Long):Long{


        var feeSchedule:FeeSchedule? = null
        if (schedule.isNotEmpty()){
            feeSchedule = schedule.filter {
                amount >= it.from && amount <= it.to
            }.single()
        }
        val percentage = feeSchedule!!.percentage
        var escrowFee = ((amount * percentage)/100)

        if (escrowFee < 3000.00){
            escrowFee = 3000.00
        }

        return escrowFee.toLong()
    }

    private fun calculateAdminFeesPlusPayments(amount:Long){
        val feeAllocation = EditTradeActivity.editTradeRequest.escrowAllocation
        val agentEmail = EditTradeActivity.editTradeRequest.agentEmail
        val escrowFee = calculateEscrowFee(amount)
        EditTradeActivity.editTradeRequest.escrowFee = escrowFee
        when (feeAllocation){
            "Seller Pays" -> {
                val sellerFee = amount-escrowFee
                EditTradeActivity.editTradeRequest.sellerFee = sellerFee
                EditTradeActivity.editTradeRequest.buyerFee = amount
            }
            "Buyer Pays" -> {
                val buyerFee = amount+escrowFee
                EditTradeActivity.editTradeRequest.buyerFee = buyerFee
                EditTradeActivity.editTradeRequest.sellerFee = amount
            }
            "Agent Pays" -> {
                val agentFee = calculateAgentFeeNoDivision(amount)-escrowFee
                EditTradeActivity.editTradeRequest.agentFee = agentFee
                EditTradeActivity.editTradeRequest.buyerFee = amount
                EditTradeActivity.editTradeRequest.sellerFee = amount
            }
            "50/50 Split [Buyer/Agent]" -> {
                val agentFee = calculateAgentFeeNoDivision(amount)-(escrowFee/2)
                EditTradeActivity.editTradeRequest.agentFee = agentFee
                val buyerFee = amount+(escrowFee/2)
                EditTradeActivity.editTradeRequest.buyerFee = buyerFee
                EditTradeActivity.editTradeRequest.sellerFee = amount
            }
            "50/50 Split [Buyer/Seller]" -> {
                val buyerFee = amount+(escrowFee/2)
                EditTradeActivity.editTradeRequest.buyerFee = buyerFee
                val sellerFee = amount-(escrowFee/2)
                EditTradeActivity.editTradeRequest.sellerFee = sellerFee
            }
            "50/50 Split [Seller/Agent]" -> {
                val agentFee = calculateAgentFeeNoDivision(amount)-(escrowFee/2)
                EditTradeActivity.editTradeRequest.agentFee = agentFee
                val sellerFee = amount-(escrowFee/2)
                EditTradeActivity.editTradeRequest.sellerFee = sellerFee
                EditTradeActivity.editTradeRequest.buyerFee = amount
            }
            "3-Way Split [Buyer/Seller/Agent]" -> {
                val buyerFee = amount+(escrowFee/3)
                EditTradeActivity.editTradeRequest.buyerFee = buyerFee
                val sellerFee = amount-(escrowFee/3)
                EditTradeActivity.editTradeRequest.sellerFee = sellerFee
                val agentFee = calculateAgentFeeNoDivision(amount)-(escrowFee/3)
                EditTradeActivity.editTradeRequest.agentFee = agentFee
            }
        }

        //calculate agent deductions and additions after the base values have been set
        if (agentEmail != null){
            setAgentFeeAllocation(amount)
        }
    }

    private fun setAgentFeeAllocation(amount: Long){
        val agentFeeAllocation = EditTradeActivity.editTradeRequest.agentFeeAllocation
        //if agent fee hasn't been set above then set it
        if (EditTradeActivity.editTradeRequest.agentFee == 0L){
            EditTradeActivity.editTradeRequest.agentFee = calculateAgentFeeNoDivision(amount)
        }
        when (agentFeeAllocation){
            "Seller Pays" -> {
                EditTradeActivity.editTradeRequest.sellerFee = EditTradeActivity.editTradeRequest.sellerFee-calculateAgentFeeNoDivision(amount)
            }
            "Buyer Pays" -> {
                EditTradeActivity.editTradeRequest.buyerFee = EditTradeActivity.editTradeRequest.buyerFee+calculateAgentFeeNoDivision(amount)
            }
            "50/50 Split [Buyer/Seller]" -> {
                EditTradeActivity.editTradeRequest.buyerFee = EditTradeActivity.editTradeRequest.buyerFee+calculateAgentFee5050Division(amount)
                EditTradeActivity.editTradeRequest.sellerFee = EditTradeActivity.editTradeRequest.sellerFee-calculateAgentFee5050Division(amount)
            }
        }
    }

    private fun calculateAgentFee5050Division(tradeAmount: Long) : Long{
        val agentFeeType = EditTradeActivity.editTradeRequest.agentFeeType
        val agentFeeUnedited = EditTradeActivity.editTradeRequest.unCalculatedAgentFee
        var agentFeeEdited:Long = 0
        when (agentFeeType){
            "Fixed Fee" -> {
                agentFeeEdited = agentFeeUnedited/2
            }
            "Percentage (%)" -> {
                //get cedi amounts from pesewas
                //and actual number from the 100 multiplication
                val agentFee = agentFeeUnedited.toDouble()/100
                val amount = tradeAmount.toDouble()/100
                //get percentage of amount
                val percentageOfAmount = (amount*agentFee)/100
                //divide amount into 2 for 50/50 division
                val shareAmount = percentageOfAmount/2
                //multiply by hundred again to get pesewas
                agentFeeEdited = (shareAmount*100).toLong()
            }
        }
        return agentFeeEdited
    }

    private fun calculateAgentFeeNoDivision(tradeAmount: Long) : Long{
        val agentFeeType = EditTradeActivity.editTradeRequest.agentFeeType
        val agentFeeUnedited = EditTradeActivity.editTradeRequest.unCalculatedAgentFee
        var agentFeeEdited:Long = 0
        when (agentFeeType){
            "Fixed Fee" -> {
                agentFeeEdited = agentFeeUnedited
            }
            "Percentage (%)" -> {
                //get cedi amounts from pesewas
                //and actual number from the 100 multiplication
                val agentFee = agentFeeUnedited.toDouble()/100
                val amount = tradeAmount.toDouble()/100
                //get percentage of amount
                val percentageOfAmount = (amount*agentFee)/100
                //multiply by hundred again to get pesewas
                agentFeeEdited = (percentageOfAmount*100).toLong()
            }
        }
        return agentFeeEdited
    }

    private fun showDialog(){
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity!!)
        builder.setTitle(R.string.alert_title)
                .setMessage(R.string.trade_progress_empty_text)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton(android.R.string.ok){_, _ ->
                    //                    activity!!.finish()
                }
        val dialog = builder.create()
        dialog.show()
    }

    private fun passEntriesToVariable(){
        var runningTotal:Long = 0
        val feeAllocation = EditTradeActivity.editTradeRequest.escrowAllocation
        val agentFeeAllocation = EditTradeActivity.editTradeRequest.agentFeeAllocation
        var cumulativePayableToSeller:Long = 0
        //basically for 1 until and including the progress number
        for (i in 1 .. progressNumber){
            val mileStoneName = view!!.findViewById<EditText>(progressNameNumber.plus(i)).text.toString()
            val description = view!!.findViewById<EditText>(progressDescriptionNumber.plus(i)).text.toString()
            val amount = (view!!.findViewById<EditText>(progressSellerAmountNumber+i).text.toString().toDouble().times(100)).toLong()
            val deliveryTime = view!!.findViewById<EditText>(progressDeliveryNumber+i).text.toString()
            val inspectionTime = view!!.findViewById<EditText>(progressInspectionNumber+i).text.toString()

            runningTotal+=amount
            val escrowFee = calculateEscrowFee(amount)
            var sellerFee:Long = 0

            when (feeAllocation){
                "Seller Pays" -> {
                    sellerFee+=escrowFee
                }
                "50/50 Split [Buyer/Seller]" -> {
                    sellerFee+=(escrowFee/2)
                }
                "50/50 Split [Seller/Agent]" -> {
                    sellerFee+=(escrowFee/2)
                }
                "3-Way Split [Buyer/Seller/Agent]" -> {
                    sellerFee+=(escrowFee/3)
                }
            }

            if (agentFeeAllocation != null){
                when (agentFeeAllocation){
                    "Seller Pays" -> {
                        sellerFee+=calculateAgentFeeNoDivision(amount)
                    }
                    "50/50 Split [Buyer/Seller]" -> {
                        sellerFee+=calculateAgentFee5050Division(amount)
                    }
                }
            }

            val payableToSeller = amount-sellerFee
            cumulativePayableToSeller+=payableToSeller

            val currentMileStone = TradeMileStones(null, null, mileStoneName,
                    description, amount, deliveryTime, inspectionTime, runningTotal,
                    sellerFee, payableToSeller, cumulativePayableToSeller, null, null)

            EditTradeActivity.mileStones.add(currentMileStone)
        }

        EditTradeActivity.editTradeRequest.tradeAmount = runningTotal
        calculateAdminFeesPlusPayments(runningTotal)
    }

    override fun onResume() {
        super.onResume()
        btn_back.setOnClickListener(this)
        btn_next.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btn_back.setOnClickListener(null)
        btn_next.setOnClickListener(null)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onClick(v: View?) {
        when (v?.id){
            R.id.btn_back -> {
                if (listener != null){
                    listener?.onBackButtonPressed(this)
                }
            }
            R.id.btn_next -> {
                if (progressNumber < 2){
                    showDialog()
                }else{
                    if (validateTheEditTexts()){
                        if (listener != null){
                            passEntriesToVariable()
                            listener?.onNextPressed(this)
                        }
                    }else{
                        Toast.makeText(activity, R.string.login_empty_text, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        fun onBackButtonPressed(fragment: Fragment)
        fun onNextPressed(fragment: Fragment)
    }

}
