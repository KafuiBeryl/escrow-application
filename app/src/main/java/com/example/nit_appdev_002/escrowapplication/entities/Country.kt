package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "country")
data class Country (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "countryId")
        @SerializedName("id")
        var countryId:Int,

        @ColumnInfo(name = "country_code")
        @SerializedName("country_code")
        var countryCode:String,

        @ColumnInfo(name = "country_name")
        @SerializedName("country_name")
        var countryName:String,

        @ColumnInfo(name = "country_dialing_code")
        @SerializedName("country_dialing_code")
        var countryDialingCode:String,

        @ColumnInfo(name = "currency")
        @SerializedName("currency")
        var currency:String,

        @ColumnInfo(name = "currency_name")
        @SerializedName("currency_name")
        var currencyName:String,

        @ColumnInfo(name = "currency_code")
        @SerializedName("currency_code")
        var currencyCode:String,

        @ColumnInfo(name = "active")
        @SerializedName("active")
        var active:Boolean
)