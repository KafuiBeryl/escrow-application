package com.example.nit_appdev_002.escrowapplication.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.example.nit_appdev_002.escrowapplication.fragments.payment.CardPaymentFragment
import com.example.nit_appdev_002.escrowapplication.fragments.payment.MobileMoneyFragment
import com.example.nit_appdev_002.escrowapplication.fragments.payment.PaymentTypeFragment

class PaymentPagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager), PaymentTypeFragment.OnFragmentInteractionListener {


    private var mcardPaymentFragment : CardPaymentFragment
    private var mMobileMoneyFragment : MobileMoneyFragment

    init {
        mcardPaymentFragment = CardPaymentFragment()
        mMobileMoneyFragment = MobileMoneyFragment()
    }

    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        when(position){
            0 -> fragment = PaymentTypeFragment()
            1 -> fragment = mcardPaymentFragment
            2 -> fragment = mMobileMoneyFragment
        }
        return fragment!!
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun onNextPressed(fragment: Fragment, selectedMode: String) {
        when (fragment){
            is PaymentTypeFragment -> {
                if (selectedMode == "Card"){
                    mcardPaymentFragment.onNextPressed(PaymentTypeFragment(), selectedMode)
                }else{

                    mMobileMoneyFragment.onNextPressed(PaymentTypeFragment(), selectedMode)
                }
            }
        }
    }
}