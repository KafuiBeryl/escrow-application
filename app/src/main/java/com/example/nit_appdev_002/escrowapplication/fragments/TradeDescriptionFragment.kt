package com.example.nit_appdev_002.escrowapplication.fragments


import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.set
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.trade.CreateTradeActivity
import com.example.nit_appdev_002.escrowapplication.entities.FeeSchedule
import com.example.nit_appdev_002.escrowapplication.utilities.*
import com.example.nit_appdev_002.escrowapplication.viewmodels.MiscViewModel
import kotlinx.android.synthetic.main.fragment_trade_description.*
import kotlinx.android.synthetic.main.partial_agent_fee_description.*


/**
 *
 * A simple [Fragment] subclass.
 *
 */
class TradeDescriptionFragment : Fragment(), View.OnClickListener, TradeBeneficiaryFragment.OnTradeBeneficiaryListener, TradeProgressFragment.OnTradeProgressListener{

    companion object {
        @JvmStatic
        private var isAgentAdded:Boolean? = false

        @JvmStatic
        private var isProgressPayment:Boolean = false

        @JvmStatic
        private lateinit var miscViewModel: MiscViewModel

        @JvmStatic
        private var selectedIndustryClassification:String? = null

        @JvmStatic
        private var selectedAdminAllocation:String? = null

        @JvmStatic
        private var isBackPressed:Boolean = false

        @JvmStatic
        private var selectedAgentAllocation:String? = null

        @JvmStatic
        private var selectedAgentFeeType:String? = null
    }

    private var mListener: OnTradeDescriptionListener? = null


    //declare admin allocation array
    private val adminAllocationArray = ArrayList<String>()

    //declare admin allocation array adapter
    private lateinit var adminAllocationAdapter:ArrayAdapter<String>

    //declare fee schedule array
    private val schedule = mutableListOf<FeeSchedule>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_trade_description, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progress_lock_linear_layout.visibility = View.VISIBLE

        //instantiate ViewModel object
        miscViewModel = ViewModelProviders.of(this).get(MiscViewModel::class.java)

        //set the adapter for the spinners
        val industryArray = ArrayList<String>()
        val industryAdapter = ArrayAdapter(activity!!.baseContext, R.layout.simple_spinner_item, industryArray)
        industryAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_industry_classification.adapter = industryAdapter


        adminAllocationAdapter = ArrayAdapter(activity!!.baseContext, R.layout.simple_spinner_item, adminAllocationArray)
        adminAllocationAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_admin_allocation.adapter = adminAllocationAdapter

        //set on click listeners to spinners
        spinner_admin_allocation.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedAdminAllocation = parent?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedAdminAllocation = null
            }
        }

        spinner_industry_classification.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedIndustryClassification = parent?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedIndustryClassification = null
            }
        }

        miscViewModel.getIndustries().observe(this, Observer {
            industries -> if (!industries!!.isEmpty()){
            for (industry in industries){
                industryArray.add(industry)
            }
            industryAdapter.notifyDataSetChanged()
        }
        })

        miscViewModel.getFeeSchedule().observe(this, Observer {
            feeList ->
            if (!feeList!!.isEmpty()){
                for (item in feeList){
                    schedule.add(item)
                }
            }
        })

        if (isBackPressed){
            setInsertedValues(industryAdapter, adminAllocationAdapter)
        }
    }

    private fun setInsertedValues(industryAdapter:ArrayAdapter<String>, feeAllocationAdapter:ArrayAdapter<String>){
        val addedAlready = CreateTradeActivity.createTradeRequest
        trade_name_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.tradeName)
        val position = industryAdapter.getPosition(addedAlready.tradeCategory)
        spinner_industry_classification.setSelection(position)
        trade_description_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.description)
        val allocationPosition = feeAllocationAdapter.getPosition(addedAlready.escrowAllocation)
        spinner_admin_allocation.setSelection(allocationPosition)
        checkbox_is_progress.isChecked = addedAlready.isProgressPayment

        if (!addedAlready.isProgressPayment){
            trade_amount_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.tradeAmount.toString())
            delivery_time_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.deliveryTime)
            inspection_time_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.inspectionPeriod)
        }
        //set agent if exists
        addAgentOnBackPressed()
    }

    private fun addAgentOnBackPressed(){
        val context = activity?.applicationContext
        if (context != null){
            val prefs = PreferenceHelper.defaultPrefs(context)
            isAgentAdded = prefs[PREF_HAS_AGENT]!!
            adminAllocationArray.clear()
            when (isAgentAdded) {
                true -> {

                    //set the partial view
                    val stub = ViewStub(activity)
                    val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    stub.layoutParams = secondLayoutParams
                    stub.layoutResource = R.layout.partial_agent_fee_description
                    agent_desc_linear_layout.addView(stub)
                    stub.inflate()

                    setAgentSpinners()
                    miscViewModel.getAgentAdminFeeAllocation().observe(this, Observer {
                        allocates ->
                        if (!allocates!!.isEmpty()){
                            for (allocate in allocates){
                                adminAllocationArray.add(allocate)
                            }
                            adminAllocationAdapter.notifyDataSetChanged()
                        }
                    })
                }
                false -> {
                    //remove agent partial view
                    agent_desc_linear_layout.removeAllViews()

                    miscViewModel.getAdminFeeAllocation().observe(this, Observer {
                        allocates ->
                        if (!allocates!!.isEmpty()){
                            for (allocate in allocates){
                                adminAllocationArray.add(allocate)
                            }
                            adminAllocationAdapter.notifyDataSetChanged()
                        }
                    })
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        btn_back.setOnClickListener(this)
        btn_next.setOnClickListener(this)
        checkbox_is_progress.setOnCheckedChangeListener { _, isChecked ->
            when (isChecked){
                true -> {
                    isProgressPayment = true
                    progress_lock_linear_layout.visibility = View.GONE
                }
                false -> {
                    isProgressPayment = false
                    progress_lock_linear_layout.visibility = View.VISIBLE
                }
            }
        }

        checkbox_is_progress.isChecked = false


    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser){
            val context = activity?.applicationContext
            if (context != null){
                val prefs = PreferenceHelper.defaultPrefs(context)
                isAgentAdded = prefs[PREF_HAS_AGENT]!!
                when (isAgentAdded) {
                    true -> {

                        //set the partial view
                        val stub = ViewStub(activity)
                        val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                        stub.layoutParams = secondLayoutParams
                        stub.layoutResource = R.layout.partial_agent_fee_description
                        agent_desc_linear_layout.addView(stub)
                        stub.inflate()

                        setAgentSpinners()
                        miscViewModel.getAgentAdminFeeAllocation().observe(this, Observer {
                            allocates ->
                            if (!allocates!!.isEmpty()){
                                for (allocate in allocates){
                                    adminAllocationArray.add(allocate)
                                }
                                adminAllocationAdapter.notifyDataSetChanged()
                            }
                        })
                    }
                    false -> {
                        //remove agent partial view
                        agent_desc_linear_layout.removeAllViews()

                        miscViewModel.getAdminFeeAllocation().observe(this, Observer {
                            allocates ->
                            if (!allocates!!.isEmpty()){
                                for (allocate in allocates){
                                    adminAllocationArray.add(allocate)
                                }
                                adminAllocationAdapter.notifyDataSetChanged()
                            }
                        })
                    }
                }
            }
        }
    }

    private fun setAgentSpinners(){
        val agentAllocationArray = ArrayList<String>()
        val agentAllocationAdapter = ArrayAdapter(activity!!.baseContext, R.layout.simple_spinner_item, agentAllocationArray)
        agentAllocationAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_agent_allocation.adapter = agentAllocationAdapter

        spinner_agent_allocation.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedAgentAllocation = parent?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedAgentAllocation = null
            }
        }

        miscViewModel.getStandardFeeAllocation().observe(this, Observer {
            allocates -> if (!allocates!!.isEmpty()){
                for (allocate in allocates){
                    agentAllocationArray.add(allocate)
                }
                agentAllocationAdapter.notifyDataSetChanged()
            }
        })

        val agentFeeTypeArray = ArrayList<String>()
        val agentFeeTypeAdapter = ArrayAdapter(activity!!.baseContext, R.layout.simple_spinner_item, agentFeeTypeArray)
        agentFeeTypeAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_agent_fee_type.adapter = agentFeeTypeAdapter

        spinner_agent_fee_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedAgentFeeType = parent?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedAgentFeeType = null
            }
        }

        miscViewModel.getFeeTypes().observe(this, Observer {
            feeTypes -> if (!feeTypes!!.isEmpty()){
                for (feeType in feeTypes){
                    agentFeeTypeArray.add(feeType)
                }
                agentFeeTypeAdapter.notifyDataSetChanged()
            }
        })

        if (isBackPressed){
            val addedAlready = CreateTradeActivity.createTradeRequest
            txt_agent_fee.text = Editable.Factory.getInstance().newEditable(addedAlready.unCalculatedAgentFee.toString())
            val position = agentAllocationAdapter.getPosition(addedAlready.agentFeeAllocation)
            spinner_agent_allocation.setSelection(position)
            val secondPosition = agentFeeTypeAdapter.getPosition(addedAlready.agentFeeType)
            spinner_agent_fee_type.setSelection(secondPosition)
        }
    }

    override fun onPause() {
        super.onPause()
        btn_back.setOnClickListener(null)
        btn_next.setOnClickListener(null)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnTradeDescriptionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnTradeBeneficiaryListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    private fun passEntriesToVariable(){
        CreateTradeActivity.createTradeRequest.currencyUsed = "GHS"
        //set has progress to value
        CreateTradeActivity.createTradeRequest.isProgressPayment = isProgressPayment
        when (isAgentAdded){
            true -> {
                when (isProgressPayment){
                    true -> {
                        CreateTradeActivity.createTradeRequest.tradeName = trade_name_txt.text.toString()
                        CreateTradeActivity.createTradeRequest.description = trade_description_txt.text.toString()
                        //depending on the fee type, the agent fee is calculated
                        CreateTradeActivity.createTradeRequest.unCalculatedAgentFee = ((txt_agent_fee.text.toString().toDouble() * 100).toLong())
                        CreateTradeActivity.createTradeRequest.tradeCategory = selectedIndustryClassification
                        CreateTradeActivity.createTradeRequest.escrowAllocation = selectedAdminAllocation
                        CreateTradeActivity.createTradeRequest.agentFeeAllocation = selectedAgentAllocation
                        CreateTradeActivity.createTradeRequest.agentFeeType = selectedAgentFeeType

                        //set has progress to true
//                        CreateTradeActivity.createTradeRequest.isProgressPayment = true
                    }
                    false -> {
                        CreateTradeActivity.createTradeRequest.tradeName = trade_name_txt.text.toString()
                        CreateTradeActivity.createTradeRequest.description = trade_description_txt.text.toString()
                        //multiplying by hundred because the values in the database are in pesewas
                        CreateTradeActivity.createTradeRequest.tradeAmount = ((trade_amount_txt.text.toString().toDouble() * 100).toLong())
                        CreateTradeActivity.createTradeRequest.deliveryTime = delivery_time_txt.text.toString()
                        CreateTradeActivity.createTradeRequest.inspectionPeriod = inspection_time_txt.text.toString()
                        //depending on the fee type, the agent fee is calculated
                        CreateTradeActivity.createTradeRequest.unCalculatedAgentFee = ((txt_agent_fee.text.toString().toDouble() * 100).toLong())
                        CreateTradeActivity.createTradeRequest.tradeCategory = selectedIndustryClassification
                        CreateTradeActivity.createTradeRequest.escrowAllocation = selectedAdminAllocation
                        CreateTradeActivity.createTradeRequest.agentFeeAllocation = selectedAgentAllocation
                        CreateTradeActivity.createTradeRequest.agentFeeType = selectedAgentFeeType

                        //calculate individual fees
                        calculateAdminFeesPlusPayments((trade_amount_txt.text.toString().toDouble() * 100).toLong())
                    }
                }
            }
            false -> {
                when (isProgressPayment){
                    true -> {
                        CreateTradeActivity.createTradeRequest.tradeName = trade_name_txt.text.toString()
                        CreateTradeActivity.createTradeRequest.description = trade_description_txt.text.toString()
                        CreateTradeActivity.createTradeRequest.tradeCategory = selectedIndustryClassification
                        CreateTradeActivity.createTradeRequest.escrowAllocation = selectedAdminAllocation

                        //set has progress to true
//                        CreateTradeActivity.createTradeRequest.isProgressPayment = true
                    }
                    false -> {
                        CreateTradeActivity.createTradeRequest.tradeName = trade_name_txt.text.toString()
                        CreateTradeActivity.createTradeRequest.description = trade_description_txt.text.toString()
                        //multiplying by hundred because the values in the database are in pesewas
                        CreateTradeActivity.createTradeRequest.tradeAmount = ((trade_amount_txt.text.toString().toDouble() * 100).toLong())
                        CreateTradeActivity.createTradeRequest.deliveryTime = delivery_time_txt.text.toString()
                        CreateTradeActivity.createTradeRequest.inspectionPeriod = inspection_time_txt.text.toString()
                        CreateTradeActivity.createTradeRequest.tradeCategory = selectedIndustryClassification
                        CreateTradeActivity.createTradeRequest.escrowAllocation = selectedAdminAllocation

                        //calculate individual fees
                        calculateAdminFeesPlusPayments((trade_amount_txt.text.toString().toDouble() * 100).toLong())
                    }
                }
            }
        }
    }

    private fun calculateEscrowFee(amount: Long):Long{

        var feeSchedule: FeeSchedule? = null
        if (schedule.isNotEmpty()){
            feeSchedule = schedule.filter {
                amount >= it.from && amount <= it.to
            }.single()
        }
        val percentage = feeSchedule!!.percentage
        //percentage of amount in pesewas
        var escrowFee = ((amount * percentage)/100)

        if (escrowFee < 3000.00){
            escrowFee = 3000.00
        }

        return escrowFee.toLong()
    }

    private fun calculateAdminFeesPlusPayments(amount:Long){
        val feeAllocation = CreateTradeActivity.createTradeRequest.escrowAllocation
        val agentEmail = CreateTradeActivity.createTradeRequest.agentEmail
        val escrowFee = calculateEscrowFee(amount)
        CreateTradeActivity.createTradeRequest.escrowFee = escrowFee
        when (feeAllocation){
            "Seller Pays" -> {
                val sellerFee = amount-escrowFee
                CreateTradeActivity.createTradeRequest.sellerFee = sellerFee
                CreateTradeActivity.createTradeRequest.buyerFee = amount
            }
            "Buyer Pays" -> {
                val buyerFee = amount+escrowFee
                CreateTradeActivity.createTradeRequest.buyerFee = buyerFee
                CreateTradeActivity.createTradeRequest.sellerFee = amount
            }
            "Agent Pays" -> {
                val agentFee = calculateAgentFeeNoDivision(amount)-escrowFee
                CreateTradeActivity.createTradeRequest.agentFee = agentFee
                CreateTradeActivity.createTradeRequest.buyerFee = amount
                CreateTradeActivity.createTradeRequest.sellerFee = amount
            }
            "50/50 Split [Buyer/Agent]" -> {
                val agentFee = calculateAgentFeeNoDivision(amount)-(escrowFee/2)
                CreateTradeActivity.createTradeRequest.agentFee = agentFee
                val buyerFee = amount+(escrowFee/2)
                CreateTradeActivity.createTradeRequest.buyerFee = buyerFee
                CreateTradeActivity.createTradeRequest.sellerFee = amount
            }
            "50/50 Split [Buyer/Seller]" -> {
                val buyerFee = amount+(escrowFee/2)
                CreateTradeActivity.createTradeRequest.buyerFee = buyerFee
                val sellerFee = amount-(escrowFee/2)
                CreateTradeActivity.createTradeRequest.sellerFee = sellerFee
            }
            "50/50 Split [Seller/Agent]" -> {
                val agentFee = calculateAgentFeeNoDivision(amount)-(escrowFee/2)
                CreateTradeActivity.createTradeRequest.agentFee = agentFee
                val sellerFee = amount-(escrowFee/2)
                CreateTradeActivity.createTradeRequest.sellerFee = sellerFee
                CreateTradeActivity.createTradeRequest.buyerFee = amount
            }
            "3-Way Split [Buyer/Seller/Agent]" -> {
                val buyerFee = amount+(escrowFee/3)
                CreateTradeActivity.createTradeRequest.buyerFee = buyerFee
                val sellerFee = amount-(escrowFee/3)
                CreateTradeActivity.createTradeRequest.sellerFee = sellerFee
                val agentFee = calculateAgentFeeNoDivision(amount)-(escrowFee/3)
                CreateTradeActivity.createTradeRequest.agentFee = agentFee
            }
        }

        //calculate agent deductions and additions after the base values have been set
        if (agentEmail != null){
            setAgentFeeAllocation(amount)
        }
    }

    private fun setAgentFeeAllocation(amount: Long){
        val agentFeeAllocation = CreateTradeActivity.createTradeRequest.agentFeeAllocation
        //if agent fee hasn't been set above then set it
        if (CreateTradeActivity.createTradeRequest.agentFee == 0L){
            CreateTradeActivity.createTradeRequest.agentFee = calculateAgentFeeNoDivision(amount)
        }
        when (agentFeeAllocation){
            "Seller Pays" -> {
                CreateTradeActivity.createTradeRequest.sellerFee = CreateTradeActivity.createTradeRequest.sellerFee-calculateAgentFeeNoDivision(amount)
            }
            "Buyer Pays" -> {
                CreateTradeActivity.createTradeRequest.buyerFee = CreateTradeActivity.createTradeRequest.buyerFee+calculateAgentFeeNoDivision(amount)
            }
            "50/50 Split [Buyer/Seller]" -> {
                CreateTradeActivity.createTradeRequest.buyerFee = CreateTradeActivity.createTradeRequest.buyerFee+calculateAgentFee5050Division(amount)
                CreateTradeActivity.createTradeRequest.sellerFee = CreateTradeActivity.createTradeRequest.sellerFee-calculateAgentFee5050Division(amount)
            }
        }
    }

    private fun calculateAgentFee5050Division(tradeAmount: Long) : Long{
        val agentFeeType = CreateTradeActivity.createTradeRequest.agentFeeType
        val agentFeeUnedited = CreateTradeActivity.createTradeRequest.unCalculatedAgentFee
        var agentFeeEdited:Long = 0
        when (agentFeeType){
            "Fixed Fee" -> {
                agentFeeEdited = agentFeeUnedited/2
            }
            "Percentage (%)" -> {
                //get cedi amounts from pesewas
                //and actual number from the 100 multiplication
                val agentFee = agentFeeUnedited.toDouble()/100
                val amount = tradeAmount.toDouble()/100
                //get percentage of amount
                val percentageOfAmount = (amount*agentFee)/100
                //divide amount into 2 for 50/50 division
                val shareAmount = percentageOfAmount/2
                //multiply by hundred again to get pesewas
                agentFeeEdited = (shareAmount*100).toLong()
            }
        }
        return agentFeeEdited
    }

    private fun calculateAgentFeeNoDivision(tradeAmount: Long) : Long{
        val agentFeeType = CreateTradeActivity.createTradeRequest.agentFeeType
        val agentFeeUnedited = CreateTradeActivity.createTradeRequest.unCalculatedAgentFee
        var agentFeeEdited:Long = 0
        when (agentFeeType){
            "Fixed Fee" -> {
                agentFeeEdited = agentFeeUnedited
            }
            "Percentage (%)" -> {
                //get cedi amounts from pesewas
                //and actual number from the 100 multiplication
                val agentFee = agentFeeUnedited.toDouble()/100
                val amount = tradeAmount.toDouble()/100
                //get percentage of amount
                val percentageOfAmount = (amount*agentFee)/100
                //multiply by hundred again to get pesewas
                agentFeeEdited = (percentageOfAmount*100).toLong()
            }
        }
        return agentFeeEdited
    }

    private fun validateTheEditTexts():Boolean{
        var validate = false
        val emptyText = Validator.traverseEditTexts(description_linear_layout)
        //if null then it didn't find any edit text without text
        if (emptyText == null){
            when (isAgentAdded){
                true -> {
                    if (selectedIndustryClassification != null &&
                            selectedAdminAllocation != null && selectedAgentAllocation != null &&
                            selectedAgentFeeType != null){
                        validate = true
                    }
                }
                false -> {
                    if (selectedIndustryClassification != null &&
                            selectedAdminAllocation != null){
                        validate = true
                    }
                }
            }
        }
        return validate
    }

    override fun onClick(v: View?) {
        when (v?.id){
            R.id.btn_back -> {
                if (mListener != null){
                    adminAllocationArray.clear()
                    mListener?.onBackButtonPressed(this)
                }
            }
            R.id.btn_next -> {
                if (validateTheEditTexts()){
                    val prefs = PreferenceHelper.defaultPrefs(activity!!.applicationContext)
                    prefs[PREF_IS_PROCESS_PAYMENT] = isProgressPayment
                    passEntriesToVariable()
                    mListener?.onNextPressed(this)
                }else{
                    Toast.makeText(activity, R.string.login_empty_text, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onNextPressed(fragment: Fragment) {
        TODO("Not Needed")
    }

    override fun onBackButtonPressed(fragment: Fragment) {
        when (fragment){
            is TradeBeneficiaryFragment -> {
                isBackPressed = true
                Log.i("bene go bak", isBackPressed.toString())
            }
            is TradeProgressFragment -> {
                isBackPressed = true
                Log.i("pregress go bak", isBackPressed.toString())
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnTradeDescriptionListener {
        fun onNextPressed(fragment: Fragment)

        fun onBackButtonPressed(fragment: Fragment)
    }
}
