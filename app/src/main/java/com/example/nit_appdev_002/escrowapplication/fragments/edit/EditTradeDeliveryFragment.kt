package com.example.nit_appdev_002.escrowapplication.fragments.edit

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.trade.view.EditTradeActivity
import com.example.nit_appdev_002.escrowapplication.apicalls.OtherNetworkCalls
import com.example.nit_appdev_002.escrowapplication.apicalls.RetrofitInstance
import com.example.nit_appdev_002.escrowapplication.apiobjects.*
import com.example.nit_appdev_002.escrowapplication.entities.DeliveryAddress
import com.example.nit_appdev_002.escrowapplication.entities.NotificationGang
import com.example.nit_appdev_002.escrowapplication.entities.TradeMileStones
import com.example.nit_appdev_002.escrowapplication.utilities.PREF_TOKEN
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.utilities.Validator
import com.example.nit_appdev_002.escrowapplication.viewmodels.TransactionViewModel
import com.example.nit_appdev_002.escrowapplication.viewmodels.UserViewModel
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.fragment_edit_trade_delivery.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [EditTradeDeliveryFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 *
 */
class EditTradeDeliveryFragment : Fragment(), View.OnClickListener {
    companion object {
        @JvmStatic
        private var isDelivery:Boolean = false

        @JvmStatic
        private var deliveryAddress: SentDeliveryAddress? = null

        @JvmStatic
        private lateinit var mUserViewModel: UserViewModel

        @JvmStatic
        private lateinit var mTransactionViewModel: TransactionViewModel
    }
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_trade_delivery, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //initialize view models
        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        mTransactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel::class.java)
        delivery_option_group.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId){
                R.id.radio_delivery_positive -> {
                    delivery_linear_layout.visibility = View.VISIBLE
                    isDelivery = true
                }
                R.id.radio_delivery_negative -> {
                    delivery_linear_layout.visibility = View.GONE
                    isDelivery = false
                }
            }
        }


//        radio_delivery_negative.isChecked = true
        mTransactionViewModel.getTradeDeliveryAddress(EditTradeActivity.selectedTrade?.id!!).observe(this, Observer {
            address ->
            if (address != null){
                radio_delivery_positive.isChecked = true
                addOldLayout(address)
            }
        })
    }

    private fun addOldLayout(deliveryAddress: DeliveryAddress){
        txt_house_number.text = Editable.Factory.getInstance().newEditable(deliveryAddress.houseNumber)
        txt_billing_line_1.text = Editable.Factory.getInstance().newEditable(deliveryAddress.streetLine1)
        txt_billing_line_2.text = Editable.Factory.getInstance().newEditable(deliveryAddress.streetLine2)
        txt_billing_district.text = Editable.Factory.getInstance().newEditable(deliveryAddress.suburb)
        txt_billing_city.text = Editable.Factory.getInstance().newEditable(deliveryAddress.townOrCity)
        txt_billing_digital.text = Editable.Factory.getInstance().newEditable(deliveryAddress.digitalAddress)
        trade_delivery_comment_txt.text = Editable.Factory.getInstance().newEditable(deliveryAddress.comments)
    }

    private fun validateTheEditTexts():Boolean{
        var validate = false
        if (isDelivery){
            val emptyText = Validator.traverseEditTexts(delivery_linear_layout)
            if (emptyText == null){
                validate = true
            }
        }else{
            validate = true
        }
        return validate
    }

    private fun passEntriesToVariable(){
        EditTradeActivity.editTradeRequest.isDelivery = true
        val houseNumber = txt_house_number.text.toString()
        val streetLine1 = txt_billing_line_1.text.toString()
        val streetLine2 = txt_billing_line_2.text.toString()
        val suburb = txt_billing_district.text.toString()
        val city = txt_billing_city.text.toString()
        val digitalAddress = txt_billing_digital.text.toString()
        val comment = trade_delivery_comment_txt.text.toString()

        deliveryAddress = SentDeliveryAddress(houseNumber, streetLine1, streetLine2,
                suburb, city, "Ghana", digitalAddress, comment)
    }

    override fun onResume() {
        super.onResume()
        btn_back.setOnClickListener(this)
        btn_submit.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btn_back.setOnClickListener(null)
        btn_submit.setOnClickListener(null)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onClick(v: View?) {
        when (v?.id){
            R.id.btn_back -> {
                if (listener != null){
                    listener?.onBackButtonPressed(this)
                }
            }
            R.id.btn_submit -> {
                if (validateTheEditTexts()){
                    val progressBar = activity?.findViewById<LinearLayout>(R.id.please_wait_group)
                    progressBar?.visibility = View.VISIBLE
                    big_delivery_linear_layout.visibility = View.GONE
                    when (isDelivery){
                        true -> {
                            passEntriesToVariable()
                            makeAPICalls()
                        }
                        false -> {
                            makeAPICalls()
                        }
                    }
                }else{
                    Toast.makeText(activity, R.string.login_empty_text, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun makeAPICalls(){
        val prefs = PreferenceHelper.defaultPrefs(activity!!.applicationContext)
        val token:String? = prefs[PREF_TOKEN]
        if (token != null){
            createTrade(token)
        }else{
            unauthorisedToast("Invalid Token.")
        }
    }

    private fun createTrade(token:String){
        val service: OtherNetworkCalls = RetrofitInstance.getRetrofitInstance()!!.create(OtherNetworkCalls::class.java)
        val tokenString = "Bearer $token"
        val tradeRequest = EditTradeActivity.editTradeRequest
        val sendExtras = (tradeRequest.hasBeneficiary || tradeRequest.isProgressPayment ||
                tradeRequest.isDelivery || tradeRequest.hasNotifications)
        val call = service.postEditTrade(tokenString, tradeRequest)
        call.enqueue(object : Callback<EditTradeResult>{
            override fun onResponse(call: Call<EditTradeResult>, response: Response<EditTradeResult>) {
                if (response.body() != null){
                    val result = response.body() as EditTradeResult
                    mTransactionViewModel.updateTrade(result.trade)
                    //extras are sent if any
                    if (sendExtras){
                        sendExtrasIfAny(token, EditTradeActivity.selectedTrade?.id!!)
                    }
                }else if (response.errorBody() != null){
                    val gson = GsonBuilder().create()
                    val errorResult:EditTradeResult = gson.fromJson<EditTradeResult>(response.errorBody()?.string(), EditTradeResult::class.java)
                    Log.e("TradeRequest", errorResult.error)
                    invalidTradeRequestToast(errorResult.error)
                }
            }

            override fun onFailure(call: Call<EditTradeResult>, t: Throwable) {
                if (t.message != null){
                    Log.e("TradeRequestError", t.message.toString())
                    unauthorisedToast(t.message.toString())
                }
            }
        })
    }

    private fun sendExtrasIfAny(token:String, tradeId:Int){
        val service: OtherNetworkCalls = RetrofitInstance.getRetrofitInstance()!!.create(OtherNetworkCalls::class.java)
        val tokenString = "Bearer $token"
        val beneficiaryList:List<SentBeneficiary> = EditTradeActivity.beneficiaries.toList()
        val mileStoneList:List<TradeMileStones> = EditTradeActivity.mileStones.toList()
        val notificationsList:List<NotificationGang> = EditTradeActivity.notificationGang.toList()
        val deliveryAddress:SentDeliveryAddress? = deliveryAddress
        val tradeExtrasRequest = CreateTradeExtrasRequest(tradeId, mileStoneList, beneficiaryList, notificationsList, deliveryAddress)
        val call = service.postEditTradeExtras(tokenString, tradeExtrasRequest)
        call.enqueue(object : Callback<CreateTradeExtrasResult> {
            override fun onResponse(call: Call<CreateTradeExtrasResult>, response: Response<CreateTradeExtrasResult>) {
                if (response.body() != null){
                    val result = response.body() as CreateTradeExtrasResult
                    mTransactionViewModel.insertTradesMilestones(result.milestones)
                    mTransactionViewModel.insertTradeBeneficiaries(result.beneficiaries)
                    mTransactionViewModel.insertTradeDeliveryAddress(result.deliveryAddress)
                    mTransactionViewModel.insertTradeNotifications(result.notificationGang)
                    showDialog()
                }else if (response.errorBody() != null){
                    val gson = GsonBuilder().create()
                    val errorResult: CreateTradeExtrasResult = gson.fromJson<CreateTradeExtrasResult>(response.errorBody()?.string(), CreateTradeExtrasResult::class.java)
                    Log.e("TradeExtraRequest", errorResult.error)
                    invalidTradeRequestToast(errorResult.error)
                }
            }

            override fun onFailure(call: Call<CreateTradeExtrasResult>, t: Throwable) {
                if (t.message != null){
                    Log.e("TradeExtraRequestError", t.message.toString())
                    unauthorisedToast(t.message.toString())
                }
            }
        })
    }

    private fun showDialog(){
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity!!)
        builder.setTitle(R.string.trade_success_alert_title)
                .setMessage(R.string.trade_success_text)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton(android.R.string.ok){_, _ ->
                    activity!!.finish()
                }
        val dialog = builder.create()
        dialog.show()
    }

    private fun unauthorisedToast(error:String?){
        Toast.makeText(activity, "$error ...Please logout and login again. You will be unable to make any transactions otherwise", Toast.LENGTH_SHORT).show()
        activity!!.finish()
    }

    private fun invalidTradeRequestToast(error:String?){
        val errorString = "$error ...Please recreate the trade"
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity!!)
        builder.setTitle(R.string.alert_title)
                .setMessage(errorString)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton(android.R.string.ok){_, _ ->
                    activity!!.finish()
                }
        val dialog = builder.create()
        dialog.show()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        fun onBackButtonPressed(fragment: Fragment)
        fun onNextPressed(fragment: Fragment)
    }

}
