package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.google.gson.annotations.SerializedName

data class BillingDetailsRequest (
        @SerializedName("bill_address_line_1")
        var billLine1:String,

        @SerializedName("bill_address_line_2")
        var billLine2:String,

        @SerializedName("bill_address_suburb")
        var billSuburb:String,

        @SerializedName("bill_address_city")
        var billCity:String,

        @SerializedName("bill_address_region")
        var billRegion:String,

        @SerializedName("bill_address_country")
        var billCountry:String,

        @SerializedName("bill_address_postcode")
        var billPostCode:String,

        @SerializedName("digital_address")
        var digitalAddress:String
)