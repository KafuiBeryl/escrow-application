package com.example.nit_appdev_002.escrowapplication.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.nit_appdev_002.escrowapplication.entities.*
import com.example.nit_appdev_002.escrowapplication.repositories.MiscRepository

class MiscViewModel(application: Application) : AndroidViewModel(application){
    private var miscRepository:MiscRepository

    init {
        miscRepository = MiscRepository(application)
    }

//    fun logOut(){
//        miscRepository.logOut()
//    }

    fun getBanks(): LiveData<List<String>> {
        return miscRepository.getBanks()
    }

    fun getIndustries():LiveData<List<String>>{
        return miscRepository.getIndustries()
    }

    fun getCompanyCategories(): LiveData<List<String>>{
        return miscRepository.getCompanyCategories()
    }

    fun getFeeSchedule(): LiveData<List<FeeSchedule>>{
        return miscRepository.getFeeSchedule()
    }

    fun getBankAccountTypes(): LiveData<List<String>>{
        return miscRepository.getBankAccountTypes()
    }

    fun getIdentificationTypes(): LiveData<List<String>>{
        return miscRepository.getIdentificationTypes()
    }

    fun getAdminFeeAllocation(): LiveData<List<String>>{
        return miscRepository.getAdminFeeAllocation()
    }

    fun getAgentAdminFeeAllocation(): LiveData<List<String>>{
        return miscRepository.getAgentAdminFeeAllocation()
    }

    fun getStandardFeeAllocation(): LiveData<List<String>>{
        return miscRepository.getStandardFeeAllocation()
    }

    fun getFeeTypes(): LiveData<List<String>>{
        return miscRepository.getFeeType()
    }

    fun getBeneficiaryTypes(): LiveData<List<String>>{
        return miscRepository.getBeneficiaryType()
    }

    fun getOccupations(): LiveData<List<String>>{
        return miscRepository.getOccupations()
    }

    fun insertBanks(banks: List<Bank>){
        if (!banks.isEmpty()){
            miscRepository.insertBanks(banks)
        }
    }

    fun insertCompanyCategories(categories:List<CompanyCategory>?){
        if (categories != null){
            miscRepository.insertCompanyCategories(categories)
        }
    }

    fun insertFeeSchedule(feeSchedule: List<FeeSchedule>?){
        if (feeSchedule != null){
            miscRepository.insertFeeSchedule(feeSchedule)
        }
    }

    fun insertIndustries(industries: List<Industry>?){
        if (industries != null){
            miscRepository.insertIndustries(industries)
        }
    }

    fun insertIdentityTypes(types:List<IdentificationType>?){
        if (types != null){
            miscRepository.insertIdentityTypes(types)
        }
    }

    fun insertBankAccountTypes(types:List<BankAccountType>?){
        if (types != null){
            miscRepository.insertBankAccountTypes(types)
        }
    }

    fun insertAdminFeeAllocation(types:List<AdminFeeAllocation>?){
        if (types != null){
            miscRepository.insertAdminFeeAllocation(types)
        }
    }

    fun insertStandardFeeAllocation(types:List<StandardFeeAllocation>?){
        if (types != null){
            miscRepository.insertStandardFeeAllocation(types)
        }
    }

    fun insertFeeType(types:List<FeeType>?){
        if (types != null){
            miscRepository.insertFeeType(types)
        }
    }

    fun insertBeneficiaryType(types:List<BeneficiaryType>?){
        if (types != null){
            miscRepository.insertBeneficiaryType(types)
        }
    }

    fun insertOccupations(types:List<Occupation>?){
        if (types != null){
            miscRepository.insertOccupation(types)
        }
    }
}