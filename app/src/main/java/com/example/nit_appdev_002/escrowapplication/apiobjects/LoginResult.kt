package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.example.nit_appdev_002.escrowapplication.entities.Occupation
import com.example.nit_appdev_002.escrowapplication.entities.User
import com.example.nit_appdev_002.escrowapplication.entities.UserTradeRoles
import com.google.gson.annotations.SerializedName

data class LoginResult (
        @SerializedName("token")
        var token:String,
        @SerializedName("token_expiry_date")
        var tokenExpiryDate:String,
        @SerializedName("user")
        var user:User?,
        @SerializedName("user_trade_roles")
        var userTrades:List<UserTradeRoles>?,
        @SerializedName("image")
        var userImage:String?,
        @SerializedName("occupation")
        var userOccupation:Occupation?,
        @SerializedName("error")
        var error:String?
)