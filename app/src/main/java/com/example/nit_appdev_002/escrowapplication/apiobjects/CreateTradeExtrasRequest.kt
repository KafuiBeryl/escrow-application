package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.example.nit_appdev_002.escrowapplication.entities.NotificationGang
import com.example.nit_appdev_002.escrowapplication.entities.TradeMileStones
import com.google.gson.annotations.SerializedName

data class CreateTradeExtrasRequest (
        @SerializedName("trade_id")
        var tradeId:Int,
        @SerializedName("milestones")
        var milestones:List<TradeMileStones>?,
        @SerializedName("beneficiaries")
        var beneficiaries:List<SentBeneficiary>?,
        @SerializedName("notificationGang")
        var notificationGang:List<NotificationGang>?,
        @SerializedName("deliveryAddress")
        var deliveryAddress: SentDeliveryAddress?
)