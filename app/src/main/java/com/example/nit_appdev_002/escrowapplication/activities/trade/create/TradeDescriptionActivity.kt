package com.example.nit_appdev_002.escrowapplication.activities.trade.create

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.utilities.PREF_HAS_AGENT
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.viewmodels.MiscViewModel
import kotlinx.android.synthetic.main.activity_trade_description.*
import kotlinx.android.synthetic.main.partial_agent_fee_description.*
import kotlinx.android.synthetic.main.toolbar.*

class TradeDescriptionActivity : AppCompatActivity() {

    companion object {
        @JvmStatic
        private var isAgentAdded:Boolean? = false

        @JvmStatic
        private var isProgressPayment:Boolean? = false

        @JvmStatic
        private lateinit var miscViewModel: MiscViewModel

        @JvmStatic
        private var selectedIndustryClassification:String? = null

        @JvmStatic
        private var selectedAdminAllocation:String? = null

        @JvmStatic
        private var selectedAgentAllocation:String? = null

        @JvmStatic
        private var selectedAgentFeeType:String? = null

        private lateinit var prefs:SharedPreferences
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trade_description)
        setSupportActionBar(custom_toolbar)

        //instantiate ViewModel object
        miscViewModel = ViewModelProviders.of(this).get(MiscViewModel::class.java)

        //instantiate shared preferences
        prefs = PreferenceHelper.defaultPrefs(this)

        progress_lock_linear_layout.visibility = View.VISIBLE


        isAgentAdded = prefs[PREF_HAS_AGENT]

        setSpinnerData()

        btn_back.setOnClickListener {
            isProgressPayment = false
            finish()
        }

        btn_next.setOnClickListener {
            startActivity(Intent(this, TradeBeneficiariesActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()

        checkbox_is_progress.setOnCheckedChangeListener { _, isChecked ->
            when (isChecked){
                true -> {
                    isProgressPayment = true
                    progress_lock_linear_layout.visibility = View.GONE
                }
                false -> {
                    isProgressPayment = false
                    progress_lock_linear_layout.visibility = View.VISIBLE
                }
            }
        }

        isAgentAdded = prefs[PREF_HAS_AGENT]
        if (isAgentAdded!!){
            agent_description_stub.layoutResource = R.layout.partial_agent_fee_description
            agent_description_stub.inflate()

            setAgentSpinners()
        }
    }

    private fun setSpinnerData(){
        //set the adapter for the spinners
        val industryArray = ArrayList<String>()
        spinner_industry_classification.drawingCacheBackgroundColor = ResourcesCompat.getColor(resources, R.color.colorPrimary, null)
        val industryAdapter = ArrayAdapter(this, R.layout.simple_spinner_item, industryArray)
        industryAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_industry_classification.adapter = industryAdapter

        val adminAllocationArray = ArrayList<String>()
        val adminAllocationAdapter = ArrayAdapter(this, R.layout.simple_spinner_item, adminAllocationArray)
        adminAllocationAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_admin_allocation.adapter = adminAllocationAdapter

        //set on click listeners to spinners
        spinner_admin_allocation.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                selectedAdminAllocation = parent.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedAdminAllocation = null
            }
        }

        spinner_industry_classification.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                selectedIndustryClassification = parent.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedIndustryClassification = null
            }
        }

        miscViewModel.getIndustries().observe(this, Observer {
            industries -> if (!industries!!.isEmpty()){
            for (industry in industries){
                industryArray.add(industry)
            }
        }
        })

        when (isAgentAdded) {
            true -> {
                miscViewModel.getAgentAdminFeeAllocation().observe(this, Observer {
                    allocates -> if (!allocates!!.isEmpty()){
                    for (allocate in allocates){
                        adminAllocationArray.add(allocate)
                    }
                }
                })
            }
            false -> {
                miscViewModel.getAdminFeeAllocation().observe(this, Observer {
                    allocates -> if (!allocates!!.isEmpty()){
                    for (allocate in allocates){
                        adminAllocationArray.add(allocate)
                    }
                }
                })
            }
        }
    }

    private fun setAgentSpinners(){
        val agentAllocationArray = ArrayList<String>()
        val agentAllocationAdapter = ArrayAdapter(this, R.layout.simple_spinner_item, agentAllocationArray)
        agentAllocationAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_agent_allocation.adapter = agentAllocationAdapter

        spinner_agent_allocation.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                selectedAgentAllocation = parent.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedAgentAllocation = null
            }
        }

        miscViewModel.getStandardFeeAllocation().observe(this, Observer {
            allocates -> if (!allocates!!.isEmpty()){
            for (allocate in allocates){
                agentAllocationArray.add(allocate)
            }
        }
        })

        val agentFeeTypeArray = ArrayList<String>()
        val agentFeeTypeAdapter = ArrayAdapter(this, R.layout.simple_spinner_item, agentFeeTypeArray)
        agentFeeTypeAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_agent_fee_type.adapter = agentFeeTypeAdapter

        spinner_agent_fee_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                selectedAgentFeeType = parent.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedAgentFeeType = null
            }
        }

        miscViewModel.getFeeTypes().observe(this, Observer {
            feeTypes -> if (!feeTypes!!.isEmpty()){
            for (feeType in feeTypes){
                agentFeeTypeArray.add(feeType)
            }
        }
        })
    }
}
