package com.example.nit_appdev_002.escrowapplication.utilities

class GeneralHelper {
    companion object {
        @JvmStatic
        fun formatAmountToString(amount: Long?):String?{
            if (amount != null){
                val actualAmount:Double? = amount.toDouble()/100
                return "$actualAmount GH¢"
            }else{
                return null
            }
        }

        @JvmStatic
        fun formatAmount(amount: Long?):String?{
            if (amount != null){
                val actualAmount:Double? = amount.toDouble()/100
                return "$actualAmount"
            }else{
                return null
            }
        }

        @JvmStatic
        fun formatBoolean(boolean: Boolean?):String?{
            var string =""
            when (boolean){
                true -> string = "Yes"
                false -> string = "No"
            }
            return  string
        }

        @JvmStatic
        fun formatAgentFee(feeType: String?, feeUncalculated:Long?, feeCalculated:Long?):String?{
            var string =""
            val feeUncalculatedDouble = feeUncalculated!!.toDouble()/100
            val feeCalculatedDouble = feeCalculated!!.toDouble()/100
            when (feeType){
                "Fixed Fee" -> string = "$feeCalculatedDouble GH¢"
                "Percentage (%)" -> string = "$feeUncalculatedDouble% of total amount which is $feeCalculatedDouble GH¢"
            }
            return  string
        }
    }
}