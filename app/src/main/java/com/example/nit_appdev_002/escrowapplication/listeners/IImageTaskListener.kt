package com.example.nit_appdev_002.escrowapplication.listeners

import android.graphics.Bitmap

interface IImageTaskListener {
    fun onComplete(compressed:String)
    fun onError(error:Throwable)
}