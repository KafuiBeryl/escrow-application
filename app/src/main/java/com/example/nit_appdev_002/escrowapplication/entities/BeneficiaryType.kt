package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "beneficiary_type")
data class BeneficiaryType (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "beneficiaryTypeId")
        @SerializedName("id")
        var beneficiaryTypeId:Int,

        @ColumnInfo(name = "beneficiary_type")
        @SerializedName("beneficiary_type")
        var beneficiaryType:String
)