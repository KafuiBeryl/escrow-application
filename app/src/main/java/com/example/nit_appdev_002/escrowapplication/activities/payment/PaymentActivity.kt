package com.example.nit_appdev_002.escrowapplication.activities.payment

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.adapters.PaymentPagerAdapter
import com.example.nit_appdev_002.escrowapplication.fragments.payment.PaymentTypeFragment
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.please_wait.*
import kotlinx.android.synthetic.main.toolbar.*

class PaymentActivity : AppCompatActivity(), PaymentTypeFragment.OnFragmentInteractionListener {

    private val adapter = PaymentPagerAdapter(supportFragmentManager)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        setSupportActionBar(custom_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        please_wait_group.visibility = View.GONE
        payment_view_pager.adapter = adapter
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onNextPressed(fragment: Fragment, selectedMode: String) {
        when (fragment){
            is PaymentTypeFragment -> {
                if (selectedMode == "Card"){
                    adapter.onNextPressed(PaymentTypeFragment(), selectedMode)
                    payment_view_pager.setCurrentItem(1, true)
                }else{

                    adapter.onNextPressed(PaymentTypeFragment(), selectedMode)
                    payment_view_pager.setCurrentItem(2, true)
                }
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && (payment_view_pager.currentItem == 1 || payment_view_pager.currentItem == 2)){
            payment_view_pager.setCurrentItem(0, true)
            return true
        }else{
            return super.onKeyDown(keyCode, event)
        }
    }
}
