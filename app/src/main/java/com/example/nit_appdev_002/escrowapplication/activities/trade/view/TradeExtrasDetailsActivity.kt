package com.example.nit_appdev_002.escrowapplication.activities.trade.view

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.ViewStub
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.entities.Beneficiary
import com.example.nit_appdev_002.escrowapplication.entities.DeliveryAddress
import com.example.nit_appdev_002.escrowapplication.entities.NotificationGang
import com.example.nit_appdev_002.escrowapplication.entities.TradeMileStones
import com.example.nit_appdev_002.escrowapplication.utilities.GeneralHelper
import com.example.nit_appdev_002.escrowapplication.utilities.INTENT_SELECTED_EXTRA
import com.example.nit_appdev_002.escrowapplication.utilities.PREF_TRADE_SELECTED
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.viewmodels.TransactionViewModel
import kotlinx.android.synthetic.main.activity_trade_extras_details.*
import kotlinx.android.synthetic.main.partial_beneficiary_details.*
import kotlinx.android.synthetic.main.partial_delivery_details.*
import kotlinx.android.synthetic.main.partial_notification_details.*
import kotlinx.android.synthetic.main.partial_progress_details.*
import kotlinx.android.synthetic.main.toolbar.*

class TradeExtrasDetailsActivity : AppCompatActivity() {

    companion object {
        @JvmStatic
        private lateinit var mTransactionViewModel: TransactionViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trade_extras_details)
        setSupportActionBar(custom_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        //initialize the view model
        mTransactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel::class.java)

        if (savedInstanceState == null){
            val selectedShuffle = intent.getStringExtra(INTENT_SELECTED_EXTRA)
            shuffleMaster(selectedShuffle)
        }else{
            val selectedShuffle = savedInstanceState.getString(INTENT_SELECTED_EXTRA)
            shuffleMaster(selectedShuffle!!)
        }
    }

    private fun shuffleMaster(selectedShuffle:String){
        val prefs = PreferenceHelper.defaultPrefs(this)
        val tradeId:Int? = prefs[PREF_TRADE_SELECTED]
        when (selectedShuffle){
            "Beneficiary" -> {
                mTransactionViewModel.getTradBeneficiaries(tradeId!!).observe(this, Observer {
                    beneficiaries ->
                    if (!beneficiaries.isEmpty()){
                        val stub = ViewStub(this)
                        val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                        stub.layoutParams = layoutParams
                        stub.layoutResource = R.layout.partial_beneficiary_details
                        details_container.addView(stub)
                        stub.inflate()

                        for (i in 1 .. beneficiaries.count()){
                            showBeneficiaries(beneficiaries[i-1])
                        }
                    }
                })
            }
            "MileStone" -> {
                mTransactionViewModel.getTradeMilestones(tradeId!!).observe(this, Observer {
                    milestones ->
                    if (!milestones.isEmpty()){
                        val stub = ViewStub(this)
                        val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                        stub.layoutParams = layoutParams
                        stub.layoutResource = R.layout.partial_progress_details
                        details_container.addView(stub)
                        stub.inflate()

                        for (i in 1 .. milestones.count()){
                            showProgressPayments(milestones[i-1])
                        }
                    }
                })
            }
            "Notification" -> {
                mTransactionViewModel.getTradeNotificationGang(tradeId!!).observe(this, Observer {
                    gang ->
                    if (!gang.isEmpty()){
                        val stub = ViewStub(this)
                        val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                        stub.layoutParams = layoutParams
                        stub.layoutResource = R.layout.partial_notification_details
                        details_container.addView(stub)
                        stub.inflate()

                        for (i in 1 .. gang.count()){
                            showNotificationGang(gang[i-1])
                        }
                    }
                })
            }
            "Delivery" -> {
                mTransactionViewModel.getTradeDeliveryAddress(tradeId!!).observe(this, Observer {
                    address ->
                    if (address != null){
                        val stub = ViewStub(this)
                        val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                        stub.layoutParams = layoutParams
                        stub.layoutResource = R.layout.partial_delivery_details
                        details_container.addView(stub)
                        stub.inflate()

                        showDeliveryAddress(address)
                    }
                })
            }
        }
    }

    private fun showBeneficiaries(beneficiary: Beneficiary){
        val bigLinearLayout = createBigLinearLayout()

        val emailTitleTextView = createBoldTextView()
        emailTitleTextView.text = resources.getString(R.string.trade_details_beneficiary_email)
        emailTitleTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val emailTextView = createNormalTextView()
        emailTextView.text = beneficiary.email
        emailTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val beneficiaryTypeTitle = createBoldTextView()
        beneficiaryTypeTitle.text = resources.getString(R.string.trade_details_beneficiary_type)
        beneficiaryTypeTitle.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val beneficiaryTypeTextView = createNormalTextView()
        beneficiaryTypeTextView.text = beneficiary.beneficiaryType.beneficiaryType
        beneficiaryTypeTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val beneficiaryFeeTitle = createBoldTextView()
        beneficiaryFeeTitle.text = resources.getString(R.string.trade_details_beneficiary_fee)
        beneficiaryFeeTitle.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val beneficiaryFeeTextView = createNormalTextView()
        beneficiaryFeeTextView.text = GeneralHelper.formatAgentFee(beneficiary.beneficiaryFeeType.feeType, beneficiary.unCalculatedBeneficiaryFee, beneficiary.beneficiaryFee)
        beneficiaryFeeTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val beneficiaryPayeeTitle = createBoldTextView()
        beneficiaryPayeeTitle.text = resources.getString(R.string.trade_details_escrow_allocation)
        beneficiaryPayeeTitle.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val beneficiaryPayeeTextView = createNormalTextView()
        beneficiaryPayeeTextView.text = beneficiary.beneficiaryPayee.standardPayeeAllocation
        beneficiaryPayeeTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        bigLinearLayout.addView(emailTitleTextView)
        bigLinearLayout.addView(emailTextView)
        bigLinearLayout.addView(beneficiaryTypeTitle)
        bigLinearLayout.addView(beneficiaryTypeTextView)
        bigLinearLayout.addView(beneficiaryFeeTitle)
        bigLinearLayout.addView(beneficiaryFeeTextView)
        bigLinearLayout.addView(beneficiaryPayeeTitle)
        bigLinearLayout.addView(beneficiaryPayeeTextView)

        beneficiary_container.addView(bigLinearLayout)
    }

    private fun showProgressPayments(tradeMileStones: TradeMileStones){
        val bigLinearLayout = createBigLinearLayout()

        val nameTitleTextView = createBoldTextView()
        nameTitleTextView.text = resources.getString(R.string.trade_details_milestone_name)
        nameTitleTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val nameTextView = createNormalTextView()
        nameTextView.text = tradeMileStones.milestoneName
        nameTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val descriptionTitle = createBoldTextView()
        descriptionTitle.text = resources.getString(R.string.trade_details_description)
        descriptionTitle.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val descriptionTextView = createNormalTextView()
        descriptionTextView.text = tradeMileStones.description
        descriptionTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val milestoneFeeTitle = createBoldTextView()
        milestoneFeeTitle.text = resources.getString(R.string.trade_details_milestone_amount)
        milestoneFeeTitle.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val milestoneFeeTextView = createNormalTextView()
        milestoneFeeTextView.text = GeneralHelper.formatAmountToString(tradeMileStones.grossAmount)
        milestoneFeeTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        bigLinearLayout.addView(nameTitleTextView)
        bigLinearLayout.addView(nameTextView)
        bigLinearLayout.addView(descriptionTitle)
        bigLinearLayout.addView(descriptionTextView)
        bigLinearLayout.addView(milestoneFeeTitle)
        bigLinearLayout.addView(milestoneFeeTextView)

        progress_container.addView(bigLinearLayout)
    }

    private fun showNotificationGang(notificationGang: NotificationGang){
        val bigLinearLayout = createBigLinearLayout()

        val emailTitleTextView = createBoldTextView()
        emailTitleTextView.text = resources.getString(R.string.register_email)
        emailTitleTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val emailTextView = createNormalTextView()
        emailTextView.text = notificationGang.email
        emailTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val firstNameTitle = createBoldTextView()
        firstNameTitle.text = resources.getString(R.string.profile_first_name)
        firstNameTitle.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val firstNameTextView = createNormalTextView()
        firstNameTextView.text = notificationGang.firstName
        firstNameTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val lastNameTitle = createBoldTextView()
        lastNameTitle.text = resources.getString(R.string.profile_last_name)
        lastNameTitle.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val lastNameTextView = createNormalTextView()
        lastNameTextView.text = notificationGang.lastName
        lastNameTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        bigLinearLayout.addView(emailTitleTextView)
        bigLinearLayout.addView(emailTextView)
        bigLinearLayout.addView(firstNameTitle)
        bigLinearLayout.addView(firstNameTextView)
        bigLinearLayout.addView(lastNameTitle)
        bigLinearLayout.addView(lastNameTextView)

        notification_container.addView(bigLinearLayout)
    }

    private fun showDeliveryAddress(deliveryAddress: DeliveryAddress){
        val bigLinearLayout = createBigLinearLayout()

        val addressLine1TextView = createNormalTextView()
        addressLine1TextView.text = deliveryAddress.houseNumber
        addressLine1TextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val addressLine2TextView = createNormalTextView()
        addressLine2TextView.text = deliveryAddress.streetLine2
        addressLine2TextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val suburbTextView = createNormalTextView()
        suburbTextView.text = deliveryAddress.suburb
        suburbTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val cityTextView = createNormalTextView()
        cityTextView.text = deliveryAddress.townOrCity
        cityTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val digitalAddressTextView = createNormalTextView()
        digitalAddressTextView.text = deliveryAddress.digitalAddress
        digitalAddressTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        val commentsTextView = createNormalTextView()
        commentsTextView.text = deliveryAddress.comments
        commentsTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

        bigLinearLayout.addView(addressLine1TextView)
        bigLinearLayout.addView(addressLine2TextView)
        bigLinearLayout.addView(suburbTextView)
        bigLinearLayout.addView(cityTextView)
        bigLinearLayout.addView(digitalAddressTextView)
        bigLinearLayout.addView(commentsTextView)

        delivery_container
    }

    private fun createBigLinearLayout():LinearLayout{
        val bigLinearLayout = LinearLayout(this)
        val linearLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        linearLayoutParams.bottomMargin = 40
        bigLinearLayout.layoutParams = linearLayoutParams
        bigLinearLayout.orientation = LinearLayout.VERTICAL
        return bigLinearLayout
    }

    private fun createBoldTextView(): TextView {
        //craft email textview
        val textView = TextView(this)
        textView.setPadding(10, 0, 10, 0)
//        textView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        if (Build.VERSION.SDK_INT < 23){
            textView.setTextAppearance(this, android.R.style.TextAppearance_Small)
        }else{
            textView.setTextAppearance(android.R.style.TextAppearance_Small)
        }
        val textViewTypeface = ResourcesCompat.getFont(this, R.font.montserrat_semibold)
        textView.typeface = textViewTypeface
        val textViewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        textViewLayoutParams.bottomMargin = 5
        textViewLayoutParams.gravity = Gravity.CENTER
        textView.layoutParams = textViewLayoutParams
        return textView
    }

    private fun createNormalTextView(): TextView {
        //craft email textview
        val textView = TextView(this)
        textView.setPadding(10, 0, 10, 0)
//        textView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        if (Build.VERSION.SDK_INT < 23){
            textView.setTextAppearance(this, android.R.style.TextAppearance_Small)
        }else{
            textView.setTextAppearance(android.R.style.TextAppearance_Small)
        }
        val textViewTypeface = ResourcesCompat.getFont(this, R.font.montserrat_regular)
        textView.typeface = textViewTypeface
        val textViewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        textViewLayoutParams.bottomMargin = 5
        textViewLayoutParams.gravity = Gravity.CENTER
        textView.layoutParams = textViewLayoutParams
        return textView
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onNewIntent(intent: Intent?) {
        if (intent != null){
            setIntent(intent)
        }
    }
}
