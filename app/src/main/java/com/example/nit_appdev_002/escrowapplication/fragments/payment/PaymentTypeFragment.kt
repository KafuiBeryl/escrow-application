package com.example.nit_appdev_002.escrowapplication.fragments.payment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.nit_appdev_002.escrowapplication.R
import kotlinx.android.synthetic.main.fragment_payment_type.*


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PaymentTypeFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 *
 */
class PaymentTypeFragment : Fragment(), View.OnClickListener {
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment_type, container, false)
    }



    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onResume() {
        super.onResume()
        btn_card_payment.setOnClickListener(this)
        btn_mtn_mobile_money.setOnClickListener(this)
        btn_vodafone_cash.setOnClickListener(this)
        btn_tigo_cash.setOnClickListener(this)
        btn_airtel_money.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btn_card_payment.setOnClickListener(null)
        btn_mtn_mobile_money.setOnClickListener(null)
        btn_vodafone_cash.setOnClickListener(null)
        btn_tigo_cash.setOnClickListener(null)
        btn_airtel_money.setOnClickListener(null)
    }

    override fun onClick(v: View?) {
        when (v?.id){
            R.id.btn_card_payment -> {
                if (listener != null){
                    listener?.onNextPressed(this, "Card")
                }
            }
            R.id.btn_mtn_mobile_money -> {
                if (listener != null){
                    listener?.onNextPressed(this, "MTN")
                }
            }
            R.id.btn_vodafone_cash -> {
                if (listener != null){
                    listener?.onNextPressed(this, "Vodafone")
                }
            }
            R.id.btn_tigo_cash -> {
                if (listener != null){
                    listener?.onNextPressed(this, "Tigo")
                }
            }
            R.id.btn_airtel_money -> {
                if (listener != null){
                    listener?.onNextPressed(this, "Airtel")
                }
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        fun onNextPressed(fragment: Fragment, selectedMode:String)
    }

}
