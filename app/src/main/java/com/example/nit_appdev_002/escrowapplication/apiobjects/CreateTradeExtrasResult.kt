package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.example.nit_appdev_002.escrowapplication.entities.Beneficiary
import com.example.nit_appdev_002.escrowapplication.entities.DeliveryAddress
import com.example.nit_appdev_002.escrowapplication.entities.NotificationGang
import com.example.nit_appdev_002.escrowapplication.entities.TradeMileStones
import com.google.gson.annotations.SerializedName

data class CreateTradeExtrasResult (
        @SerializedName("milestones")
        var milestones:List<TradeMileStones>?,
        @SerializedName("beneficiaries")
        var beneficiaries:List<Beneficiary>?,
        @SerializedName("notifications")
        var notificationGang:List<NotificationGang>?,
        @SerializedName("delivery")
        var deliveryAddress: DeliveryAddress?,
        @SerializedName("error")
        var error:String?
)