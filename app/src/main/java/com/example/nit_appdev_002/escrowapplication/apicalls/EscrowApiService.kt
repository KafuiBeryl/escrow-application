package com.example.nit_appdev_002.escrowapplication.apicalls

import com.example.nit_appdev_002.escrowapplication.apiobjects.LoginRequest
import com.example.nit_appdev_002.escrowapplication.apiobjects.LoginResult
import com.example.nit_appdev_002.escrowapplication.apiobjects.RegisterResult
import com.example.nit_appdev_002.escrowapplication.utilities.API_LOGIN
import com.example.nit_appdev_002.escrowapplication.utilities.API_REGISTER
import retrofit2.Call
import retrofit2.http.*

interface EscrowApiService {
    @FormUrlEncoded
    @POST(API_REGISTER)
    fun register(@Field("email") email:String,
                 @Field("password") password:String,
                 @Field("confirm_password") confirmPassword:String): Call<RegisterResult>


    @POST(API_LOGIN)
    fun login(@Body login:LoginRequest) : Call<LoginResult>
}