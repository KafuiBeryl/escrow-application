package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "company_category")
data class CompanyCategory(
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "companyCategoryId")
        @SerializedName("id")
        var companyCategoryId:Int,

        @ColumnInfo(name = "category")
        @SerializedName("category")
        var category:String
)