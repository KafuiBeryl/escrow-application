package com.example.nit_appdev_002.escrowapplication.utilities

//shared preferences
const val PREF_AGREE = "Agreed"

const val PREF_IS_COMPANY = "company_account"

const val PREF_TOKEN = "token"

const val PREF_TOKEN_EXPIRY = "token_expiry_date"

const val PREF_USER_ID = "user_id"

const val PREF_IS_PROCESS_PAYMENT = "process_payment"

const val PREF_HAS_AGENT = "has_agent"

const val PREF_ROLE_SELECTED = "selected_role"

const val PREF_TRADE_SELECTED = "selected_trade"

//@Volatile var isDownloading:Boolean = false

//intent extras
const val INTENT_TRADE_PARTY = "trade_party_intent"

const val INTENT_SELECTED_TRADE_ID = "selected_trade_id_intent"

const val INTENT_SELECTED_EXTRA = "selected_extra_intent"

const val INTENT_PAYMENT_REPLY = "payment_reply_intent"
//select image intent
const val RC_SELECT_IMAGE = 2

//API Calls
//url
const val BASE_URL = "http://10.0.2.2:8000/api/"

//const val BASE_URL = "http://newsapi.rate233.com/api/"

//register endpoint
const val API_REGISTER = "register"
//log da fak in
const val API_LOGIN = "login"
//get banks, fee schedule, company categories , industry classifications
const val API_GET_MISC = "home/misc/get"
//post details to complete account
const val API_SEND_PERSONAL_DETAILS = "account"
//upload account image
const val API_UPLOAD_IMAGE = "account/image"
//initialize a trade
const val API_START_BASIC_TRADE = "trade/initialize"
//edit a trade
const val API_EDIT_TRADE = "trade/edit"
//add trade extras
const val API_ADD_TRADE_EXTRAS = "trade/initialize/extras"
//edit trade extras
const val API_EDIT_TRADE_EXTRAS = "trade/edit/extras"
//pay via mobile money
const val API_PAY_VIA_MOBILE_MONEY = "make/payment/mobile"
//pay via card
const val API_PAY_VIA_BANK_CARD = "make/payment/card"
//get single trade by id
const val API_GET_SINGLE_TRADE = "trade/get"
//get all trade extras
const val API_GET_SINGLE_TRADE_EXTRAS = "trade/get/extras"
//chat message
const val API_CHAT_SEND_MESSAGE = "negotiate/chat/send"
//send invitation to all involved parties
const val API_SEND_TRADE_INVITATION = "invite/send"
//send user's decision as pertains to a particular trade
const val API_SEND_USER_TRADE_DECISION = "trade/user/decision"
//get all trades the user is involved in
const val API_GET_USER_INVOLVED_TRADES = "trade/get/all"


//pin lock events
const val PIN_FAILED = 101

const val PIN_SUCCEEDED = 201

const val PIN_FORGOT = 301

const val REQUEST_FIRST_RUN_PIN = 4

//pusher values
const val PUSHER_CLUSTER = "eu"
const val PUSHER_APP_KEY = "20d4a9e7e9b5ad04b3b3"

//tags
const val ERRORTAG = "ChatActivity"
