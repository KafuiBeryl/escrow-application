package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.google.gson.annotations.SerializedName

data class ModelError(
        @SerializedName("error")
        var error:String
)