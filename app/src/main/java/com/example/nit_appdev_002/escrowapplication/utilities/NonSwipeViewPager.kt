package com.example.nit_appdev_002.escrowapplication.utilities

import android.content.Context
import android.util.AttributeSet
import android.view.animation.DecelerateInterpolator
import android.widget.Scroller
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager


class NonSwipeViewPager : ViewPager {

    private var swipeEnabled:Boolean

    constructor(context: Context) : super(context) {
        setMyScroller()
        this.swipeEnabled = false
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setMyScroller()
        this.swipeEnabled = false
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        // Never allow swiping to switch between pages
        if (this.swipeEnabled){
            return super.onInterceptTouchEvent(event)
        }
        return false
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        // Never allow swiping to switch between pages
        if (this.swipeEnabled){
            return super.onTouchEvent(event)
        }
        return false
    }

    override fun performClick(): Boolean {
        if (swipeEnabled){
            return super.performClick()
        }
        return false
    }

    fun setPagingEnabled(enabled:Boolean){
        this.swipeEnabled = enabled
    }

    //down one is added for smooth scrolling

    private fun setMyScroller() {
        try {
            val viewpager = ViewPager::class.java
            val scroller = viewpager.getDeclaredField("mScroller")
            scroller.isAccessible = true
            scroller.set(this, MyScroller(context))
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    inner class MyScroller(context: Context) : Scroller(context, DecelerateInterpolator()) {

        override fun startScroll(startX: Int, startY: Int, dx: Int, dy: Int, duration: Int) {
            super.startScroll(startX, startY, dx, dy, 350 /*1 secs*/)
        }
    }
}