package com.example.nit_appdev_002.escrowapplication.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Update
import com.example.nit_appdev_002.escrowapplication.entities.*

@Dao
interface TransactionDao {
    @Insert(onConflict = REPLACE)
    fun insertTrade(trade:Trade)

    @Insert(onConflict = REPLACE)
    fun insertMilestone(tradeMileStones: TradeMileStones)

    @Insert(onConflict = REPLACE)
    fun insertTradeNotification(notificationGang: NotificationGang)

    @Insert(onConflict = REPLACE)
    fun insertTradeBeneficiaries(beneficiary: Beneficiary)

    @Insert(onConflict = REPLACE)
    fun insertTradeDeliveryAddress(deliveryAddress: DeliveryAddress)

    @Update
    fun updateTrade(trade: Trade)

    @Update
    fun updateMilestone(tradeMileStones: TradeMileStones)

    @Update
    fun updateTradeNotification(notificationGang: NotificationGang)

    @Update
    fun updateTradeBeneficiary(beneficiary: Beneficiary)

    @Update
    fun updateTradeDeliveryAddress(deliveryAddress: DeliveryAddress)

    @Query("select * from trade where id = :id")
    fun getSingleTrade(id:Int):LiveData<Trade>

    @Query("select * from trade")
    fun getTrades(): LiveData<List<Trade>>

    @Query("select * from trade_milestones where trade_id = :tradeId")
    fun getTradeMilestones(tradeId:Int):LiveData<List<TradeMileStones>>

    @Query("select * from notification_gang where trade_id = :tradeId")
    fun getTradeNotificationGang(tradeId:Int):LiveData<List<NotificationGang>>

    @Query("select * from beneficiary where trade_id = :tradeId")
    fun getTradBeneficiaries(tradeId:Int):LiveData<List<Beneficiary>>

    @Query("select * from delivery_address where trade_id = :tradeId")
    fun getTradeDeliveryAddress(tradeId:Int):LiveData<DeliveryAddress>

    @Query("delete from trade")
    fun nukeTradeTable()

    @Query("delete from trade_milestones")
    fun nukeTradeMileStonesTable()

    @Query("delete from notification_gang")
    fun nukeTradeNotificationsTable()

    @Query("delete from beneficiary")
    fun nukeTradeBeneficiariesTable()

    @Query("delete from delivery_address")
    fun nukeTradeDeliveryAddressTable()
}