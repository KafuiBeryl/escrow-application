package com.example.nit_appdev_002.escrowapplication.activities.profile

import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.authentication.LoginActivity
import com.example.nit_appdev_002.escrowapplication.apicalls.OtherNetworkCalls
import com.example.nit_appdev_002.escrowapplication.apicalls.RetrofitInstance
import com.example.nit_appdev_002.escrowapplication.apiobjects.BankingDetailsRequest
import com.example.nit_appdev_002.escrowapplication.apiobjects.ProfileResult
import com.example.nit_appdev_002.escrowapplication.utilities.*
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.viewmodels.MiscViewModel
import com.example.nit_appdev_002.escrowapplication.viewmodels.UserViewModel
import com.github.omadahealth.lollipin.lib.PinCompatActivity
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_banking_details.*
import kotlinx.android.synthetic.main.please_wait.*
import kotlinx.android.synthetic.main.toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BankingDetailsActivity : PinCompatActivity()  {

    companion object {
        @JvmStatic
        private lateinit var miscViewModel: MiscViewModel

        @JvmStatic
        private lateinit var mUserViewModel: UserViewModel

        @JvmStatic
        private var selectedBank:String? = null

        @JvmStatic
        private var selectedAccountType:String? = null
    }
    private lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_banking_details)
        setSupportActionBar(custom_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        //instantiate ViewModel object
        miscViewModel = ViewModelProviders.of(this).get(MiscViewModel::class.java)
        //instantiate ViewModel object
        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        prefs = PreferenceHelper.defaultPrefs(this)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        setUpSpinners()
    }

    private fun setUpSpinners(){
        please_wait_group.visibility = View.GONE

        val bankArray = ArrayList<String>()
        val bankAdapter = ArrayAdapter(this, R.layout.simple_spinner_item, bankArray)
        bankAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)

        //set the adapter for the spinner
        spinner_bank_name.adapter = bankAdapter

        spinner_bank_name.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                selectedBank = parent.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedBank = null
            }
        }


        val accountArray = ArrayList<String>()
        val accountAdapter = ArrayAdapter(this,  R.layout.simple_spinner_item, accountArray)
        accountAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)

        //set the adapter for the spinner
        spinner_banking_account_type.adapter = accountAdapter

        spinner_banking_account_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                selectedAccountType = parent.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedAccountType = null
            }
        }


        miscViewModel.getBanks().observe(this, Observer {
            banks -> if (!banks!!.isEmpty()){
            for (bank in banks){
                bankArray.add(bank)
            }
            bankAdapter.notifyDataSetChanged()
        }
        })

        miscViewModel.getBankAccountTypes().observe(this, Observer {
            accounts -> if (!accounts!!.isEmpty()){
            for (account in accounts){
                accountArray.add(account)
            }
            accountAdapter.notifyDataSetChanged()
        }
        })

        banking_details_submit_btn.setOnClickListener {
            banking_linear_layout.visibility = View.GONE
            please_wait_group.visibility = View.VISIBLE
            validateEntry()
        }

        setValues(bankAdapter, accountAdapter)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == PIN_FAILED || resultCode == PIN_FORGOT){
            mUserViewModel.logoutThings()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setValues(bankAdapter:ArrayAdapter<String>, accountAdapter:ArrayAdapter<String>){
//        val prefs = PreferenceHelper.defaultPrefs(this)
        //get the user id from the shared preferences
        val userId:Int? = prefs[PREF_USER_ID]
        if (userId != null){
            mUserViewModel.getUser(userId).observe(this, Observer {
                user ->
                txt_banking_account_name.text = Editable.Factory.getInstance().newEditable(user?.accountName ?: "")
                txt_banking_account_number.text = Editable.Factory.getInstance().newEditable(user?.accountNumber ?: "")
                txt_banking_branch.text = Editable.Factory.getInstance().newEditable(user?.bankBranch ?: "")
                if (user?.bank_id != null){
                    val bank = user.bank_id
                    val position = bankAdapter.getPosition(bank!!.bankName)
                    spinner_bank_name.setSelection(position)
                }

                if (user?.account_type != null){
                    val bankAccountType = user.account_type
                    val position = accountAdapter.getPosition(bankAccountType!!.accountType)
                    spinner_banking_account_type.setSelection(position)
                }
            })
        }
    }

    private fun showDialogueBox(errorMessage:String?){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.alert_title)
                .setMessage(errorMessage)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setNeutralButton(android.R.string.ok, DialogInterface.OnClickListener {
                    dialog, which ->
                    dialog.cancel()
                })
        val dialog = builder.create()
        dialog.show()
    }

    private fun validateEntry(){
        if (TextUtils.isEmpty(txt_banking_account_name.text) || TextUtils.isEmpty(txt_banking_account_number.text)
                || TextUtils.isEmpty(txt_banking_branch.text) || selectedAccountType == null
                || selectedBank == null ){
            banking_linear_layout.visibility = View.VISIBLE
            please_wait_group.visibility = View.GONE
            Toast.makeText(this, R.string.login_empty_text, Toast.LENGTH_LONG).show()
        }else{
            postRequest()
        }
    }

    private fun postRequest(){
        val bankName = selectedBank!!
        val accountName = txt_banking_account_name.text.toString()
        val accountNumber = txt_banking_account_number.text.toString()
        val accountType = selectedAccountType!!
        val branch = txt_banking_branch.text.toString()

        val bankingDetailsRequest = BankingDetailsRequest(bankName, accountNumber, accountName,
                branch, accountType)
        retrofitCall(bankingDetailsRequest)
    }

    private fun retrofitCall(bankingDetailsRequest: BankingDetailsRequest){
//        val prefs = PreferenceHelper.defaultPrefs(this)
        val token:String? = prefs[PREF_TOKEN]
        if (token != null){
            val service: OtherNetworkCalls = RetrofitInstance.getRetrofitInstance()!!.create(OtherNetworkCalls::class.java)
            val tokenString = "Bearer $token"
            val call = service.postBankingDetails(tokenString, bankingDetailsRequest)
            call.enqueue(object : Callback<ProfileResult>{
                override fun onResponse(call: Call<ProfileResult>?, response: Response<ProfileResult>?) {
                    if (response?.body() != null){
                        val result = response.body() as ProfileResult
                        mUserViewModel.updateUser(result.user)
                        banking_linear_layout.visibility = View.VISIBLE
                        please_wait_group.visibility = View.GONE
                        startActivity(Intent(this@BankingDetailsActivity, ProfileActivity::class.java))
                        finish()
                    }else if (response?.errorBody() != null){
                        val gson = GsonBuilder().create()
                        val errorResult:ProfileResult = gson.fromJson<ProfileResult>(response.errorBody()?.string(), ProfileResult::class.java)
                        banking_linear_layout.visibility = View.VISIBLE
                        please_wait_group.visibility = View.GONE
                        showDialogueBox(errorResult.error)
                    }
                }

                override fun onFailure(call: Call<ProfileResult>?, t: Throwable?) {
                    if (t?.message != null){
                        banking_linear_layout.visibility = View.VISIBLE
                        please_wait_group.visibility = View.GONE
                        showDialogueBox(t.message.toString())
                    }
                }
            })
        }
    }
}
