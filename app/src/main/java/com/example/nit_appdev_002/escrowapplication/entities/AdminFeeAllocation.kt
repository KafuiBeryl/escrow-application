package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "admin_fee_allocation")
data class AdminFeeAllocation (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "adminAllocationId")
        @SerializedName("id")
        var adminAllocationId:Int,

        @ColumnInfo(name = "payee_allocation")
        @SerializedName("payee_allocation")
        var adminPayeeAllocation:String,

        @ColumnInfo(name = "agent")
        @SerializedName("agent")
        var agent:Boolean
)