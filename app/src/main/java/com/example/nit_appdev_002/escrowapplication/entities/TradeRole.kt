package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "trade_role")
data class TradeRole (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "tradeRoleId")
        @SerializedName("id")
        var tradeRoleId:Int,

        @ColumnInfo(name = "trade_role")
        @SerializedName("trade_role")
        var role:String
)