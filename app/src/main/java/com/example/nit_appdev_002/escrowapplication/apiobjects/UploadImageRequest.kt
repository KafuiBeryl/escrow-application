package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.google.gson.annotations.SerializedName

data class UploadImageRequest (
        @SerializedName("image_byte_array")
        var imageByteArray: String
)