package com.example.nit_appdev_002.escrowapplication.fragments


import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.*
import android.widget.*
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.trade.CreateTradeActivity
import com.example.nit_appdev_002.escrowapplication.apiobjects.SentBeneficiary
import com.example.nit_appdev_002.escrowapplication.utilities.Validator
import com.example.nit_appdev_002.escrowapplication.viewmodels.MiscViewModel
import kotlinx.android.synthetic.main.fragment_trade_beneficiary.*


/**
 * A simple [Fragment] subclass.
 *
 */
class TradeBeneficiaryFragment : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    companion object {
        @JvmStatic
        private lateinit var miscViewModel: MiscViewModel

        @JvmStatic
        private var benefitNumber: Int = 0

        @JvmStatic
        private val benefitEmailNumber: Int = 100

        @JvmStatic
        private val benefitConfirmEmailNumber: Int = 200

        @JvmStatic
        private val benefitAmountNumber: Int = 300

        @JvmStatic
        private val benefitTypeNumber: Int = 400

        @JvmStatic
        private val benefitFeeTypeNumber: Int = 500

        @JvmStatic
        private val benefitPayeeNumber: Int = 600

        @JvmStatic
        private var selectedFirstBeneficiaryType: String? = null

        @JvmStatic
        private var selectedFirstBeneficiaryFeeAllocation: String? = null

        @JvmStatic
        private var selectedFirstBeneficiaryFeeType: String? = null

        @JvmStatic
        private var selectedSecondBeneficiaryType: String? = null

        @JvmStatic
        private var selectedSecondBeneficiaryFeeAllocation: String? = null

        @JvmStatic
        private var selectedSecondBeneficiaryFeeType: String? = null

        @JvmStatic
        private var selectedThirdBeneficiaryType: String? = null

        @JvmStatic
        private var selectedThirdBeneficiaryFeeAllocation: String? = null

        @JvmStatic
        private var selectedThirdBeneficiaryFeeType: String? = null

//        @JvmStatic
//        private var selectedFourthBeneficiaryType: String? = null
//
//        @JvmStatic
//        private var selectedFourthBeneficiaryFeeAllocation: String? = null
//
//        @JvmStatic
//        private var selectedFourthBeneficiaryFeeType: String? = null
//
//        @JvmStatic
//        private var selectedFifthBeneficiaryType: String? = null
//
//        @JvmStatic
//        private var selectedFifthBeneficiaryFeeAllocation: String? = null
//
//        @JvmStatic
//        private var selectedFifthBeneficiaryFeeType: String? = null

        @JvmStatic
        private val beneficiaryTypeArray = ArrayList<String>()

        @JvmStatic
        private val beneficiaryFeeTypeArray = ArrayList<String>()

        @JvmStatic
        private val beneficiaryFeeAllocationArray = ArrayList<String>()


    }

    private var mListener : OnTradeBeneficiaryListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_trade_beneficiary, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        pod_slider.setCurrentlySelectedPodAndAnimate(3)

        //instantiate ViewModel object
        miscViewModel = ViewModelProviders.of(this).get(MiscViewModel::class.java)

//        benefitNumber = 0

        getSpinnerData()

        btn_add_beneficiary.setOnClickListener {
            addLinearLayout()
        }
    }

    private fun getSpinnerData(){
        miscViewModel.getFeeTypes().observe(this, Observer {
            feeTypes ->
            if (!feeTypes!!.isEmpty()){
                for (feeType in feeTypes){
                    beneficiaryFeeTypeArray.add(feeType)
                }
            }
        })

        miscViewModel.getBeneficiaryTypes().observe(this, Observer {
            types ->
            if (!types!!.isEmpty()){
                for (type in types){
                    beneficiaryTypeArray.add(type)
                }
            }
        })

        miscViewModel.getStandardFeeAllocation().observe(this, Observer {
            types ->
            if (!types!!.isEmpty()){
                for (type in types){
                    beneficiaryFeeAllocationArray.add(type)
                }
            }
        })
    }

    private fun addLinearLayout(){
        if (benefitNumber < 3){
            //basically a = a+b
            benefitNumber ++
            Log.e("BenefitNum", benefitNumber.toString())

            //craft the UI in code. This should be good
            val bigLinearLayout = LinearLayout(activity)
            val linearLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
            bigLinearLayout.layoutParams = linearLayoutParams
            bigLinearLayout.orientation = LinearLayout.VERTICAL

            val emailTextView = createBigTextView()
            emailTextView.text = resources.getString(R.string.register_email)
            emailTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val emailEditText = createBigEditText()
            emailEditText.id = benefitEmailNumber + benefitNumber

            val confirmEmailTextView = createBigTextView()
            confirmEmailTextView.text = resources.getString(R.string.trade_confirm_email)
            confirmEmailTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val confirmEmailEditText = createBigEditText()
            confirmEmailEditText.id = benefitConfirmEmailNumber + benefitNumber

            val firstSmallLinearLayout = LinearLayout(activity)
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            firstSmallLinearLayout.layoutParams = layoutParams
            firstSmallLinearLayout.orientation = LinearLayout.HORIZONTAL

            val benefitTypeTextView = createSmallTextView()
            benefitTypeTextView.text = resources.getString(R.string.trade_beneficiary_type)
            benefitTypeTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val benefitPayeeTextView = createSmallTextView()
            benefitPayeeTextView.text = resources.getString(R.string.trade_beneficiary_fee_allocation)
            benefitPayeeTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            firstSmallLinearLayout.addView(benefitTypeTextView, 0)
            firstSmallLinearLayout.addView(benefitPayeeTextView, 1)

            val firstSmallEditLinearLayout = LinearLayout(activity)
            val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            secondLayoutParams.gravity = Gravity.CENTER
            firstSmallEditLinearLayout.layoutParams = secondLayoutParams
            firstSmallEditLinearLayout.orientation = LinearLayout.HORIZONTAL

            val benefitTypeSpinner = createSmallSpinner()
            benefitTypeSpinner.id = benefitTypeNumber + benefitNumber
            //set spinner adapter
            val benefitTypeAdapter = ArrayAdapter(activity!!, R.layout.simple_spinner_item, beneficiaryTypeArray)
            benefitTypeAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
            benefitTypeSpinner.adapter = benefitTypeAdapter
            //set on item selected listener to the fragment
            // since the listener has been attached to the fragment
            benefitTypeSpinner.onItemSelectedListener = this

            val benefitPayeeSpinner = createSmallSpinner()
            benefitPayeeSpinner.id = benefitPayeeNumber + benefitNumber
            //set spinner adapter
            val benefitPayeeAdapter = ArrayAdapter(activity!!, R.layout.simple_spinner_item, beneficiaryFeeAllocationArray)
            benefitPayeeAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
            benefitPayeeSpinner.adapter = benefitPayeeAdapter
            //set on item selected listener to the fragment
            // since the listener has been attached to the fragment
            benefitPayeeSpinner.onItemSelectedListener = this

            firstSmallEditLinearLayout.addView(benefitTypeSpinner, 0)
            firstSmallEditLinearLayout.addView(benefitPayeeSpinner, 1)

            val secondSmallLinearLayout = LinearLayout(activity)
            secondSmallLinearLayout.layoutParams = layoutParams
            secondSmallLinearLayout.orientation = LinearLayout.HORIZONTAL

            val benefitAmountTextView = createSmallTextView()
            benefitAmountTextView.text = resources.getString(R.string.trade_beneficiary_amount)
            benefitAmountTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val benefitFeeTypeTextView = createSmallTextView()
            benefitFeeTypeTextView.text = resources.getString(R.string.trade_fee_type)
            benefitFeeTypeTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            secondSmallLinearLayout.addView(benefitAmountTextView, 0)
            secondSmallLinearLayout.addView(benefitFeeTypeTextView, 1)

            val secondSmallEditLinearLayout = LinearLayout(activity)
            secondSmallEditLinearLayout.layoutParams = secondLayoutParams
            secondSmallEditLinearLayout.orientation = LinearLayout.HORIZONTAL

            val benefitAmountEditText = createSmallNumberEditText()
            benefitAmountEditText.id = benefitAmountNumber + benefitNumber

            val benefitFeeTypeSpinner = createSmallSpinner()
            benefitFeeTypeSpinner.id = benefitFeeTypeNumber + benefitNumber
            //set spinner adapter
            val benefitFeeTypeAdapter = ArrayAdapter(activity, R.layout.simple_spinner_item, beneficiaryFeeTypeArray)
            benefitFeeTypeAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
            benefitFeeTypeSpinner.adapter = benefitFeeTypeAdapter
            //set on item selected listener to the fragment
            // since the listener has been attached to the fragment
            benefitFeeTypeSpinner.onItemSelectedListener = this

            secondSmallEditLinearLayout.addView(benefitAmountEditText, 0)
            secondSmallEditLinearLayout.addView(benefitFeeTypeSpinner, 1)

            bigLinearLayout.addView(emailTextView)
            bigLinearLayout.addView(emailEditText)
            bigLinearLayout.addView(confirmEmailTextView)
            bigLinearLayout.addView(confirmEmailEditText)
            bigLinearLayout.addView(firstSmallLinearLayout)
            bigLinearLayout.addView(firstSmallEditLinearLayout)
            bigLinearLayout.addView(secondSmallLinearLayout)
            bigLinearLayout.addView(secondSmallEditLinearLayout)

            val removeButton = createRemoveBeneficiaryButton()

            bigLinearLayout.addView(removeButton)

            beneficiary_linear_layout.addView(bigLinearLayout)

//            val stub = ViewStub(activity)
//            stub.layoutParams = linearLayoutParams
//            stub.layoutResource = R.layout.partial_first_beneficiary
//            beneficiary_linear_layout.addView(stub)
//            stub.inflate()

        }else{
            btn_add_beneficiary.visibility = View.GONE
            Toast.makeText(activity, R.string.trade_too_many_text, Toast.LENGTH_LONG).show()
        }

    }

    private fun createRemoveBeneficiaryButton():Button{
        val button = Button(activity, null, android.R.attr.borderlessButtonStyle)
        button.setTextColor(ResourcesCompat.getColor(resources, android.R.color.holo_red_dark, null))
        button.textSize = 15f
        val textViewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_semibold)
        button.typeface = textViewTypeface
        button.isAllCaps = false
        button.gravity = Gravity.END
        val buttonLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        buttonLayoutParams.gravity = Gravity.END
//        buttonLayoutParams.topMargin = 15
        button.layoutParams = buttonLayoutParams
        button.text = resources.getString(R.string.trade_beneficiary_remove_btn_text)
        button.setOnClickListener {
            view ->
            // same as subtracting 1
            benefitNumber --
            if (benefitNumber < 3){
                btn_add_beneficiary.visibility = View.VISIBLE
            }
            beneficiary_linear_layout.removeView(view.parent as View)
        }
        return button
    }

    private fun createBigTextView():TextView{
        //craft email text view
        val textView = TextView(activity)
        textView.setPadding(10, 0, 10, 0)
        textView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        if (Build.VERSION.SDK_INT < 23){
            textView.setTextAppearance(activity, android.R.style.TextAppearance_Small)
        }else{
            textView.setTextAppearance(android.R.style.TextAppearance_Small)
        }
        val textViewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_semibold)
        textView.typeface = textViewTypeface
        val textViewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        textViewLayoutParams.bottomMargin = 5
        textView.layoutParams = textViewLayoutParams
        textView.isAllCaps = true
        return textView
    }

    private fun createSmallTextView():TextView{
        //craft email text view
        val textView = TextView(activity)
        textView.setPadding(10, 0, 10, 0)
        textView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        if (Build.VERSION.SDK_INT < 23){
            textView.setTextAppearance(activity, android.R.style.TextAppearance_Small)
        }else{
            textView.setTextAppearance(android.R.style.TextAppearance_Small)
        }
        val textViewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_semibold)
        textView.typeface = textViewTypeface
        val textViewLayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        textViewLayoutParams.weight = 1f
        textViewLayoutParams.bottomMargin = 5
        textView.layoutParams = textViewLayoutParams
        textView.isAllCaps = true
        return textView
    }

    private fun createBigEditText():EditText{
        //craft edit Text
        val editText = EditText(activity)
        editText.setPadding(20, 5, 20, 5)
        editText.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        editText.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        editText.maxLines = 1
        val viewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        viewLayoutParams.gravity = Gravity.CENTER
        viewLayoutParams.bottomMargin = resources.getDimensionPixelSize(R.dimen.terms_margin_side)
        editText.layoutParams = viewLayoutParams
        val viewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_regular)
        editText.typeface = viewTypeface
        editText.background = ResourcesCompat.getDrawable(resources, R.drawable.profile_edit_shape, null)
        return editText
    }

    private fun createSmallNumberEditText(): EditText {
        //craft edit Text
        val editText = EditText(activity)
        editText.setPadding(20, 5, 20, 5)
        editText.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        editText.inputType = InputType.TYPE_CLASS_PHONE
        editText.maxLines = 1
        val viewLayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        viewLayoutParams.weight = 1f
        viewLayoutParams.gravity = Gravity.CENTER
        viewLayoutParams.marginEnd = resources.getDimensionPixelSize(R.dimen.terms_margin)
        viewLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.edit_text_margin_top)
        viewLayoutParams.bottomMargin = resources.getDimensionPixelSize(R.dimen.terms_margin)
        editText.layoutParams = viewLayoutParams
        val viewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_regular)
        editText.typeface = viewTypeface
        editText.background = ResourcesCompat.getDrawable(resources, R.drawable.profile_edit_shape, null)
        return editText
    }

    private fun createSmallSpinner():Spinner{
        val spinner = Spinner(activity)
        spinner.setPadding(5, 5, 5, 5)
        val viewLayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        viewLayoutParams.weight = 1f
        viewLayoutParams.gravity = Gravity.CENTER
        viewLayoutParams.marginEnd = 15
        viewLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.edit_text_margin_top)
        viewLayoutParams.bottomMargin = resources.getDimensionPixelSize(R.dimen.terms_margin)
        spinner.layoutParams = viewLayoutParams
        return spinner
    }

    private fun validateEverything():Boolean{
        var validate = false
        //did to 5 cause im stubborn. Thank me later
        when (benefitNumber){
            0 -> {
                validate = true
            }
            1 -> {
                val emptyText = Validator.traverseEditTexts(beneficiary_linear_layout)
                if ( emptyText == null || selectedFirstBeneficiaryFeeAllocation != null
                    || selectedFirstBeneficiaryFeeType != null || selectedFirstBeneficiaryType != null){
                    if (view!!.findViewById<EditText>(benefitEmailNumber+1).text.toString().contentEquals(view!!.findViewById<EditText>(benefitConfirmEmailNumber+1).text.toString())){
                        validate = true
                    }
                }
            }
            2 -> {
                val emptyText = Validator.traverseEditTexts(beneficiary_linear_layout)
                if ( emptyText == null || selectedFirstBeneficiaryFeeAllocation != null
                        || selectedFirstBeneficiaryFeeType != null || selectedFirstBeneficiaryType != null
                || selectedSecondBeneficiaryFeeAllocation != null || selectedSecondBeneficiaryFeeType != null
                || selectedSecondBeneficiaryType != null){
                    if (view!!.findViewById<EditText>(benefitEmailNumber+1).text.toString().contentEquals(view!!.findViewById<EditText>(benefitConfirmEmailNumber+1).text.toString())
                    && view!!.findViewById<EditText>(benefitEmailNumber+2).text.toString().contentEquals(view!!.findViewById<EditText>(benefitConfirmEmailNumber+2).text.toString())){
                        validate = true
                    }
                }
            }
            3 -> {
                val emptyText = Validator.traverseEditTexts(beneficiary_linear_layout)
                if ( emptyText == null || selectedFirstBeneficiaryFeeAllocation != null
                        || selectedFirstBeneficiaryFeeType != null || selectedFirstBeneficiaryType != null
                        || selectedSecondBeneficiaryFeeAllocation != null || selectedSecondBeneficiaryFeeType != null
                        || selectedSecondBeneficiaryType != null || selectedThirdBeneficiaryFeeAllocation != null
                        || selectedThirdBeneficiaryFeeType != null || selectedThirdBeneficiaryType != null){
                    if (view!!.findViewById<EditText>(benefitEmailNumber+1).text.toString().contentEquals(view!!.findViewById<EditText>(benefitConfirmEmailNumber+1).text.toString())
                            && view!!.findViewById<EditText>(benefitEmailNumber+2).text.toString().contentEquals(view!!.findViewById<EditText>(benefitConfirmEmailNumber+2).text.toString())
                            && view!!.findViewById<EditText>(benefitEmailNumber+3).text.toString().contentEquals(view!!.findViewById<EditText>(benefitConfirmEmailNumber+3).text.toString())){
                        validate = true
                    }
                }
            }
//            4 -> {
//                val emptyText = Validator.traverseEditTexts(beneficiary_linear_layout)
//                if ( emptyText == null || selectedFirstBeneficiaryFeeAllocation != null
//                        || selectedFirstBeneficiaryFeeType != null || selectedFirstBeneficiaryType != null
//                        || selectedSecondBeneficiaryFeeAllocation != null || selectedSecondBeneficiaryFeeType != null
//                        || selectedSecondBeneficiaryType != null || selectedThirdBeneficiaryFeeAllocation != null
//                        || selectedThirdBeneficiaryFeeType != null || selectedThirdBeneficiaryType != null
//                        || selectedFourthBeneficiaryFeeAllocation != null || selectedFourthBeneficiaryFeeType != null
//                        || selectedFourthBeneficiaryType != null){
//                    validate = true
//                }
//            }
//            5 -> {
//                val emptyText = Validator.traverseEditTexts(beneficiary_linear_layout)
//                if ( emptyText == null || selectedFirstBeneficiaryFeeAllocation != null
//                        || selectedFirstBeneficiaryFeeType != null || selectedFirstBeneficiaryType != null
//                        || selectedSecondBeneficiaryFeeAllocation != null || selectedSecondBeneficiaryFeeType != null
//                        || selectedSecondBeneficiaryType != null || selectedThirdBeneficiaryFeeAllocation != null
//                        || selectedThirdBeneficiaryFeeType != null || selectedThirdBeneficiaryType != null
//                        || selectedFourthBeneficiaryFeeAllocation != null || selectedFourthBeneficiaryFeeType != null
//                        || selectedFourthBeneficiaryType != null || selectedFifthBeneficiaryFeeAllocation != null
//                        || selectedFifthBeneficiaryFeeType != null || selectedFifthBeneficiaryType != null){
//                    validate = true
//                }
//            }
        }

        return validate
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        when (parent.id){
            benefitTypeNumber+1 -> selectedFirstBeneficiaryType = parent.getItemAtPosition(position).toString()
            benefitTypeNumber+2 -> selectedSecondBeneficiaryType = parent.getItemAtPosition(position).toString()
            benefitTypeNumber+3 -> selectedThirdBeneficiaryType = parent.getItemAtPosition(position).toString()
//            benefitTypeNumber.toLong()+4 -> selectedFourthBeneficiaryType = parent.getItemAtPosition(position).toString()
//            benefitTypeNumber.toLong()+5 -> selectedFifthBeneficiaryType = parent.getItemAtPosition(position).toString()

            benefitFeeTypeNumber+1 -> selectedFirstBeneficiaryFeeType = parent.getItemAtPosition(position).toString()
            benefitFeeTypeNumber+2 -> selectedSecondBeneficiaryFeeType = parent.getItemAtPosition(position).toString()
            benefitFeeTypeNumber+3 -> selectedThirdBeneficiaryFeeType = parent.getItemAtPosition(position).toString()
//            benefitFeeTypeNumber.toLong()+4 -> selectedFourthBeneficiaryFeeType = parent.getItemAtPosition(position).toString()
//            benefitFeeTypeNumber.toLong()+5 -> selectedFifthBeneficiaryFeeType = parent.getItemAtPosition(position).toString()

            benefitPayeeNumber+1 -> selectedFirstBeneficiaryFeeAllocation = parent.getItemAtPosition(position).toString()
            benefitPayeeNumber+2 -> selectedSecondBeneficiaryFeeAllocation = parent.getItemAtPosition(position).toString()
            benefitPayeeNumber+3 -> selectedThirdBeneficiaryFeeAllocation = parent.getItemAtPosition(position).toString()
//            benefitPayeeNumber.toLong()+4 -> selectedFourthBeneficiaryFeeAllocation = parent.getItemAtPosition(position).toString()
//            benefitPayeeNumber.toLong()+5 -> selectedFifthBeneficiaryFeeAllocation = parent.getItemAtPosition(position).toString()

        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("nothing to be done yet")
    }

    private fun passEntriesToVariable(){
        when (benefitNumber){
            0 -> {
                CreateTradeActivity.createTradeRequest.hasBeneficiary = false
            }
            1 -> {
                CreateTradeActivity.createTradeRequest.hasBeneficiary = true
                val firstBeneficiary = SentBeneficiary(
                        view!!.findViewById<EditText>(benefitEmailNumber+1).text.toString(),
                        selectedFirstBeneficiaryType!!, selectedFirstBeneficiaryFeeType!!,
                        (view!!.findViewById<EditText>(benefitAmountNumber+1).text.toString().toDouble() * 100).toLong(),
                        (view!!.findViewById<EditText>(benefitAmountNumber+1).text.toString().toDouble() * 100).toLong(),
                        selectedFirstBeneficiaryFeeAllocation!!)

                //add the beneficiaries to the created object in the main activity
                //set the fee allocation. Subtract and add where necessary
                setBeneficiaryFeeAllocation(firstBeneficiary)
                CreateTradeActivity.beneficiaries.add(firstBeneficiary)
            }
            2 -> {
                CreateTradeActivity.createTradeRequest.hasBeneficiary = true
                val firstBeneficiary = SentBeneficiary(
                        view!!.findViewById<EditText>(benefitEmailNumber+1).text.toString(),
                        selectedFirstBeneficiaryType!!, selectedFirstBeneficiaryFeeType!!,
                        (view!!.findViewById<EditText>(benefitAmountNumber+1).text.toString().toDouble() * 100).toLong(),
                        (view!!.findViewById<EditText>(benefitAmountNumber+1).text.toString().toDouble() * 100).toLong(),
                        selectedFirstBeneficiaryFeeAllocation!!)

                val secondBeneficiary = SentBeneficiary(
                        view!!.findViewById<EditText>(benefitEmailNumber+2).text.toString(),
                        selectedSecondBeneficiaryType!!, selectedSecondBeneficiaryFeeType!!,
                        (view!!.findViewById<EditText>(benefitAmountNumber+2).text.toString().toDouble() * 100).toLong(),
                        (view!!.findViewById<EditText>(benefitAmountNumber+2).text.toString().toDouble() * 100).toLong(),
                        selectedSecondBeneficiaryFeeAllocation!!)

                //add the beneficiaries to the created object in the main activity
                //set the fee allocation. Subtract and add where necessary
                setBeneficiaryFeeAllocation(firstBeneficiary)
                CreateTradeActivity.beneficiaries.add(firstBeneficiary)
                setBeneficiaryFeeAllocation(secondBeneficiary)
                CreateTradeActivity.beneficiaries.add(secondBeneficiary)
            }
            3 -> {
                CreateTradeActivity.createTradeRequest.hasBeneficiary = true
                val firstBeneficiary = SentBeneficiary(
                        view!!.findViewById<EditText>(benefitEmailNumber+1).text.toString(),
                        selectedFirstBeneficiaryType!!, selectedFirstBeneficiaryFeeType!!,
                        (view!!.findViewById<EditText>(benefitAmountNumber+1).text.toString().toDouble() * 100).toLong(),
                        (view!!.findViewById<EditText>(benefitAmountNumber+1).text.toString().toDouble() * 100).toLong(),
                        selectedFirstBeneficiaryFeeAllocation!!)

                val secondBeneficiary = SentBeneficiary(
                        view!!.findViewById<EditText>(benefitEmailNumber+2).text.toString(),
                        selectedSecondBeneficiaryType!!, selectedSecondBeneficiaryFeeType!!,
                        (view!!.findViewById<EditText>(benefitAmountNumber+2).text.toString().toDouble() * 100).toLong(),
                        (view!!.findViewById<EditText>(benefitAmountNumber+2).text.toString().toDouble() * 100).toLong(),
                        selectedSecondBeneficiaryFeeAllocation!!)

                val thirdBeneficiary = SentBeneficiary(
                        view!!.findViewById<EditText>(benefitEmailNumber+3).text.toString(),
                        selectedThirdBeneficiaryType!!, selectedThirdBeneficiaryFeeType!!,
                        (view!!.findViewById<EditText>(benefitAmountNumber+3).text.toString().toDouble() * 100).toLong(),
                        (view!!.findViewById<EditText>(benefitAmountNumber+3).text.toString().toDouble() * 100).toLong(),
                        selectedThirdBeneficiaryFeeAllocation!!)

                //add the beneficiaries to the created object in the main activity
                //set the fee allocation. Subtract and add where necessary
                setBeneficiaryFeeAllocation(firstBeneficiary)
                CreateTradeActivity.beneficiaries.add(firstBeneficiary)
                setBeneficiaryFeeAllocation(secondBeneficiary)
                CreateTradeActivity.beneficiaries.add(secondBeneficiary)
                setBeneficiaryFeeAllocation(thirdBeneficiary)
                CreateTradeActivity.beneficiaries.add(thirdBeneficiary)
            }
        }
    }

    private fun setBeneficiaryFeeAllocation(sentBeneficiary: SentBeneficiary){
        val feeAllocation = sentBeneficiary.beneficiaryPayee
        when (feeAllocation){
            "Seller Pays" -> {
                CreateTradeActivity.createTradeRequest.sellerFee = CreateTradeActivity.createTradeRequest.sellerFee-calculateBeneficiaryFeeNoDivision(sentBeneficiary)
            }
            "Buyer Pays" -> {
                CreateTradeActivity.createTradeRequest.buyerFee = CreateTradeActivity.createTradeRequest.buyerFee+calculateBeneficiaryFeeNoDivision(sentBeneficiary)
            }
            "50/50 Split [Buyer/Seller]" -> {
                CreateTradeActivity.createTradeRequest.buyerFee = CreateTradeActivity.createTradeRequest.buyerFee+calculateBeneficiaryFee5050Division(sentBeneficiary)
                CreateTradeActivity.createTradeRequest.sellerFee = CreateTradeActivity.createTradeRequest.sellerFee-calculateBeneficiaryFee5050Division(sentBeneficiary)
            }
        }

        sentBeneficiary.beneficiaryFee = calculateBeneficiaryFeeNoDivision(sentBeneficiary)
    }

    private fun calculateBeneficiaryFee5050Division(sentBeneficiary: SentBeneficiary) : Long{
        val feeType = sentBeneficiary.beneficiaryFeeType
        val feeUnedited = sentBeneficiary.unCalculatedBeneficiaryFee
        val tradeAmount = CreateTradeActivity.createTradeRequest.tradeAmount
        var feeEdited:Long = 0
        when (feeType){
            "Fixed Fee" -> {
                feeEdited = feeUnedited/2
            }
            "Percentage (%)" -> {
                //get cedi amounts from pesewas
                //and actual number from the 100 multiplication
                val beneFee = feeUnedited.toDouble()/100
                val amount = tradeAmount.toDouble()/100
                //get percentage of amount
                val percentageOfAmount = (amount*beneFee)/100
                //divide amount into 2 for 50/50 division
                val shareAmount = percentageOfAmount/2
                //multiply by hundred again to get pesewas
                feeEdited = (shareAmount*100).toLong()
            }
        }
        return feeEdited
    }

    private fun calculateBeneficiaryFeeNoDivision(sentBeneficiary: SentBeneficiary) : Long{
        val feeType = sentBeneficiary.beneficiaryFeeType
        val feeUnedited = sentBeneficiary.unCalculatedBeneficiaryFee
        val tradeAmount = CreateTradeActivity.createTradeRequest.tradeAmount
        var feeEdited:Long = 0
        when (feeType){
            "Fixed Fee" -> {
                feeEdited = feeUnedited
            }
            "Percentage (%)" -> {
                //get cedi amounts from pesewas
                //and actual number from the 100 multiplication
                val beneFee = feeUnedited.toDouble()/100
                val amount = tradeAmount.toDouble()/100
                //get percentage of amount
                val percentageOfAmount = (amount*beneFee)/100
                //multiply by hundred again to get pesewas
                feeEdited = (percentageOfAmount*100).toLong()
            }
        }
        return feeEdited
    }

    override fun onResume() {
        super.onResume()
        btn_back.setOnClickListener(this)
        btn_next.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btn_back.setOnClickListener(null)
        btn_next.setOnClickListener(null)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnTradeBeneficiaryListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnTradeBeneficiaryListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onClick(v: View?) {
        when (v?.id){
            R.id.btn_back -> {
                if (mListener != null){
                    mListener?.onBackButtonPressed(this)
                }
            }
            R.id.btn_next -> {
                if (validateEverything()){
                    if (mListener != null){
                        passEntriesToVariable()
                        mListener?.onNextPressed(this)
                    }
                }else{
                    Toast.makeText(activity, R.string.trade_empty_text, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnTradeBeneficiaryListener {
        fun onNextPressed(fragment: Fragment)

        fun onBackButtonPressed(fragment: Fragment)
    }
}
