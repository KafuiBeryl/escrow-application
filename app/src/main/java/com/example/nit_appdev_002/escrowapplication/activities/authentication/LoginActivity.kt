package com.example.nit_appdev_002.escrowapplication.activities.authentication

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.home.HomeActivity
import com.example.nit_appdev_002.escrowapplication.apicalls.OtherNetworkCalls
import com.example.nit_appdev_002.escrowapplication.apicalls.RetrofitInstance
import com.example.nit_appdev_002.escrowapplication.apiobjects.LoginRequest
import com.example.nit_appdev_002.escrowapplication.apiobjects.LoginResult
import com.example.nit_appdev_002.escrowapplication.entities.User
import com.example.nit_appdev_002.escrowapplication.utilities.*
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.set
import com.example.nit_appdev_002.escrowapplication.viewmodels.UserViewModel
import com.github.omadahealth.lollipin.lib.managers.AppLock
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.please_wait.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    companion object {
        @JvmStatic
        private lateinit var mUserViewModel: UserViewModel

//        class LoginAsyncTask(activity: LoginActivity): AsyncTask<LoginRequest, Void, LoginResult>(){
//
//            private val activityReference:WeakReference<LoginActivity>
//
//            private val application:Application
//
//            init {
//                activityReference = WeakReference(activity)
//                application = activity.application
//            }
//
//            override fun onPreExecute() {
//                super.onPreExecute()
//                val activity = activityReference.get()
//                activity?.please_wait_group?.visibility = View.VISIBLE
//            }
//
//            override fun doInBackground(vararg params: LoginRequest): LoginResult? {
//                try {
//                    val api = RestAPI()
//                    val callResponse = api.login(params[0])
//                    val response = callResponse.execute()
//                    if (response.isSuccessful ){
//                        if (response.body() != null){
//                            return response.body() as LoginResult
//                        }
//                        return null
//                    }else if (response.errorBody() != null){
//                        val gson = GsonBuilder().create()
//                        try {
//                            val mError:LoginResult = gson.fromJson<LoginResult>(response.errorBody()?.string(), LoginResult::class.java)
//                            return mError
//                        }catch (e:Throwable){
//                            Log.e("LoginDesiariaTask", e.message)
//                        }
//                    }
//                }catch (e:Throwable){
//                    Log.e("LoginTask", e.cause.toString() + e.message)
//                }
//                return null
//            }
//
//            override fun onPostExecute(result: LoginResult?) {
//                super.onPostExecute(result)
//                val activity = activityReference.get()
//                if (result == null){
//                    activity?.please_wait_group?.visibility = View.GONE
//                    Toast.makeText(application, R.string.connection_failed, Toast.LENGTH_LONG).show()
//                }else if (!TextUtils.isEmpty(result.token)){
//                    val prefs = PreferenceHelper.defaultPrefs(application)
//                    prefs[PREF_TOKEN] = result.token
//                    prefs[PREF_TOKEN_EXPIRY] = result.tokenExpiryDate
//                    prefs[PREF_USER_ID] = result.user!!.id
//                    if (result.userTrades != null){
//                        mUserViewModel.insertUserTradeRoles(result.userTrades)
//                    }
//                    val user: User = result.user!!
//                    if (!TextUtils.isEmpty(result.userImage)){
//                        user.imagePath = result.userImage
//                    }
//                    mUserViewModel.insertUser(user)
//                    activity?.please_wait_group?.visibility = View.GONE
//                    val intent = Intent(application, CustomPinActivity::class.java)
//                    intent.putExtra(AppLock.EXTRA_TYPE, AppLock.ENABLE_PINLOCK)
//                    activity?.startActivityForResult(intent, REQUEST_FIRST_RUN_PIN)
//                }else{
//                    activity?.please_wait_group?.visibility = View.GONE
//                    val builder: AlertDialog.Builder = AlertDialog.Builder(activity!!)
//                    builder.setTitle(R.string.alert_title)
//                            .setMessage(result.error)
//                            .setIcon(android.R.drawable.ic_menu_edit)
//                            .setNeutralButton(android.R.string.ok, DialogInterface.OnClickListener {
//                                dialog, which ->
//
//                            })
//                    val dialog = builder.create()
//                    dialog.show()
//                }
//            }
//        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        please_wait_group.visibility = View.GONE

        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        dont_have_account.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
            finish()
        }

        val emailText = login_email_txt.text
        val passwordText = login_password_txt.text

        login_submit_btn.setOnClickListener {
            if (TextUtils.isEmpty(emailText) || TextUtils.isEmpty(passwordText)){
                Toast.makeText(this, R.string.login_empty_text, Toast.LENGTH_LONG).show()
            }else{
                val login = LoginRequest(emailText.toString(),passwordText.toString())
//                LoginAsyncTask(this).execute(login)
                login(login)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == PIN_SUCCEEDED){
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }else{
            mUserViewModel.logoutThings()
        }
    }

    private fun login(request: LoginRequest){
        please_wait_group?.visibility = View.VISIBLE
        val service: OtherNetworkCalls = RetrofitInstance.getRetrofitInstance()!!.create(OtherNetworkCalls::class.java)
        val call = service.login(request)
        try {
            call.enqueue(object : Callback<LoginResult>{
                override fun onResponse(call: Call<LoginResult>, response: Response<LoginResult>) {
                    if (response.body() != null){
                        val result = response.body() as LoginResult
                        storeData(result)
                    }else if (response.errorBody() != null){
                        val gson = GsonBuilder().create()
                        try {
                            val mError:LoginResult = gson.fromJson<LoginResult>(response.errorBody()?.string(), LoginResult::class.java)
                            showDialog(mError.error.toString())
                        }catch (e:Throwable){
                            Log.e("LoginDesiariaTask", e.message)
                        }
                    }
                }

                override fun onFailure(call: Call<LoginResult>?, t: Throwable?) {
                    if (t?.message != null){
                        please_wait_group.visibility = View.GONE
                        showDialog(t.message.toString())
                    }
                }
            })
        }catch (e:Throwable){
            Log.e("LoginTask", e.cause.toString() + e.message)
        }
    }

    private fun storeData(result: LoginResult){
        val prefs = PreferenceHelper.defaultPrefs(application)
        prefs[PREF_TOKEN] = result.token
        prefs[PREF_TOKEN_EXPIRY] = result.tokenExpiryDate
        prefs[PREF_USER_ID] = result.user!!.id
        if (result.userTrades != null){
            mUserViewModel.insertUserTradeRoles(result.userTrades)
        }
        val user: User = result.user!!
        if (!TextUtils.isEmpty(result.userImage)){
            user.imagePath = result.userImage
        }
        if (result.userOccupation != null){
            user.userOccupation = result.userOccupation
        }
        mUserViewModel.insertUser(user)
        please_wait_group?.visibility = View.GONE
        val intent = Intent(application, CustomPinActivity::class.java)
        intent.putExtra(AppLock.EXTRA_TYPE, AppLock.ENABLE_PINLOCK)
        startActivityForResult(intent, REQUEST_FIRST_RUN_PIN)
    }

    private fun showDialog(errorString:String){
        please_wait_group?.visibility = View.GONE
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.alert_title)
                .setMessage(errorString)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setNeutralButton(android.R.string.ok){_, _ ->

                }
        val dialog = builder.create()
        dialog.show()
    }
}
