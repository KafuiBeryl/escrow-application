package com.example.nit_appdev_002.escrowapplication.entities

import com.google.gson.annotations.SerializedName
import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "user")
data class User (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "id")
        @SerializedName("id")
        var id:Int,

        @ColumnInfo(name = "first_name")
        @SerializedName("first_name")
        var firstName:String?,

        @ColumnInfo(name = "last_name")
        @SerializedName("last_name")
        var lastName:String?,

        @ColumnInfo(name = "email")
        @SerializedName("email")
        var email:String,

        @ColumnInfo(name = "email_confirmed")
        @SerializedName("email_confirmed")
        var emailConfirmed:Boolean,

        @ColumnInfo(name = "mobile_dialing_code")
        @SerializedName("mobile_dialing_code")
        var mobileDialingCode:String?,

        @ColumnInfo(name = "mobile_number")
        @SerializedName("mobile_number")
        var mobileNumber:String?,

        @ColumnInfo(name = "home_dialing_code")
        @SerializedName("home_dialing_code")
        var homeDialingCode:String?,

        @ColumnInfo(name = "home_number")
        @SerializedName("home_number")
        var homeNumber:String?,

//        @ColumnInfo(name = "identification_type")
        @SerializedName("identification_type_used")
        @Embedded
        var identification_type:IdentificationType?,

        @ColumnInfo(name = "id_number")
        @SerializedName("id_number")
        var identificationNumber:String?,

        @ColumnInfo(name = "citizenship")
        @SerializedName("citizenship")
        var citizenship:String?,

        @ColumnInfo(name = "company_name")
        @SerializedName("company_name")
        var companyName:String?,

        @ColumnInfo(name = "tax_identification_number")
        @SerializedName("tax_identification_number")
        var TIN:String?,

        @ColumnInfo(name = "company_registration")
        @SerializedName("company_registration")
        var companyRegistrationNumber:String?,

//        @ColumnInfo(name = "company_type")
        @SerializedName("company_category")
        @Embedded
        var company_type:CompanyCategory?,

        @ColumnInfo(name = "bill_address_line_1")
        @SerializedName("bill_address_line_1")
        var billAddress1:String?,

        @ColumnInfo(name = "bill_address_line_2")
        @SerializedName("bill_address_line_2")
        var billAddress2:String?,

        @ColumnInfo(name = "bill_address_suburb")
        @SerializedName("bill_address_suburb")
        var billAddressSuburb:String?,

        @ColumnInfo(name = "bill_address_city")
        @SerializedName("bill_address_city")
        var billAddressCity:String?,

        @ColumnInfo(name = "bill_address_region")
        @SerializedName("bill_address_region")
        var billAddressRegion:String?,

        @ColumnInfo(name = "bill_address_country")
        @SerializedName("bill_address_country")
        var billAddressCountry:String?,

        @ColumnInfo(name = "bill_address_postcode")
        @SerializedName("bill_address_postcode")
        var billAddressPostCode:String?,

        @ColumnInfo(name = "digital_address")
        @SerializedName("digital_address")
        var digitalAddress:String?,

//        @ColumnInfo(name = "bank_id")
        @SerializedName("bank")
        @Embedded
        var bank_id:Bank?,

        @ColumnInfo(name = "account_number")
        @SerializedName("account_number")
        var accountNumber:String?,

        @ColumnInfo(name = "account_name")
        @SerializedName("account_name")
        var accountName:String?,

        @ColumnInfo(name = "bank_branch")
        @SerializedName("bank_branch")
        var bankBranch:String?,

//        @ColumnInfo(name = "account_type")
        @SerializedName("bank_account_type")
        @Embedded
        var account_type:BankAccountType?,

        @ColumnInfo(name = "device_id")
        @SerializedName("device_id")
        var deviceId:String?,

        @ColumnInfo(name = "created_at")
        @SerializedName("created_at")
        var createdDate:Date,

        @ColumnInfo(name = "updated_at")
        @SerializedName("updated_at")
        var updatedDate:Date,

        @ColumnInfo(name = "phone_verified")
        @SerializedName("phone_verified")
        var phoneVerified:Boolean?,

        @ColumnInfo(name = "user_account_completed")
        @SerializedName("user_account_completed")
        var accountCompleted:Boolean,

        @ColumnInfo(name = "image_path")
        var imagePath:String?,

        @SerializedName("occupations")
        @Embedded
        var userOccupation: Occupation?


)