package com.example.nit_appdev_002.escrowapplication.utilities

import android.content.Context
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.example.nit_appdev_002.escrowapplication.R

class Animator {

    companion object {

        /**
         * set animation so it slides down nicely
         * @param context
         * @param view
         */
        @JvmStatic
        fun slideDown(context: Context, view: View){
            val a:Animation = AnimationUtils.loadAnimation(context, R.anim.sliding_down)
            a.reset()
            view.clearAnimation()
            view.startAnimation(a)
        }

        /**
         * set animation so it slides down nicely
         * @param context
         * @param view
         */
        @JvmStatic
        fun slideUp(context: Context, view: View){
            val a:Animation = AnimationUtils.loadAnimation(context, R.anim.sliding_up)
            a.reset()
            view.clearAnimation()
            view.startAnimation(a)
        }
    }
}