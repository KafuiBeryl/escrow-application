package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "fee_schedule")
data class FeeSchedule(
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "id")
        @SerializedName("id")
        var id:Int,

        @ColumnInfo(name = "from")
        @SerializedName("from")
        var from:Long,

        @ColumnInfo(name = "to")
        @SerializedName("to")
        var to:Long,

        @ColumnInfo(name = "percentage")
        @SerializedName("percentage")
        var percentage:Double,

        @ColumnInfo(name = "created_at")
        @SerializedName("created_at")
        var createdDate: Date,

        @ColumnInfo(name = "updated_at")
        @SerializedName("updated_at")
        var updatedDate:Date
)