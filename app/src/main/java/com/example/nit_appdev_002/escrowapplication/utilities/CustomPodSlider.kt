package com.example.nit_appdev_002.escrowapplication.utilities

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import com.bhargavms.podslider.PodSlider

class CustomPodSlider : PodSlider {

    private var swipeEnabled:Boolean

    constructor(context: Context) : super(context) {
        this.swipeEnabled = false
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        this.swipeEnabled = false
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr:Int) : super(context, attrs, defStyleAttr) {
        this.swipeEnabled = false
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return swipeEnabled
    }
}