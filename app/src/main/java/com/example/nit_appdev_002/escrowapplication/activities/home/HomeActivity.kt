package com.example.nit_appdev_002.escrowapplication.activities.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.authentication.LoginActivity
import com.example.nit_appdev_002.escrowapplication.activities.profile.ProfileActivity
import com.example.nit_appdev_002.escrowapplication.activities.trade.CreateTradeActivity
import com.example.nit_appdev_002.escrowapplication.activities.trade.ViewTradeActivity
import com.example.nit_appdev_002.escrowapplication.apicalls.OtherNetworkCalls
import com.example.nit_appdev_002.escrowapplication.apicalls.RetrofitInstance
import com.example.nit_appdev_002.escrowapplication.apiobjects.HomeResult
import com.example.nit_appdev_002.escrowapplication.utilities.*
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.viewmodels.MiscViewModel
import com.example.nit_appdev_002.escrowapplication.viewmodels.UserViewModel
import com.github.omadahealth.lollipin.lib.PinCompatActivity
import com.google.android.material.navigation.NavigationView
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import kotlinx.android.synthetic.main.content_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.pusher.pushnotifications.PushNotifications
import kotlinx.android.synthetic.main.nav_header_home.*

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    companion object {
        @JvmStatic
        private lateinit var mUserViewModel: UserViewModel

        @JvmStatic
        private lateinit var miscViewModel: MiscViewModel
    }
    private var id:Int? = null
    private var token:String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_home)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        miscViewModel = ViewModelProviders.of(this).get(MiscViewModel::class.java)

        val prefs = PreferenceHelper.defaultPrefs(this)
        token = prefs[PREF_TOKEN]
        id = prefs[PREF_USER_ID]

        makeApiCalls()

        PushNotifications.start(applicationContext, "7eabf1eb-fad8-41ae-87da-9730736b50ef")
        PushNotifications.subscribe("hello")
        PushNotifications.subscribe("debug-hello")


        btn_home_profile.setOnClickListener {
            startActivity(Intent(this, ProfileActivity::class.java))
        }

        btn_home_new_trade.setOnClickListener {
            startActivity(Intent(this, CreateTradeActivity::class.java))
        }

        btn_home_view_trade.setOnClickListener {
            startActivity(Intent(this, ViewTradeActivity::class.java))
        }

//        btn_home_settings.setOnClickListener {
//
//        }
//
//        btn_home_feedback.setOnClickListener {
//
//        }
//        setDrawerValues()
    }

    override fun onResume() {
        super.onResume()
        setDrawerValues()
    }

    private fun setDrawerValues(){
        mUserViewModel.getUser(id!!).observe(this, Observer {
            user->
            if (user != null){
                val string = "${user.firstName} ${user.lastName}"
                if (user.imagePath != null){
                    Glide.with(this)
                            .load(Base64.decode(user.imagePath, Base64.URL_SAFE))
                            .apply(RequestOptions()
                                    .fitCenter()
                                    .placeholder(R.drawable.profile_placeholder))
                            .into(drawer_imageView)
                }
                drawer_main_textView.text = string
                drawer_sub_textView.text = user.email
            }
        })
    }

    private fun makeApiCalls(){
        if (token != null){
            val service:OtherNetworkCalls = RetrofitInstance.getRetrofitInstance()!!.create(OtherNetworkCalls::class.java)
            val tokenString = "Bearer $token"
            val call = service.getMiscInfo(tokenString)
            call.enqueue(object: Callback<HomeResult> {
                override fun onResponse(call: Call<HomeResult>?, response: Response<HomeResult>?) {
                    if (response?.body() != null){
                        val result = response.body() as HomeResult
                        miscViewModel.insertBanks(result.banks)
                        miscViewModel.insertCompanyCategories(result.companyCategories)
                        miscViewModel.insertFeeSchedule(result.feeSchedule)
                        miscViewModel.insertIndustries(result.industries)
                        miscViewModel.insertBankAccountTypes(result.bankAccountTypes)
                        miscViewModel.insertIdentityTypes(result.identificationTypes)
                        miscViewModel.insertAdminFeeAllocation(result.adminFeeAllocation)
                        miscViewModel.insertStandardFeeAllocation(result.standardFeeAllocation)
                        miscViewModel.insertFeeType(result.feeTypes)
                        miscViewModel.insertBeneficiaryType(result.beneficiaryType)
                        miscViewModel.insertOccupations(result.occupations)
                    }else if (response?.errorBody() != null){
                        val gson = GsonBuilder().create()
                        val errorResult:HomeResult = gson.fromJson<HomeResult>(response.errorBody()?.string(), HomeResult::class.java)
                        Log.e("HomeRequest", errorResult.error)
                        unauthorisedToast(errorResult.error)
                    }
                }

                override fun onFailure(call: Call<HomeResult>?, t: Throwable?) {
                    if (t?.message != null){
                        Log.e("HomeRequestError", t.message.toString())
                        unauthorisedToast(t.message.toString())
                    }
                }
            })
        }
    }

    private fun unauthorisedToast(error:String?){
        Toast.makeText(this, "$error ...", Toast.LENGTH_SHORT).show()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                // Handle the camera action
            }
            R.id.nav_new_trade -> {

            }
            R.id.nav_view_trade -> {

            }
            R.id.nav_profile -> {

            }
            R.id.nav_personal -> {

            }
            R.id.nav_billing -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
