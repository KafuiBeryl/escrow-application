package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "notification_gang")
data class NotificationGang (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "id")
        @SerializedName("id")
        var id:Int?,

        @ColumnInfo(name = "email")
        @SerializedName("email")
        var email:String,

        @ColumnInfo(name = "first_name")
        @SerializedName("first_name")
        var firstName:String,

        @ColumnInfo(name = "last_name")
        @SerializedName("last_name")
        var lastName: String,

        @ColumnInfo(name = "trade_id")
        @SerializedName("trade_id")
        var tradeId:Int?,

        @ColumnInfo(name = "created_at")
        @SerializedName("created_at")
        var createdDate: Date?,

        @ColumnInfo(name = "updated_at")
        @SerializedName("updated_at")
        var updatedDate: Date?
)