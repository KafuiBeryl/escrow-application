package com.example.nit_appdev_002.escrowapplication.activities.trade.create

import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.utilities.Validator
import kotlinx.android.synthetic.main.activity_trade_progress.*
import kotlinx.android.synthetic.main.toolbar.*

class TradeProgressActivity : AppCompatActivity() {

    companion object {
//        @JvmStatic
//        private lateinit var miscViewModel: MiscViewModel

        @JvmStatic
        private var progressNumber: Int = 0

        @JvmStatic
        private var progressNameNumber: Int = 700

        @JvmStatic
        private var progressDescriptionNumber: Int = 800

        @JvmStatic
        private var progressSellerAmountNumber: Int = 900

        @JvmStatic
        private var progressInspectionNumber: Int = 1000

        @JvmStatic
        private var progressDeliveryNumber: Int = 1100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trade_progress)
        setSupportActionBar(custom_toolbar)

        btn_add_progress.setOnClickListener {
            addLayout()
        }

        btn_back.setOnClickListener {
            finish()
        }

        btn_next.setOnClickListener {

        }
    }

    private fun addLayout(){
        if (progressNumber < 5){
            //basically a = a+b
            progressNumber =+ 1
            //craft the UI in code. This should be good again. Fingers crossed
            val bigLinearLayout = LinearLayout(this)
            val linearLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
            bigLinearLayout.layoutParams = linearLayoutParams
            bigLinearLayout.orientation = LinearLayout.VERTICAL

            val progressNameTextView = createBigTextView()
            progressNameTextView.text = resources.getString(R.string.trade_progress_name)
            progressNameTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val progressNameEdit = createBigEditText()
            progressNameEdit.id = progressNameNumber + progressNumber

            val progressDescriptionTextView = createBigTextView()
            progressDescriptionTextView.text = resources.getString(R.string.trade_progress_description)
            progressDescriptionTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val progressDescEdit = createHugeEditText()
            progressDescEdit.id = progressDescriptionNumber + progressNumber

            val progressAmountTextView = createBigTextView()
            progressAmountTextView.text = resources.getString(R.string.trade_progress_seller_amount)
            progressAmountTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val progressAmountEdit = createNumberEditText()
            progressAmountEdit.id = progressSellerAmountNumber + progressNumber

            val firstSmallLinearLayout = LinearLayout(this)
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            firstSmallLinearLayout.layoutParams = layoutParams
            firstSmallLinearLayout.orientation = LinearLayout.HORIZONTAL

            val progressInspectionTextView = createSmallTextView()
            progressInspectionTextView.text = resources.getString(R.string.trade_inspection_period)
            progressInspectionTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val progressDeliveryTextView = createSmallTextView()
            progressDeliveryTextView.text = resources.getString(R.string.trade_delivery_time)
            progressDeliveryTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            firstSmallLinearLayout.addView(progressInspectionTextView, 0)
            firstSmallLinearLayout.addView(progressDeliveryTextView, 1)

            val firstSmallEditLinearLayout = LinearLayout(this)
            val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            layoutParams.gravity = Gravity.CENTER
            firstSmallEditLinearLayout.layoutParams = secondLayoutParams
            firstSmallEditLinearLayout.orientation = LinearLayout.HORIZONTAL

            val inspectionEdit = createSmallNumberEditText()
            inspectionEdit.id = progressInspectionNumber + progressNumber

            val deliveryEdit = createSmallNumberEditText()
            deliveryEdit.id = progressDeliveryNumber + progressNumber

            firstSmallEditLinearLayout.addView(inspectionEdit, 0)
            firstSmallEditLinearLayout.addView(deliveryEdit, 1)

            bigLinearLayout.addView(progressNameTextView)
            bigLinearLayout.addView(progressNameEdit)
            bigLinearLayout.addView(progressDescriptionTextView)
            bigLinearLayout.addView(progressDescEdit)
            bigLinearLayout.addView(progressAmountTextView)
            bigLinearLayout.addView(progressAmountEdit)
            bigLinearLayout.addView(firstSmallLinearLayout)
            bigLinearLayout.addView(firstSmallEditLinearLayout)

            val removeButton = createRemoveBeneficiaryButton()

            bigLinearLayout.addView(removeButton)

            progress_linear_layout.addView(bigLinearLayout)

        }else{
            btn_add_progress.visibility = View.GONE
            Toast.makeText(this, R.string.trade_too_many_text, Toast.LENGTH_LONG).show()
        }
    }

    private fun createRemoveBeneficiaryButton(): Button {
        val button = Button(this, null, android.R.attr.borderlessButtonStyle)
        button.setTextColor(ResourcesCompat.getColor(resources, android.R.color.holo_red_dark, null))
        button.textSize = 15f
        val textViewTypeface = ResourcesCompat.getFont(this, R.font.montserrat_semibold)
        button.typeface = textViewTypeface
        button.setAllCaps(false)
        button.gravity = Gravity.END
        val buttonLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        buttonLayoutParams.gravity = Gravity.END
//        buttonLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.terms_small_margin)
        button.layoutParams = buttonLayoutParams
        button.text = resources.getString(R.string.trade_progress_remove_button)
        button.setOnClickListener {
            view ->
            // same as subtracting 1
            progressNumber =- 1
            progress_linear_layout.removeView(view.parent as View)
        }
        return button
    }

    private fun createBigTextView(): TextView {
        //craft email textview
        val textView = TextView(this)
        textView.setPadding(10, 0, 10, 0)
//        textView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        if (Build.VERSION.SDK_INT < 23){
            textView.setTextAppearance(this, android.R.style.TextAppearance_Small)
        }else{
            textView.setTextAppearance(android.R.style.TextAppearance_Small)
        }
        val textViewTypeface = ResourcesCompat.getFont(this, R.font.montserrat_semibold)
        textView.typeface = textViewTypeface
        val textViewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        textViewLayoutParams.bottomMargin = 5
        textView.layoutParams = textViewLayoutParams
        textView.setAllCaps(true)
        return textView
    }

    private fun createSmallTextView(): TextView {
        //craft email text view
        val textView = TextView(this)
        textView.setPadding(10, 0, 10, 0)
//        textView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        if (Build.VERSION.SDK_INT < 23){
            textView.setTextAppearance(this, android.R.style.TextAppearance_Small)
        }else{
            textView.setTextAppearance(android.R.style.TextAppearance_Small)
        }
        val textViewTypeface = ResourcesCompat.getFont(this, R.font.montserrat_semibold)
        textView.typeface = textViewTypeface
        val textViewLayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        textViewLayoutParams.weight = 1f
        textViewLayoutParams.bottomMargin = 5
        textView.layoutParams = textViewLayoutParams
        textView.setAllCaps(true)
        return textView
    }

    private fun createBigEditText(): EditText {
        //craft edit Text
        val editText = EditText(this)
        editText.setPadding(20, 5, 20, 5)
        editText.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        editText.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        editText.maxLines = 1
        val viewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        viewLayoutParams.gravity = Gravity.CENTER
        viewLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.edit_text_margin_top)
        viewLayoutParams.bottomMargin = resources.getDimensionPixelSize(R.dimen.terms_margin_side)
        editText.layoutParams = viewLayoutParams
        val viewTypeface = ResourcesCompat.getFont(this, R.font.montserrat_regular)
        editText.typeface = viewTypeface
        editText.background = ResourcesCompat.getDrawable(resources, R.drawable.profile_edit_shape, null)
        return editText
    }

    private fun createSmallNumberEditText(): EditText {
        //craft edit Text
        val editText = EditText(this)
        editText.setPadding(20, 5, 20, 5)
        editText.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        editText.inputType = InputType.TYPE_CLASS_PHONE
        editText.maxLines = 1
        val viewLayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        viewLayoutParams.weight = 1f
        viewLayoutParams.gravity = Gravity.CENTER
        viewLayoutParams.marginEnd = resources.getDimensionPixelSize(R.dimen.terms_margin)
        viewLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.edit_text_margin_top)
        viewLayoutParams.bottomMargin = resources.getDimensionPixelSize(R.dimen.terms_margin)
        editText.layoutParams = viewLayoutParams
        val viewTypeface = ResourcesCompat.getFont(this, R.font.montserrat_regular)
        editText.typeface = viewTypeface
        editText.background = ResourcesCompat.getDrawable(resources, R.drawable.profile_edit_shape, null)
        return editText
    }

    //input type in number hence used for the amount side as well
    private fun createNumberEditText(): EditText {
        //craft edit Text
        val editText = EditText(this)
        editText.setPadding(20, 5, 20, 5)
        editText.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        editText.inputType = InputType.TYPE_CLASS_PHONE
        editText.maxLines = 1
        val viewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        viewLayoutParams.gravity = Gravity.CENTER
        viewLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.edit_text_margin_top)
        viewLayoutParams.bottomMargin = resources.getDimensionPixelSize(R.dimen.terms_margin_side)
        editText.layoutParams = viewLayoutParams
        val viewTypeface = ResourcesCompat.getFont(this, R.font.montserrat_regular)
        editText.typeface = viewTypeface
        editText.background = ResourcesCompat.getDrawable(resources, R.drawable.profile_edit_shape, null)
        return editText
    }

    private fun createHugeEditText():EditText{
        //craft edit Text
        val editText = EditText(this)
        editText.setPadding(20, 5, 20, 5)
        editText.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        editText.inputType = InputType.TYPE_TEXT_FLAG_AUTO_CORRECT
        editText.setSingleLine(false)
        val viewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 250)
        viewLayoutParams.gravity = Gravity.CENTER
        viewLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.edit_text_margin_top)
        viewLayoutParams.bottomMargin = resources.getDimensionPixelSize(R.dimen.terms_margin_side)
        editText.layoutParams = viewLayoutParams
        val viewTypeface = ResourcesCompat.getFont(this, R.font.montserrat_regular)
        editText.typeface = viewTypeface
        editText.background = ResourcesCompat.getDrawable(resources, R.drawable.profile_edit_shape, null)
        return editText
    }

    private fun validateTheEditTexts():Boolean{
        var validate = false
        val emptyText = Validator.traverseEditTexts(progress_linear_layout)
        //if null then it didn't find any edit text without text
        if (emptyText == null){
            validate = true
        }

        return validate
    }
}
