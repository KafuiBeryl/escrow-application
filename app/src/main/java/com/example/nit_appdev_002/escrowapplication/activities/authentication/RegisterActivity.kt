package com.example.nit_appdev_002.escrowapplication.activities.authentication

import android.app.Application
import android.content.DialogInterface
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.home.HomeActivity
import com.example.nit_appdev_002.escrowapplication.apicalls.RestAPI
import com.example.nit_appdev_002.escrowapplication.apiobjects.RegisterResult
import com.example.nit_appdev_002.escrowapplication.utilities.*
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.set
import com.example.nit_appdev_002.escrowapplication.viewmodels.UserViewModel
import com.github.omadahealth.lollipin.lib.managers.AppLock
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.please_wait.*
import java.lang.ref.WeakReference
import com.google.gson.GsonBuilder


class RegisterActivity : AppCompatActivity() {

    companion object {
        @JvmStatic
        private lateinit var mUserViewModel:UserViewModel

        class RegisterAsyncTask(activity: RegisterActivity) : AsyncTask<String, Void, RegisterResult>(){
            private val activityReference: WeakReference<RegisterActivity>

            private val application:Application

            init{
                activityReference = WeakReference(activity)
                application = activity.application
            }

            override fun onPreExecute() {
                super.onPreExecute()
                val activity = activityReference.get()
                activity?.please_wait_group?.visibility = View.VISIBLE
            }

            override fun doInBackground(vararg params: String): RegisterResult? {
                try {
                    val api = RestAPI()
                    val callResponse = api.register(params[0], params[1], params[2])
                    val response = callResponse.execute()
                    if (response.isSuccessful || response.body() != null){
                        return response.body() as RegisterResult
                    }else if (response.errorBody() != null){
                        val gson = GsonBuilder().create()
                        try {
                            val mError:RegisterResult = gson.fromJson<RegisterResult>(response.errorBody()?.string(), RegisterResult::class.java)
                            return mError
                        }catch (e:Throwable){
                            Log.e("RegisterDesiariaTask", e.message)
                        }
                    }
                }catch (e:Throwable){
                    Log.e("RegisterTask", e.message)
                }
                return null
            }

            override fun onPostExecute(result: RegisterResult?) {
                super.onPostExecute(result)
                val activity = activityReference.get()
                if (result == null){
                    activity?.please_wait_group?.visibility = View.GONE
                    Toast.makeText(application, R.string.connection_failed, Toast.LENGTH_LONG).show()
                }else if (!TextUtils.isEmpty(result.token)){
                    val prefs = PreferenceHelper.defaultPrefs(application)
                    prefs[PREF_TOKEN] = result.token
                    prefs[PREF_TOKEN_EXPIRY] = result.tokenExpiryDate
                    prefs[PREF_USER_ID] = result.user!!.id
                    if (result.userTradeRoles != null){
                        mUserViewModel.insertUserTradeRoles(result.userTradeRoles)
                    }
                    mUserViewModel.insertUser(result.user)
                    activity?.please_wait_group?.visibility = View.GONE
                    val intent = Intent(application, CustomPinActivity::class.java)
                    intent.putExtra(AppLock.EXTRA_TYPE, AppLock.ENABLE_PINLOCK)
                    activity?.startActivityForResult(intent, REQUEST_FIRST_RUN_PIN)
                }else{
                    activity?.please_wait_group?.visibility = View.GONE
                    val builder:AlertDialog.Builder = AlertDialog.Builder(activity!!)
                    builder.setTitle(R.string.alert_title)
                            .setMessage(result.error)
                            .setIcon(android.R.drawable.ic_menu_edit)
                            .setNeutralButton(android.R.string.ok, DialogInterface.OnClickListener {
                                dialog, which ->

                            })
                    val dialog = builder.create()
                    dialog.show()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        please_wait_group.visibility = View.GONE

        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        val emailText = register_email_txt.text
        val passwordText = register_password_txt.text
        val confirmText = register_confirm_txt.text


        register_submit_btn.setOnClickListener {
            if (TextUtils.isEmpty(emailText)||TextUtils.isEmpty(passwordText)||TextUtils.isEmpty(confirmText)|| !passwordText.toString().contentEquals(confirmText.toString())){
                Toast.makeText(this, R.string.register_empty_text, Toast.LENGTH_LONG).show()
            }else{
                RegisterAsyncTask(this).execute(emailText.toString(), passwordText.toString(), confirmText.toString())
            }
        }

        have_account.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == PIN_SUCCEEDED){
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }else{
            mUserViewModel.logoutThings()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }
}

