package com.example.nit_appdev_002.escrowapplication.activities.trade.view

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.ViewStub
import android.view.ViewTreeObserver
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.authentication.LoginActivity
import com.example.nit_appdev_002.escrowapplication.activities.chat.ChatLandingActivity
import com.example.nit_appdev_002.escrowapplication.activities.payment.PaymentActivity
import com.example.nit_appdev_002.escrowapplication.apicalls.OtherNetworkCalls
import com.example.nit_appdev_002.escrowapplication.apicalls.RetrofitInstance
import com.example.nit_appdev_002.escrowapplication.apiobjects.*
import com.example.nit_appdev_002.escrowapplication.entities.Trade
import com.example.nit_appdev_002.escrowapplication.utilities.*
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.set
import com.example.nit_appdev_002.escrowapplication.viewmodels.TransactionViewModel
import com.example.nit_appdev_002.escrowapplication.viewmodels.UserViewModel
import com.github.omadahealth.lollipin.lib.PinCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.content_trade_details.*
import kotlinx.android.synthetic.main.fab.*
import kotlinx.android.synthetic.main.partial_trade_details.*
import kotlinx.android.synthetic.main.partial_trade_details_agent.*
import kotlinx.android.synthetic.main.toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TradeDetailsActivity : PinCompatActivity() {

    companion object {
        @JvmStatic
        private lateinit var mTransactionViewModel: TransactionViewModel

        @JvmStatic
        private lateinit var selectedTrade:Trade

        @JvmStatic
        private lateinit var mUserViewModel: UserViewModel

        @JvmStatic
        private lateinit var userEmail:String

        @JvmStatic
        val SEND_PAYMENT_ACTIVITY_REQUEST_CODE = 1

    }

    private var selectedTradeId:Int? = null

    private val TRANSLATION_Y = "translationY"

    private var expanded:Boolean = false

    private var offset1:Float = 0f
    private var offset2:Float = 0f
//    private var offset3:Float = 0f
    private var offset4:Float = 0f
    private var offset5:Float = 0f
    private var offset6:Float = 0f
    private var offset7:Float = 0f

    private lateinit var prefs: SharedPreferences

    private lateinit var service: OtherNetworkCalls
    private var token:String? = null
    private var tokenString:String? = null
    private var userId:Int? = null
    private var canInvite:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trade_details)
        setSupportActionBar(custom_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        //initialize the view model
        mTransactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel::class.java)
        //initiate the view model object
        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        prefs = PreferenceHelper.defaultPrefs(this)
        token = prefs[PREF_TOKEN]
        tokenString = "Bearer $token"

        service = RetrofitInstance.getRetrofitInstance()!!.create(OtherNetworkCalls::class.java)

        if (savedInstanceState == null){
            selectedTradeId = intent.getIntExtra(INTENT_SELECTED_TRADE_ID, 0)
            prefs[PREF_TRADE_SELECTED] = selectedTradeId
        }else{
            selectedTradeId = savedInstanceState.getInt(INTENT_SELECTED_TRADE_ID)
            prefs[PREF_TRADE_SELECTED] = selectedTradeId
        }

        mTransactionViewModel.getSingleTrade(selectedTradeId!!).observe(this, Observer {
            trade ->
            if (trade != null){
                selectedTrade = trade
                setValues(selectedTrade)
            }else{
                checkIfTokenExists()
            }
        })

        mTransactionViewModel.nukeExtras()

        userId = prefs[PREF_USER_ID]
        mUserViewModel.getUser(userId!!).observe(this, Observer {
            user ->
            if (user != null){
                userEmail = user.email
                canInvite = user.accountCompleted
            }
        })
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        getCurrentTradeExtrasIfAny()

        setVisibilitiesAsNeeded()

        setImageButtonListeners()

        setOnPreDrawListenerForFAB()
    }

    private fun setVisibilitiesAsNeeded(){
        trade_details_collapse.visibility = View.GONE
        btn_view_beneficiaries.visibility = View.GONE
        txt_view_beneficiaries.visibility = View.GONE
        btn_view_notification.visibility = View.GONE
        txt_view_notification.visibility = View.GONE
        btn_view_milestones.visibility = View.GONE
        txt_view_milestone.visibility = View.GONE
        btn_view_delivery.visibility = View.GONE
        txt_view_delivery.visibility = View.GONE
        fab_edit.visibility = View.GONE
        fab_pay.visibility = View.GONE
        fab_accept.visibility = View.GONE
        fab_decline.visibility = View.GONE
        fab_cancel.visibility = View.GONE
        fab_invite.visibility = View.GONE
    }

    private fun setOnPreDrawListenerForFAB(){
        fab_container.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener{
            override fun onPreDraw(): Boolean {
                fab_container.viewTreeObserver.removeOnPreDrawListener(this)
                offset1 = fab.y - (fab_edit.y - 10f)
                fab_edit.translationY = offset1
                fab_edit.translationX = (fab.x - (fab_edit.x - 18f))
                offset2 = fab.y - (fab_pay.y - 10f)
                fab_pay.translationY = offset2
                fab_pay.translationX = (fab.x - (fab_pay.x - 18f))
//                offset3 = fab.y - (fab_chat.y - 10f)
//                fab_chat.translationY = offset3
//                fab_chat.translationX = (fab.x - (fab_chat.x - 18f))
                offset4 = fab.y - (fab_accept.y - 10f)
                fab_accept.translationY = offset4
                fab_accept.translationX = (fab.x - (fab_accept.x - 18f))
                offset5 = fab.y - (fab_decline.y - 10f)
                fab_decline.translationY = offset5
                fab_decline.translationX = (fab.x - (fab_decline.x - 18f))
                offset6 = fab.y - (fab_invite.y - 10f)
                fab_invite.translationY = offset6
                fab_invite.translationX = (fab.x - (fab_invite.x - 18f))
                offset7 = fab.y - (fab_cancel.y - 10f)
                fab_cancel.translationY = offset7
                fab_cancel.translationX = (fab.x - (fab_cancel.x - 18f))
                return true
            }
        })
    }

    private fun setImageButtonListeners(){
//        chat has been put on hold

//        fab_chat.setOnClickListener {
//            fabChat()
//        }

        fab_edit.setOnClickListener {
            fabEdit()
        }

        fab_pay.setOnClickListener {
            fabPay()
        }

        fab_accept.setOnClickListener {
            showQueryDialogue("Accept", resources.getString(R.string.accept_string), this::sendTradeDecision)
        }

        fab_decline.setOnClickListener {
            showQueryDialogue("Decline", resources.getString(R.string.decline_string), this::sendTradeDecision)
        }

        fab_cancel.setOnClickListener {
            showQueryDialogue("Cancel", resources.getString(R.string.cancel_string), this::sendTradeDecision)
        }

        fab_invite.setOnClickListener {
            showInviteQueryDialogue(resources.getString(R.string.invite_string), this::sendInvitation)
        }

        fab.setOnClickListener {
            expanded = !expanded
            if (expanded){
                expandFab()
            }else{
                collapseFab()
            }
        }

        btn_view_beneficiaries.setOnClickListener {
            val intent = Intent(this, TradeExtrasDetailsActivity::class.java)
            intent.putExtra(INTENT_SELECTED_EXTRA, "Beneficiary")
            startActivity(intent)
        }

        btn_view_milestones.setOnClickListener {
            val intent = Intent(this, TradeExtrasDetailsActivity::class.java)
            intent.putExtra(INTENT_SELECTED_EXTRA, "MileStone")
            startActivity(intent)
        }

        btn_view_notification.setOnClickListener {
            val intent = Intent(this, TradeExtrasDetailsActivity::class.java)
            intent.putExtra(INTENT_SELECTED_EXTRA, "Notification")
            startActivity(intent)
        }

        btn_view_delivery.setOnClickListener {
            val intent = Intent(this, TradeExtrasDetailsActivity::class.java)
            intent.putExtra(INTENT_SELECTED_EXTRA, "Delivery")
            startActivity(intent)
        }
    }

    private fun getCurrentTradeExtrasIfAny(){
        if (token != null){
            val tradeIdRequest = GetSingleTradeRequest(selectedTradeId!!)
            val service: OtherNetworkCalls = RetrofitInstance.getRetrofitInstance()!!.create(OtherNetworkCalls::class.java)
            val call = service.getTradeExtras(tokenString!!, tradeIdRequest)
            call.enqueue(object : Callback<CreateTradeExtrasResult>{
                override fun onResponse(call: Call<CreateTradeExtrasResult>, response: Response<CreateTradeExtrasResult>) {
                    if (response.body() != null){
                        val result = response.body() as CreateTradeExtrasResult
                        mTransactionViewModel.insertTradesMilestones(result.milestones)
                        mTransactionViewModel.insertTradeBeneficiaries(result.beneficiaries)
                        mTransactionViewModel.insertTradeDeliveryAddress(result.deliveryAddress)
                        mTransactionViewModel.insertTradeNotifications(result.notificationGang)
                    }else if (response.errorBody() != null){
                        val gson = GsonBuilder().create()
                        val errorResult:CreateTradeExtrasResult = gson.fromJson<CreateTradeExtrasResult>(response.errorBody()?.string(), CreateTradeExtrasResult::class.java)
                        Log.e("TradeExtraRequest", errorResult.error)
                        unAuthorisedToast(errorResult.error)
                    }
                }

                override fun onFailure(call: Call<CreateTradeExtrasResult>, t: Throwable) {
                    if (t.message != null){
                        Log.e("TradeExtraRequestError", t.message.toString())
                        unAuthorisedToast(t.message.toString())
                    }
                }
            })
        }else{
            unAuthorisedToast("Invalid Token.")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SEND_PAYMENT_ACTIVITY_REQUEST_CODE){
            if (data != null){
                val message = data.getStringExtra(INTENT_PAYMENT_REPLY)
                showDialogue(message)
            }
        }else{
            Toast.makeText(this, "Payment not successful", Toast.LENGTH_LONG).show()
        }

        if (resultCode == PIN_FAILED || resultCode == PIN_FORGOT){
            mUserViewModel.logoutThings()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    private fun showDialogue(message:String?){
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.result)
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton(android.R.string.ok){_, _ ->
                }
        val dialog = builder.create()
        dialog.show()
    }

    private fun showQueryDialogue(title:String, message:String, onConfirm:(String)->Unit){
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.confirmation)
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton(android.R.string.ok){dialogue, _ ->
//                    dialogue.dismiss()
                    onConfirm(title)
                }
                .setNegativeButton(android.R.string.cancel){dialogue,_->
//                    dialogue.dismiss()
                    collapseFab()
                }
        val dialog = builder.create()
        dialog.show()
    }

    private fun showInviteQueryDialogue(message:String, onConfirm:()->Unit){
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.confirmation)
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton(android.R.string.ok){dialogue, _ ->
//                    dialogue.dismiss()
                    onConfirm()
                }
                .setNegativeButton(android.R.string.cancel){dialogue,_->
//                    dialogue.dismiss()
                    collapseFab()
                }
        val dialog = builder.create()
        dialog.show()
    }

    private fun sendTradeDecision(decision:String){
        if (token != null){
            val decisionRequest = DecisionRequest(selectedTradeId!!, decision)
            val call = service.postSendUserTradeDecision(tokenString!!, decisionRequest)
            call.enqueue(object :Callback<DecisionResult>{
                override fun onResponse(call: Call<DecisionResult>, response: Response<DecisionResult>) {
                    if (response.body() != null){
                        val result = response.body() as DecisionResult
                        mTransactionViewModel.updateTrade(result.trade)
                        collapseFab()
                        setValues(result.trade)
                    }else if (response.errorBody() != null){
                        val gson = GsonBuilder().create()
                        val errorResult:DecisionResult = gson.fromJson<DecisionResult>(response.errorBody()?.string(), DecisionResult::class.java)
                        Log.e("DecisionRequest", errorResult.error)
                        unSuccessfulToast(errorResult.error)
                    }
                }

                override fun onFailure(call: Call<DecisionResult>, t: Throwable) {
                    Log.e("DecisionRequestError", t.message.toString())
                    unSuccessfulToast(t.message.toString())
                }
            })
        }else{
            unAuthorisedToast("Invalid Token.")
        }
    }

    private fun sendInvitation(){
        if (token != null){
            val invitationRequest = InvitationRequest(selectedTradeId!!)
            val call = service.postSendTradeInvitation(tokenString!!, invitationRequest)
            call.enqueue(object :Callback<InvitationResult>{
                override fun onResponse(call: Call<InvitationResult>, response: Response<InvitationResult>) {
                    if (response.body() != null){
                        val result = response.body() as InvitationResult
                        mTransactionViewModel.updateTrade(result.trade)
                        collapseFab()
                        setValues(result.trade)
                        unSuccessfulToast("${result.successText} Unfortunately, ${result.unfortunatelyText}")
                    }else if (response.errorBody() != null){
                        val gson = GsonBuilder().create()
                        val errorResult:InvitationResult = gson.fromJson<InvitationResult>(response.errorBody()?.string(), InvitationResult::class.java)
                        Log.e("InvitationRequest", errorResult.error)
                        unSuccessfulToast(errorResult.error)
                    }
                }

                override fun onFailure(call: Call<InvitationResult>, t: Throwable) {
                    Log.e("InvitationRequestError", t.message.toString())
                    unSuccessfulToast(t.message.toString())
                }
            })
        }else{
            unAuthorisedToast("Invalid Token.")
        }
    }

    fun toggleView(view:View){
        if (trade_details_collapse.isShown){
            Animator.slideUp(this, trade_details_collapse)
            trade_details_collapse.visibility = View.GONE
            toggle_image.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp)
        }else{
            trade_details_collapse.visibility = View.VISIBLE
            toggle_image.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp)
            Animator.slideDown(this, trade_details_collapse)
        }
    }

    private fun checkIfTokenExists(){
        val token:String? = prefs[PREF_TOKEN]
        if (token != null){
            getTradeIfNotAvailable("Bearer $token")
        }else{
            unAuthorisedToast("Invalid Token.")
        }
    }

    private fun getTradeIfNotAvailable(token:String){
        val getSingleTradeRequest = GetSingleTradeRequest(selectedTradeId!!)
        val call = service.getTradeById(token, getSingleTradeRequest)
        call.enqueue(object : Callback<TradeResult>{
            override fun onResponse(call: Call<TradeResult>, response: Response<TradeResult>) {
                if (response.body() != null){
                    val result = response.body() as TradeResult
                    val list = mutableListOf<Trade>()
                    list.add(result.trade!!)
                    mTransactionViewModel.insertTrades(list)
                    setValues(result.trade!!)
                }else if (response.errorBody() != null){
                    val gson = GsonBuilder().create()
                    val errorResult:TradeResult = gson.fromJson<TradeResult>(response.errorBody()?.string(), TradeResult::class.java)
                    Log.e("GetSingleTradeRequest", errorResult.error)
                    unSuccessfulToast(errorResult.error)
                }
            }

            override fun onFailure(call: Call<TradeResult>, t: Throwable) {
                if (t.message != null){
                    Log.e("GetTradeRequestError", t.message.toString())
                    unSuccessfulToast(t.message.toString())
                }
            }
        })
    }

    private fun unAuthorisedToast(error:String?){
        Toast.makeText(this, "$error ...Please logout and login again. You will be unable to make any transactions otherwise", Toast.LENGTH_SHORT).show()
        finish()
    }

    private fun unSuccessfulToast(error:String?){
        Toast.makeText(this, "$error ...", Toast.LENGTH_SHORT).show()
        finish()
    }

    private fun setValues(trade: Trade){
        val name = String.format(" %s", trade.tradeName)
        trade_name.text = name
        trade_step.text = trade.tradeProcessStep?.processName
        if (trade.hasBeneficiary!!){
            btn_view_beneficiaries.visibility = View.VISIBLE
            txt_view_beneficiaries.visibility = View.VISIBLE
        }
        if (trade.hasNotifications!!){
            btn_view_notification.visibility = View.VISIBLE
            txt_view_notification.visibility = View.VISIBLE
        }
        if (trade.isProgress!!){
            btn_view_milestones.visibility = View.VISIBLE
            txt_view_milestone.visibility = View.VISIBLE
        }
        if (trade.isDelivery!!){
            btn_view_delivery.visibility = View.VISIBLE
            txt_view_delivery.visibility = View.VISIBLE
        }
        txt_trade_name.text = trade.tradeName
        txt_trade_description.text = trade.description
        txt_trade_amount.text = GeneralHelper.formatAmountToString(trade.tradeAmount)
        txt_trade_seller_email.text = trade.sellerEmail
        txt_trade_seller_amount.text = GeneralHelper.formatAmountToString(trade.sellerFee)
        txt_trade_buyer_email.text = trade.buyerEmail
        txt_trade_buyer_amount.text = GeneralHelper.formatAmountToString(trade.buyerFee)
        txt_trade_escrow_fee.text = GeneralHelper.formatAmountToString(trade.escrowFee)
        txt_trade_escrow_fee_allocation.text = trade.escrowFeeAllocation?.adminPayeeAllocation
        txt_trade_has_beneficiaries.text = GeneralHelper.formatBoolean(trade.hasBeneficiary)
        txt_trade_has_notification.text = GeneralHelper.formatBoolean(trade.hasNotifications)
        txt_trade_is_progress.text = GeneralHelper.formatBoolean(trade.isProgress)
        txt_trade_is_delivery.text = GeneralHelper.formatBoolean(trade.isDelivery)

        if (trade.agentEmail != null){
            val stub = ViewStub(this)
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
            stub.layoutParams = layoutParams
            stub.layoutResource = R.layout.partial_trade_details_agent
            trade_details_agent.addView(stub)
            stub.inflate()

            txt_trade_agent_email.text = trade.agentEmail
            txt_trade_agent_amount.text = GeneralHelper.formatAgentFee(trade.agentFeeType?.feeType, trade.unCalculatedAgentFee, trade.agentFee)
            txt_trade_agent_fee_allocation.text = trade.agentFeeAllocation?.standardPayeeAllocation
        }

        if (trade.sellerEmail == userEmail || trade.buyerEmail == userEmail || trade.userId == userId){
            if (trade.tradeProcessStep!!.stepNumber < 4){
                fab_edit.visibility = View.VISIBLE
            }
        }

        if (trade.buyerEmail == userEmail && trade.tradeProcessStep!!.stepNumber == 4){
            fab_pay.visibility = View.VISIBLE
        }

        if (trade.sellerEmail == userEmail || trade.buyerEmail == userEmail || trade.agentEmail == userEmail){
            if (trade.userId != userId && (trade.tradeProcessStep!!.stepNumber in 2..3 || trade.tradeProcessStep!!.stepNumber == 11)){
                fab_accept.visibility = View.VISIBLE
                fab_decline.visibility = View.VISIBLE
            }
        }

        if (trade.userId == userId){
            fab_cancel.visibility = View.VISIBLE
            if (trade.tradeProcessStep!!.stepNumber == 1){
                fab_invite.visibility = View.VISIBLE
//                fab_invite.isEnabled = canInvite
            }
        }

        if (!fab_accept.isShown && !fab_decline.isShown && !fab_pay.isShown &&
                !fab_edit.isShown && !fab_invite.isShown && !fab_cancel.isShown){
            fab.visibility = View.GONE
        }
    }

    private fun collapseFab(){
        fab.setImageResource(R.drawable.animated_minus)
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(createCollapseAnimator(fab_edit, offset1),
                createCollapseAnimator(fab_pay, offset2),
                createCollapseAnimator(fab_accept, offset4),
                createCollapseAnimator(fab_decline, offset5),
                createCollapseAnimator(fab_invite, offset6),
                createCollapseAnimator(fab_cancel, offset7))
        animatorSet.start()
        animateFab()
    }

    private fun expandFab(){
        fab.setImageResource(R.drawable.animated_plus)
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(createExpandAnimator(fab_edit, offset1),
                createExpandAnimator(fab_pay, offset2),
                createExpandAnimator(fab_accept, offset4),
                createExpandAnimator(fab_decline, offset5),
                createExpandAnimator(fab_invite, offset6),
                createExpandAnimator(fab_cancel, offset7))
        animatorSet.start()
        animateFab()
    }

    private fun createCollapseAnimator(view: View, offset:Float):android.animation.Animator{
        return ObjectAnimator.ofFloat(view, TRANSLATION_Y, 0f, offset)
                .setDuration(resources.getInteger(android.R.integer.config_mediumAnimTime).toLong())
    }

    private fun createExpandAnimator(view: View, offset: Float):android.animation.Animator{
        return ObjectAnimator.ofFloat(view, TRANSLATION_Y, offset, 0f)
                .setDuration(resources.getInteger(android.R.integer.config_mediumAnimTime).toLong())
    }

    private fun animateFab(){
        val drawable = fab.drawable
        if (drawable is Animatable){
            (drawable as Animatable).start()
        }
    }

    private fun fabEdit(){
        val intent = Intent(this, EditTradeActivity::class.java)
        startActivity(intent)
    }

//      chat put on ice for the time being
//    private fun fabChat(){
//        val intent = Intent(this, ChatLandingActivity::class.java)
//        startActivity(intent)
//    }

    private fun fabPay(){
        val intent = Intent(this, PaymentActivity::class.java)
        startActivityForResult(intent, SEND_PAYMENT_ACTIVITY_REQUEST_CODE)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onNewIntent(intent: Intent?) {
        if (intent != null){
            setIntent(intent)
        }
    }
}
