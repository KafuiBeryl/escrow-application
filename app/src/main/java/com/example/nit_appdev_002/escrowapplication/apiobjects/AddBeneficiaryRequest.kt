package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.google.gson.annotations.SerializedName

data class AddBeneficiaryRequest (
        @SerializedName("trade_id")
        var tradeId:Int,

        @SerializedName("beneficiaries")
        var beneficiaries: List<SentBeneficiary>
)