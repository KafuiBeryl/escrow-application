package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.google.gson.annotations.SerializedName

data class InvitationRequest (
        @SerializedName("trade_id")
        var tradeId:Int
)