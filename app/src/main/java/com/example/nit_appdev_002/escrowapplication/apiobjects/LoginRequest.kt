package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.google.gson.annotations.SerializedName

data class LoginRequest(
        @SerializedName("email")
        var email:String,
        @SerializedName("password")
        var password:String
)