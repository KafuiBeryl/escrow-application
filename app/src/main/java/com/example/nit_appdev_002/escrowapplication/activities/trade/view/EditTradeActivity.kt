package com.example.nit_appdev_002.escrowapplication.activities.trade.view

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bhargavms.podslider.PodSlider
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.adapters.EditTradePagerAdapter
import com.example.nit_appdev_002.escrowapplication.apiobjects.EditTradeRequest
import com.example.nit_appdev_002.escrowapplication.apiobjects.SentBeneficiary
import com.example.nit_appdev_002.escrowapplication.entities.NotificationGang
import com.example.nit_appdev_002.escrowapplication.entities.Trade
import com.example.nit_appdev_002.escrowapplication.entities.TradeMileStones
import com.example.nit_appdev_002.escrowapplication.fragments.edit.*
import com.example.nit_appdev_002.escrowapplication.utilities.PREF_IS_PROCESS_PAYMENT
import com.example.nit_appdev_002.escrowapplication.utilities.PREF_TRADE_SELECTED
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.viewmodels.TransactionViewModel
import kotlinx.android.synthetic.main.content_edit_trade.*
import kotlinx.android.synthetic.main.please_wait.*
import kotlinx.android.synthetic.main.toolbar.*

class EditTradeActivity : AppCompatActivity(), EditTradeProgressFragment.OnFragmentInteractionListener,
        EditTradeBeneficiariesFragment.OnFragmentInteractionListener, EditTradeDescriptionFragment.OnFragmentInteractionListener,
        EditTradeDeliveryFragment.OnFragmentInteractionListener, EditTradeNotificationFragment.OnFragmentInteractionListener,
        EditTradePartiesFragment.OnFragmentInteractionListener {

    companion object {
        @JvmStatic
        val editTradeRequest = EditTradeRequest(0, null,
                null, null, null, 0,
                null, null, false, false,
                false, false, false, false,
                0, null, null, null,
                null, 0, 0, 0, 0, "GHS",
                null)

        @JvmStatic
        var selectedTrade: Trade? = null

        @JvmStatic
        private lateinit var mTransactionViewModel: TransactionViewModel

        @JvmStatic
        val beneficiaries = ArrayList<SentBeneficiary>()

        @JvmStatic
        val mileStones = ArrayList<TradeMileStones>()

        @JvmStatic
        val notificationGang = ArrayList<NotificationGang>()
    }

    private lateinit var prefs: SharedPreferences

    private val adapter = EditTradePagerAdapter(supportFragmentManager)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_trade)
        setSupportActionBar(custom_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        //hide please wait
        please_wait_group.visibility = View.GONE

        //initialize the view model
        mTransactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel::class.java)
        //get stored tradeId
        prefs = PreferenceHelper.defaultPrefs(this)
        val tradeId:Int? = prefs[PREF_TRADE_SELECTED]
        mTransactionViewModel.getSingleTrade(tradeId!!).observe(this, Observer {
            trade ->
            if (trade != null){
                selectedTrade = trade
                editTradeRequest.tradeId = trade.id!!
                //set viewpager to podslider
                setSliderToViewPager()
            }
        })

        //make the podslider unclickable
        pod_slider.setPodClickListener {

        }
    }

    private fun setSliderToViewPager(){
        //set total number of pods
        pod_slider.setNumberOfPods(6)
        val person: Drawable = makeImage(R.drawable.add_person_icon)
        val beneficiary: Drawable = makeImage(R.drawable.beneficiaries_icon)
//        val briefcase: Drawable = makeImage(R.drawable.briefcase_icon)
        val delivery: Drawable =  makeImage(R.drawable.delivery_icon)
        val tradeDescription: Drawable = makeImage(R.drawable.trade_description_icon)
        val notificationIcon: Drawable = makeImage(R.drawable.notification_icon)
        val progressIcon: Drawable = makeImage(R.drawable.progress_icon)

        val drawables = arrayOf<Drawable>(person, tradeDescription,
                progressIcon, beneficiary, notificationIcon, delivery)

        //set it up with the viewpager
        edit_trade_view_pager.adapter = adapter
        pod_slider.setUpWithViewPager(edit_trade_view_pager)
        //set the drawables to the podslider
        pod_slider.setPodDrawables(drawables, PodSlider.DrawableSize.FIT_LARGE_CIRCLE)
    }

    private fun makeImage(id: Int):Drawable{
        val drawable = ResourcesCompat.getDrawable(resources, id, null)!!
        drawable.setTint(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        return drawable
    }

    override fun onNextPressed(fragment: Fragment) {
        when (fragment){
            is EditTradePartiesFragment -> {
                edit_trade_view_pager.setCurrentItem(1, true)
            }
            is EditTradeDescriptionFragment -> {
                val prefs = PreferenceHelper.defaultPrefs(this)
                val isProcess:Boolean? = prefs[PREF_IS_PROCESS_PAYMENT]
                if (isProcess!!){
                    edit_trade_view_pager.setCurrentItem(2, true)
                }else{
                    edit_trade_view_pager.setCurrentItem(3, true)
                }
            }
            is EditTradeProgressFragment -> {
                edit_trade_view_pager.setCurrentItem(3, true)
            }
            is EditTradeBeneficiariesFragment -> {
                edit_trade_view_pager.setCurrentItem(4, true)
            }
            is EditTradeNotificationFragment -> {
                edit_trade_view_pager.setCurrentItem(5, true)
            }
        }
    }

    override fun onBackButtonPressed(fragment: Fragment) {
        when (fragment){
            is EditTradeDescriptionFragment -> {
                adapter.onBackButtonPressed(EditTradeDescriptionFragment())
                edit_trade_view_pager.setCurrentItem(0, true)
            }
            is EditTradeProgressFragment -> {
                adapter.onBackButtonPressed(EditTradeProgressFragment())
                edit_trade_view_pager.setCurrentItem(1, true)
            }
            is EditTradeBeneficiariesFragment -> {
                val prefs = PreferenceHelper.defaultPrefs(this)
                val isProcess:Boolean? = prefs[PREF_IS_PROCESS_PAYMENT]
                if (isProcess!!){
                    edit_trade_view_pager.setCurrentItem(2, true)
                }else{
                    adapter.onBackButtonPressed(EditTradeBeneficiariesFragment())
                    edit_trade_view_pager.setCurrentItem(1, true)
                }
            }
            is EditTradeNotificationFragment -> {
                edit_trade_view_pager.setCurrentItem(3, true)
            }
            is EditTradeDeliveryFragment -> {
                edit_trade_view_pager.setCurrentItem(4, true)
            }
        }
    }



    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
