package com.example.nit_appdev_002.escrowapplication.activities.home

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.authentication.RegisterActivity
import com.example.nit_appdev_002.escrowapplication.utilities.PREF_AGREE
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.set
import kotlinx.android.synthetic.main.activity_terms.*

class TermsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms)

        //get default prefs
        val prefs = PreferenceHelper.defaultPrefs(this)

        agree_btn.isEnabled = false

        checkboxAgree.setOnCheckedChangeListener { _, isChecked ->
            when(isChecked){
                true -> agree_btn.isEnabled = true
                false -> agree_btn.isEnabled = false
            }
        }

        agree_btn.setOnClickListener {
            prefs[PREF_AGREE] = true
            startActivity(Intent(this, RegisterActivity::class.java))
            finish()
        }
    }
}
