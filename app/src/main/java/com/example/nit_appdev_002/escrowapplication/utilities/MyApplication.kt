package com.example.nit_appdev_002.escrowapplication.utilities

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.authentication.CustomPinActivity
import com.example.nit_appdev_002.escrowapplication.database.EscrowRoomDatabase
import com.github.omadahealth.lollipin.lib.managers.LockManager

class MyApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        val lockManager = LockManager.getInstance()
        lockManager.enableAppLock(this, CustomPinActivity::class.java)
        lockManager.appLock.logoId = R.drawable.ic_safe_transact_logo
        lockManager.appLock.setShouldShowForgot(true)
//        lockManager.appLock.setOnlyBackgroundTimeout(true)
        lockManager.appLock.timeout = 1000
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    fun logout(){
        val db = EscrowRoomDatabase
        db.destroyInstance()

    }
}