package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "identification_type")
data class IdentificationType (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "identityTypeId")
        @SerializedName("id")
        var identityTypeId:Int,

        @ColumnInfo(name = "identification_type")
        @SerializedName("identification_type")
        var type:String
)