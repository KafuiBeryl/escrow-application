package com.example.nit_appdev_002.escrowapplication.activities.profile

import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.authentication.LoginActivity
import com.example.nit_appdev_002.escrowapplication.apicalls.OtherNetworkCalls
import com.example.nit_appdev_002.escrowapplication.apicalls.RetrofitInstance
import com.example.nit_appdev_002.escrowapplication.apiobjects.BillingDetailsRequest
import com.example.nit_appdev_002.escrowapplication.apiobjects.ProfileResult
import com.example.nit_appdev_002.escrowapplication.utilities.*
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.viewmodels.UserViewModel
import com.github.omadahealth.lollipin.lib.PinCompatActivity
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_billing_details.*
import kotlinx.android.synthetic.main.please_wait.*
import kotlinx.android.synthetic.main.toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BillingDetailsActivity : PinCompatActivity() {

    companion object {
        @JvmStatic
        private lateinit var mUserViewModel: UserViewModel

        @JvmStatic
        private var selectedRegion:String? = null
    }
    private lateinit var prefs: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_billing_details)
        setSupportActionBar(custom_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        //instantiate ViewModel object
        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        prefs = PreferenceHelper.defaultPrefs(this)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        setUpSpinnersAndListener()
    }

    private fun setUpSpinnersAndListener(){
        //make sure progress ring isn't showing initially even though it is in the layout
        please_wait_group.visibility = View.GONE
        //set onItemSelected listener for the spinner
        spinner_billing_region.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                selectedRegion = parent.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedRegion = null
            }
        }

        //set the adapter for the spinner
        val adapter = ArrayAdapter.createFromResource(this, R.array.region_array, R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_billing_region.adapter = adapter

        billing_details_submit_btn.setOnClickListener {
            billing_linear_layout.visibility = View.GONE
            please_wait_group.visibility = View.VISIBLE
            validateEntry()
        }

        setValues(adapter)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == PIN_FAILED || resultCode == PIN_FORGOT){
            mUserViewModel.logoutThings()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setValues(adapter:ArrayAdapter<CharSequence>){
//        val prefs = PreferenceHelper.defaultPrefs(this)
        //get the user id from the shared preferences
        val userId:Int? = prefs[PREF_USER_ID]
        if (userId != null){
            mUserViewModel.getUser(userId).observe(this, Observer {
                user ->
                txt_billing_line_1.text = Editable.Factory.getInstance().newEditable(user?.billAddress1 ?: "")
                txt_billing_line_2.text = Editable.Factory.getInstance().newEditable(user?.billAddress2 ?: "")
                txt_billing_district.text = Editable.Factory.getInstance().newEditable(user?.billAddressSuburb ?: "")
                txt_billing_city.text = Editable.Factory.getInstance().newEditable(user?.billAddressCity ?: "")
//                txt_billing_country.text = Editable.Factory.getInstance().newEditable(user?.billAddressCountry ?: "")
                txt_billing_digital.text = Editable.Factory.getInstance().newEditable(user?.digitalAddress ?: "")
                txt_billing_post_code.text = Editable.Factory.getInstance().newEditable(user?.billAddressPostCode ?: "")
                if (user?.billAddressRegion != null){
                    val region = user.billAddressRegion
                    val position = adapter.getPosition(region)
                    spinner_billing_region.setSelection(position)
                }
            })
        }
    }

    private fun validateEntry(){
        if (TextUtils.isEmpty(txt_billing_line_1.text) || TextUtils.isEmpty(txt_billing_line_2.text)
                || TextUtils.isEmpty(txt_billing_district.text) || TextUtils.isEmpty(txt_billing_city.text)
                || TextUtils.isEmpty(txt_billing_country.text) || TextUtils.isEmpty(txt_billing_digital.text)
                || selectedRegion == null || TextUtils.isEmpty(txt_billing_post_code.text)){
            billing_linear_layout.visibility = View.VISIBLE
            please_wait_group.visibility = View.GONE
            Toast.makeText(this, R.string.login_empty_text, Toast.LENGTH_LONG).show()
        }else{
            postRequest()
        }
    }

    private fun postRequest(){
        val billLine1 = txt_billing_line_1.text.toString()
        val billLine2 = txt_billing_line_2.text.toString()
        val district = txt_billing_district.text.toString()
        val city = txt_billing_city.text.toString()
        val country = txt_billing_country.text.toString()
        val digitalAddress = txt_billing_digital.text.toString()
        val region = selectedRegion!!
        val postCode = txt_billing_post_code.text.toString()

        val billingDetailsRequest = BillingDetailsRequest(billLine1, billLine2, district, city,
                region, country, postCode, digitalAddress)

        retrofitCall(billingDetailsRequest)
    }

    private fun retrofitCall(billingDetailsRequest: BillingDetailsRequest){
//        val prefs = PreferenceHelper.defaultPrefs(this)
        val token:String? = prefs[PREF_TOKEN]
        if (token != null){
            val service: OtherNetworkCalls = RetrofitInstance.getRetrofitInstance()!!.create(OtherNetworkCalls::class.java)
            val tokenString = "Bearer $token"
            val call = service.postBillingDetails(tokenString, billingDetailsRequest)
            call.enqueue(object : Callback<ProfileResult> {
                override fun onResponse(call: Call<ProfileResult>?, response: Response<ProfileResult>?) {
                    if (response?.body() != null){
                        val result = response.body() as ProfileResult
                        mUserViewModel.updateUser(result.user)
                        billing_linear_layout.visibility = View.VISIBLE
                        please_wait_group.visibility = View.GONE
                        startActivity(Intent(this@BillingDetailsActivity, ProfileActivity::class.java))
                        finish()
                    }else if (response?.errorBody() != null){
                        val gson = GsonBuilder().create()
                        val errorResult: ProfileResult = gson.fromJson<ProfileResult>(response.errorBody()?.string(), ProfileResult::class.java)
                        billing_linear_layout.visibility = View.VISIBLE
                        please_wait_group.visibility = View.GONE
                        showDialogueBox(errorResult.error)
                    }
                }

                override fun onFailure(call: Call<ProfileResult>?, t: Throwable?) {
                    if (t?.message != null){
                        billing_linear_layout.visibility = View.VISIBLE
                        please_wait_group.visibility = View.GONE
                        showDialogueBox(t.message.toString())
                    }
                }
            })
        }
    }

    private fun showDialogueBox(errorMessage:String?){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.alert_title)
                .setMessage(errorMessage)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setNeutralButton(android.R.string.ok, DialogInterface.OnClickListener {
                    dialog, which ->
                    dialog.cancel()
                })
        val dialog = builder.create()
        dialog.show()
    }

}
