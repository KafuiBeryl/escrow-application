package com.example.nit_appdev_002.escrowapplication.activities.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.authentication.LoginActivity
import com.example.nit_appdev_002.escrowapplication.apicalls.OtherNetworkCalls
import com.example.nit_appdev_002.escrowapplication.apicalls.RetrofitInstance
import com.example.nit_appdev_002.escrowapplication.apiobjects.UploadImageRequest
import com.example.nit_appdev_002.escrowapplication.apiobjects.UploadImageResult
import com.example.nit_appdev_002.escrowapplication.listeners.IImageTaskListener
import com.example.nit_appdev_002.escrowapplication.miscobjects.InsertUserImage
import com.example.nit_appdev_002.escrowapplication.utilities.*
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.viewmodels.MiscViewModel
import com.example.nit_appdev_002.escrowapplication.viewmodels.TransactionViewModel
import com.example.nit_appdev_002.escrowapplication.viewmodels.UserViewModel
import com.github.omadahealth.lollipin.lib.PinCompatActivity
import com.google.android.material.navigation.NavigationView
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.big_toolbar.*
import kotlinx.android.synthetic.main.content_profile.*
import kotlinx.android.synthetic.main.nav_header_home.*
import org.jetbrains.anko.design.navigationView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executors

class ProfileActivity : PinCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    companion object {
        @JvmStatic
        private lateinit var mUserViewModel: UserViewModel
    }
    private var token:String? = null
    private var imageCompressTask: ImageCompressTask? = null
    private var id:Int? = null
    private val mExecutorService = Executors.newFixedThreadPool(1)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        setSupportActionBar(custom_big_toolbar)

        val toggle = ActionBarDrawerToggle(
                this, profile_drawer_layout, custom_big_toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        profile_drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        profile_nav_view.setNavigationItemSelectedListener(this)

        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        val prefs = PreferenceHelper.defaultPrefs(this)
        id = prefs[PREF_USER_ID]
        token = prefs[PREF_TOKEN]
        mUserViewModel.getUserImage(id!!).observe(this, Observer {
            imagePath ->
            if(imagePath != null){
                val imageByteArray = Base64.decode(imagePath, Base64.URL_SAFE)
                setProfileImage(imageByteArray)
            }else{
                profile_image.setImageResource(R.drawable.profile_placeholder)
            }
        })

        btn_profile_personal.setOnClickListener {
            startActivity(Intent(this, PersonalDetailsActivity::class.java))
        }

        btn_profile_banking.setOnClickListener {
            startActivity(Intent(this, BankingDetailsActivity::class.java))
        }

        btn_profile_billing.setOnClickListener {
            startActivity(Intent(this, BillingDetailsActivity::class.java))
        }

        btn_profile_company.setOnClickListener {
            startActivity(Intent(this, CompanyDetailsActivity::class.java))
        }

        profile_image.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            intent.type = "image/jpeg|image/png"
//            intent.action = Intent.ACTION_GET_CONTENT
            intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/jpeg", "image/png"))
            startActivityForResult(intent, RC_SELECT_IMAGE)
//            startActivityForResult(Intent.createChooser(intent, "Select Profile Image"), RC_SELECT_IMAGE)
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        setDrawerValues()
    }

    private fun setDrawerValues(){
        mUserViewModel.getUser(id!!).observe(this, Observer {
            user->
            if (user != null){
                val string = "${user.firstName} ${user.lastName}"
                if (user.imagePath != null){
                    Glide.with(this)
                            .load(Base64.decode(user.imagePath, Base64.URL_SAFE))
                            .apply(RequestOptions()
                                    .fitCenter()
                                    .placeholder(R.drawable.profile_placeholder))
                            .into(drawer_imageView)
                }
                drawer_main_textView.text = string
                drawer_sub_textView.text = user.email
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SELECT_IMAGE && resultCode == Activity.RESULT_OK &&
                data != null && data.data != null){
            val selectedImagePath = data.data!!.toString()
            imageCompressTask = ImageCompressTask(this, selectedImagePath, iImageTaskListener)
            mExecutorService.execute(imageCompressTask)
        }

        if (resultCode == PIN_FAILED || resultCode == PIN_FORGOT){
            mUserViewModel.logoutThings()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    private val iImageTaskListener = object: IImageTaskListener{
        override fun onComplete(compressed: String) {
            uploadImage(compressed)
        }

        override fun onError(error: Throwable) {
            Toast.makeText(this@ProfileActivity, error.message.toString(), Toast.LENGTH_LONG).show()
        }
    }

    private fun uploadImage(imageString: String){
        if (token != null){
            val tokenString = "Bearer $token"
            val service = RetrofitInstance.getRetrofitInstance()!!.create(OtherNetworkCalls::class.java)
            val call = service.postUploadImage(tokenString, UploadImageRequest(imageString))
            call.enqueue(object : Callback<UploadImageResult>{
                override fun onResponse(call: Call<UploadImageResult>, response: Response<UploadImageResult>) {
                    if (response.body() != null){
                        val result = response.body() as UploadImageResult
                        mUserViewModel.addUserImage(InsertUserImage(id!!, result.imageByteArray))
                        val imageByteArray = Base64.decode(result.imageByteArray, Base64.URL_SAFE )
                        setProfileImage(imageByteArray)
                        Toast.makeText(this@ProfileActivity, "Profile image has been successfully changed.", Toast.LENGTH_SHORT).show()
                    }else if (response.errorBody() != null){
                        val gson = GsonBuilder().create()
                        val errorResult:UploadImageResult = gson.fromJson<UploadImageResult>(response.errorBody()?.string(), UploadImageResult::class.java)
                        Log.e("UploadRequest", errorResult.error)
                        Toast.makeText(this@ProfileActivity, errorResult.error, Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<UploadImageResult>, t: Throwable) {
                    if (t.message != null){
                        Log.e("UploadRequestError", t.message.toString())
                        Toast.makeText(this@ProfileActivity, t.message.toString(), Toast.LENGTH_LONG).show()
                    }
                }
            })
        }
    }

    private fun setProfileImage(byteArray: ByteArray){
        Glide.with(this)
                .load(byteArray)
                .apply(RequestOptions()
                        .fitCenter()
                        .placeholder(R.drawable.profile_placeholder))
                .into(profile_image)
    }

    override fun onBackPressed() {
        if (profile_drawer_layout.isDrawerOpen(GravityCompat.START)) {
            profile_drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                // Handle the camera action
            }
            R.id.nav_new_trade -> {

            }
            R.id.nav_view_trade -> {

            }
            R.id.nav_profile -> {

            }
            R.id.nav_personal -> {

            }
            R.id.nav_billing -> {

            }
        }

        profile_drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
