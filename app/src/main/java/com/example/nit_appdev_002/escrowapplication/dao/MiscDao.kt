package com.example.nit_appdev_002.escrowapplication.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.nit_appdev_002.escrowapplication.entities.*

@Dao
interface MiscDao {
    @Insert(onConflict = REPLACE)
    fun insertBanks(bank: Bank)

    @Insert(onConflict = REPLACE)
    fun insertIndustries(industry: Industry)

    @Insert(onConflict = REPLACE)
    fun insertCompanyCats(companyCategory: CompanyCategory)

    @Insert(onConflict = REPLACE)
    fun insertFeeSchedule(feeSchedule: FeeSchedule)

    @Insert(onConflict = REPLACE)
    fun insertBankAccountTypes(bankAccountType: BankAccountType)

    @Insert(onConflict = REPLACE)
    fun insertIdentificationTypes(identificationType: IdentificationType)

    @Insert(onConflict = REPLACE)
    fun insertAdminFeeAllocation(adminFeeAllocation: AdminFeeAllocation)

    @Insert(onConflict = REPLACE)
    fun insertStandardFeeAllocation(standardFeeAllocation: StandardFeeAllocation)

    @Insert(onConflict = REPLACE)
    fun insertFeeType(feeType: FeeType)

    @Insert(onConflict = REPLACE)
    fun insertBeneficiaryType(beneficiaryType: BeneficiaryType)

    @Insert(onConflict = REPLACE)
    fun insertOccupations(occupation: Occupation)

    @Query("select * from fee_schedule")
    fun getFeeSchedule(): LiveData<List<FeeSchedule>>

    @Query("select bank_name from bank")
    fun getBankNames(): LiveData<List<String>>

    @Query("select category from company_category")
    fun getCompanyCategories(): LiveData<List<String>>

    @Query("select industry_classification from industry")
    fun getIndustries(): LiveData<List<String>>

    @Query("select account_type from bank_account_type")
    fun getBankAccountTypes(): LiveData<List<String>>

    @Query("select identification_type from identification_type")
    fun getIdentificationTypes(): LiveData<List<String>>

    @Query("select payee_allocation from admin_fee_allocation")
    fun getAdminFeeAllocationAgent(): LiveData<List<String>>

    @Query("select payee_allocation from admin_fee_allocation where agent = 0")
    fun getAdminFeeAllocation(): LiveData<List<String>>

    @Query("select standard_payee_allocation from standard_fee_allocation")
    fun getStandardFeeAllocation(): LiveData<List<String>>

    @Query("select fee_type from fee_type")
    fun getFeeType(): LiveData<List<String>>

    @Query("select beneficiary_type from beneficiary_type")
    fun getBeneficiaryType(): LiveData<List<String>>

    @Query("select occupation from occupations")
    fun getOccupations(): LiveData<List<String>>

    @Query("delete from industry")
    fun nukeIndustryTable()

    @Query("delete from company_category")
    fun nukeCompanyCategoryTable()

    @Query("delete from fee_schedule")
    fun nukeFeeScheduleTable()

    @Query("delete from bank")
    fun nukeBanksTable()

    @Query("delete from bank_account_type")
    fun nukeBankAccountTypeTable()

    @Query("delete from identification_type")
    fun nukeIdentificationTypeTable()

    @Query("delete from admin_fee_allocation")
    fun nukeAdminAllocationTable()

    @Query("delete from standard_fee_allocation")
    fun nukeStandardAllocationTable()

    @Query("delete from fee_type")
    fun nukeFeeTypeTable()

    @Query("delete from beneficiary_type")
    fun nukeBeneficiaryTypeTable()

    @Query("delete from occupations")
    fun nukeOccupationTable()

}