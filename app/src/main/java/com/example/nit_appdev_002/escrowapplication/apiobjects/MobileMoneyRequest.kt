package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.google.gson.annotations.SerializedName

data class MobileMoneyRequest (
        @SerializedName("trade_id")
        var tradeId:String,

        @SerializedName("description")
        var description:String,

        @SerializedName("subscriber_number")
        var subscriberNumber:String,

        @SerializedName("network")
        var network:String
)