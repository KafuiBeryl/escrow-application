package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.example.nit_appdev_002.escrowapplication.entities.Trade
import com.google.gson.annotations.SerializedName

data class TradeResult (
        @SerializedName("trade")
        var trade:Trade?,
        @SerializedName("error")
        var error:String?
)