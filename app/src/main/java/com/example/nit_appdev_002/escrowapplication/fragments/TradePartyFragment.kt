package com.example.nit_appdev_002.escrowapplication.fragments

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.trade.CreateTradeActivity
import com.example.nit_appdev_002.escrowapplication.utilities.*
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.set
import com.example.nit_appdev_002.escrowapplication.viewmodels.UserViewModel
import kotlinx.android.synthetic.main.fragment_trade_party.*
import kotlinx.android.synthetic.main.partial_agent_added.*
import kotlinx.android.synthetic.main.partial_agent_role.*
import kotlinx.android.synthetic.main.partial_buyer_role.*
import kotlinx.android.synthetic.main.partial_seller_role.*


/**
 * A simple [Fragment] subclass.
 */
class TradePartyFragment : Fragment(), View.OnClickListener, TradeRoleFragment.OnTradeRoleListener, TradeDescriptionFragment.OnTradeDescriptionListener {

    companion object {
        @JvmStatic
        private var selectedRole:String? = null

        @JvmStatic
        private var userEmail:String? = null

        @JvmStatic
        private var userId:Int? = null

        @JvmStatic
        private var isAgentAdded:Boolean = false

        @JvmStatic
        private var isBackPressed:Boolean = false

        @JvmStatic
        private lateinit var mUserViewModel: UserViewModel
    }

    private var mListener: OnTradePartyListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_trade_party, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //initiate the view model object
        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)



        checkbox_agent_added.setOnCheckedChangeListener { _, isChecked ->
            when (isChecked){
                true -> {
                    when (selectedRole){
                        "Buyer" -> {
                            isAgentAdded = true
                            val agentStub = ViewStub(activity)
                            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                            agentStub.layoutParams = layoutParams
                            agentStub.layoutResource = R.layout.partial_agent_added
                            buyer_agent_added_linear_layout.addView(agentStub)
                            agentStub.inflate()
                        }

                        "Seller" -> {
                            isAgentAdded = true
                            val agentStub = ViewStub(activity)
                            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                            agentStub.layoutParams = layoutParams
                            agentStub.layoutResource = R.layout.partial_agent_added
                            seller_agent_added_linear_layout.addView(agentStub)
                            agentStub.inflate()
                        }
                    }
                }
                false -> {
                    when (selectedRole){
                        "Buyer" -> {
                            isAgentAdded = false
                            buyer_agent_added_linear_layout.removeAllViews()
                        }

                        "Seller" -> {
                            isAgentAdded = false
                            seller_agent_added_linear_layout.removeAllViews()
                        }
                    }
                }
            }
        }

        if (isBackPressed){
            setInsertedValues()
        }
    }

    private fun setInsertedValues(){
        val addedAlready = CreateTradeActivity.createTradeRequest
        when(selectedRole){
            "Agent" -> {
                seller_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.sellerEmail)
                seller_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.sellerEmail)
                buyer_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.buyerEmail)
                buyer_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.buyerEmail)
            }
            "Buyer" -> {
                buyer_seller_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.sellerEmail)
                buyer_seller_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.sellerEmail)
                if (isAgentAdded){
                    checkbox_agent_added.isChecked = true
                    agent_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.agentEmail)
                    agent_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.agentEmail)
                }
            }
            "Seller" -> {
                seller_buyer_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.buyerEmail)
                seller_buyer_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.buyerEmail)
                if (isAgentAdded){
                    checkbox_agent_added.isChecked = true
                    agent_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.agentEmail)
                    agent_confirm_email_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.agentEmail)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        btn_back.setOnClickListener(this)
        btn_next.setOnClickListener(this)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser){
            val prefs = PreferenceHelper.defaultPrefs(activity!!.applicationContext)
            selectedRole = prefs[PREF_ROLE_SELECTED]
            userId = prefs[PREF_USER_ID]
            if (selectedRole != null){
                if (!isBackPressed){
                    mUserViewModel.getUserEmail(userId!!).observe(this, Observer {
                        email ->
                        if (!TextUtils.isEmpty(email)){
                            userEmail = email!!
                        }
                    })
                    when (selectedRole){
                        "Agent" -> {
                            //set is agent added to true
                            isAgentAdded = true
                            checkbox_agent_added.visibility = View.GONE
                            //set the partial view
                            val stub = ViewStub(activity)
                            val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                            stub.layoutParams = secondLayoutParams
                            stub.layoutResource = R.layout.partial_agent_role
                            trade_role_linear_layout.addView(stub)
                            stub.inflate()
                        }

                        "Buyer" -> {
                            //set the partial view
                            checkbox_agent_added.isChecked = false

                            val stub = ViewStub(activity)
                            val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                            stub.layoutParams = secondLayoutParams
                            stub.layoutResource = R.layout.partial_buyer_role
                            trade_role_linear_layout.addView(stub)
                            stub.inflate()
                        }

                        "Seller" -> {
                            checkbox_agent_added.isChecked = false

                            //set the partial view
                            val stub = ViewStub(activity)
                            val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                            stub.layoutParams = secondLayoutParams
                            stub.layoutResource = R.layout.partial_seller_role
                            trade_role_linear_layout.addView(stub)
                            stub.inflate()
                        }
                    }
                }

            }
        }
    }

    private fun validateTheEditTexts():Boolean{
        var validate = false
        val emptyText = Validator.traverseEditTexts(trade_role_linear_layout)
        if (emptyText == null){
            when (selectedRole){
                "Agent" -> {
                    if (seller_email_txt.text.trim().toString().equals(seller_confirm_email_txt.text.trim().toString(), false)
                    && buyer_email_txt.text.toString().contentEquals(buyer_confirm_email_txt.text.toString())){
                        validate = true
                    }
                }
                "Buyer" -> {
                    if (buyer_seller_email_txt.text.toString().contentEquals(buyer_seller_confirm_email_txt.text.toString())){
                        when (isAgentAdded){
                            true -> {
                                if (agent_email_txt.text.toString().contentEquals(agent_confirm_email_txt.text.toString())){
                                    validate = true
                                }
                            }
                            false -> {
                                validate = true
                            }
                        }
                    }
                }
                "Seller" -> {
                    if (seller_buyer_email_txt.text.toString().contentEquals(seller_buyer_confirm_email_txt.text.toString())){
                        when (isAgentAdded){
                            true -> {
                                if (agent_email_txt.text.toString().contentEquals(agent_confirm_email_txt.text.toString())){
                                    validate = true
                                }
                            }
                            false -> {
                                validate = true
                            }
                        }
                    }
                }
            }
        }

        return validate
    }

    private fun passEntriesToVariable(){
        var sellerEmail = ""
        var buyerEmail = ""
        var agentEmail = ""
        val userEmail = userEmail!!

        when(selectedRole){
            "Agent" -> {
                sellerEmail = seller_email_txt.text.toString()
                buyerEmail = buyer_email_txt.text.toString()
                agentEmail = userEmail
            }
            "Buyer" -> {
                sellerEmail = buyer_seller_email_txt.text.toString()
                buyerEmail = userEmail
                if (isAgentAdded){
                    agentEmail = agent_email_txt.text.toString()
                }
            }
            "Seller" -> {
                sellerEmail = userEmail
                buyerEmail = seller_buyer_email_txt.text.toString()
                if (isAgentAdded){
                    agentEmail = agent_email_txt.text.toString()
                }
            }
        }
        CreateTradeActivity.createTradeRequest.buyerEmail = buyerEmail
        CreateTradeActivity.createTradeRequest.sellerEmail = sellerEmail
        CreateTradeActivity.createTradeRequest.agentEmail = agentEmail
    }

    override fun onPause() {
        super.onPause()
        btn_back.setOnClickListener(null)
        btn_next.setOnClickListener(null)
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnTradePartyListener) {
            mListener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnTradePartyListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onClick(v: View?) {
        when (v?.id){
            R.id.btn_back -> {
                if (mListener != null){
                    mListener?.onBackButtonPressed(this)
                    isAgentAdded = false
                }
            }

            R.id.btn_next -> {
                if (mListener != null && selectedRole != null){
                    //set preferences
                    val prefs = PreferenceHelper.defaultPrefs(activity!!.applicationContext)
                    val boolean = isAgentAdded
                    prefs[PREF_HAS_AGENT] = boolean
                    if (validateTheEditTexts()){
                        passEntriesToVariable()
                        mListener?.onNextPressed(this)
                    }else{
                        Toast.makeText(activity, R.string.trade_empty_text, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }

    }


    override fun onNextPressed(fragment: Fragment) {
        TODO("Not Needed")
    }

    override fun onBackButtonPressed(fragment: Fragment) {
        when (fragment){
            is TradeDescriptionFragment -> {
                isBackPressed = true
                Log.i("listener go bak", isBackPressed.toString())
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnTradePartyListener {
        fun onNextPressed(fragment: Fragment)

        fun onBackButtonPressed(fragment: Fragment)
    }

}
