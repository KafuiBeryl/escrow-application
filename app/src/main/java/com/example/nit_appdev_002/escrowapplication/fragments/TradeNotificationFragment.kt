package com.example.nit_appdev_002.escrowapplication.fragments


import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment

import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.trade.CreateTradeActivity
import com.example.nit_appdev_002.escrowapplication.entities.NotificationGang
import com.example.nit_appdev_002.escrowapplication.utilities.Validator
import kotlinx.android.synthetic.main.fragment_trade_notification.*


/**
 * A simple [Fragment] subclass.
 *
 */
class TradeNotificationFragment : Fragment(), View.OnClickListener {

    companion object {
        @JvmStatic
        private var notifyNumber: Int = 0

        @JvmStatic
        private var notifyEmailNumber: Int = 1200

        @JvmStatic
        private var notifyFirstNameNumber: Int = 1300

        @JvmStatic
        private var notifyLastNameNumber: Int = 1400
    }

    private var mListener : OnTradeNotificationListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_trade_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        pod_slider.setCurrentlySelectedPodAndAnimate(6)

        btn_add_notification.setOnClickListener {
            addLayout()
        }
    }

    private fun addLayout(){
        if (notifyNumber < 3){
            //basically a = a+b
            notifyNumber ++
            //craft the UI in code. This should be good again. Fingers crossed
            val bigLinearLayout = LinearLayout(activity)
            bigLinearLayout.orientation = LinearLayout.VERTICAL
            val linearLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
            bigLinearLayout.layoutParams = linearLayoutParams

            val notifyEmailTextView = createBigTextView()
            notifyEmailTextView.text = resources.getString(R.string.register_email)
            notifyEmailTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val notifyEmailEdit = createBigEditText()
            notifyEmailEdit.id = notifyEmailNumber + notifyNumber

            val notifyFirstNameTextView = createBigTextView()
            notifyFirstNameTextView.text = resources.getString(R.string.profile_first_name)
            notifyFirstNameTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val notifyFirstNameEdit = createBigEditText()
            notifyFirstNameEdit.id = notifyFirstNameNumber + notifyNumber

            val notifyLastNameTextView = createBigTextView()
            notifyLastNameTextView.text = resources.getString(R.string.profile_last_name)
            notifyLastNameTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val notifyLastNameEdit = createBigEditText()
            notifyLastNameEdit.id = notifyLastNameNumber + notifyNumber



            bigLinearLayout.addView(notifyEmailTextView)
            bigLinearLayout.addView(notifyEmailEdit)
            bigLinearLayout.addView(notifyFirstNameTextView)
            bigLinearLayout.addView(notifyFirstNameEdit)
            bigLinearLayout.addView(notifyLastNameTextView)
            bigLinearLayout.addView(notifyLastNameEdit)

            val removeButton = createRemoveBeneficiaryButton()

            bigLinearLayout.addView(removeButton)

            notification_linear_layout.addView(bigLinearLayout)

        }else{
            btn_add_notification.visibility = View.GONE
            Toast.makeText(activity, R.string.trade_too_many_text, Toast.LENGTH_LONG).show()
        }
    }

    private fun createRemoveBeneficiaryButton(): Button {
        val button = Button(activity, null, android.R.attr.borderlessButtonStyle)
        button.setTextColor(ResourcesCompat.getColor(resources, android.R.color.holo_red_dark, null))
        button.textSize = 15f
        val textViewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_semibold)
        button.typeface = textViewTypeface
        button.setAllCaps(false)
        button.gravity = Gravity.END
        val buttonLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        buttonLayoutParams.gravity = Gravity.END
//        buttonLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.terms_small_margin)
        button.layoutParams = buttonLayoutParams
        button.text = resources.getString(R.string.trade_progress_remove_button)
        button.setOnClickListener {
            view ->
            // same as subtracting 1
            notifyNumber --
            if (notifyNumber < 3){
                btn_add_notification.visibility = View.VISIBLE
            }
            notification_linear_layout.removeView(view.parent as View)
        }
        return button
    }


    private fun createBigTextView(): TextView {
        //craft normal text view
        val textView = TextView(activity)
        textView.setPadding(10, 0, 10, 0)
        //not setting text and text color here because it doesn't reflect in the UI.
        //have to call it at the top where this helper method is called
//        textView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        if (Build.VERSION.SDK_INT < 23){
            textView.setTextAppearance(activity, android.R.style.TextAppearance_Small)
        }else{
            textView.setTextAppearance(android.R.style.TextAppearance_Small)
        }
        val textViewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_semibold)
        textView.typeface = textViewTypeface
        val textViewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        textViewLayoutParams.bottomMargin = 5
        textView.layoutParams = textViewLayoutParams
        textView.setAllCaps(true)
        return textView
    }

    private fun createBigEditText(): EditText {
        //craft edit Text
        val editText = EditText(activity)
        editText.setPadding(20, 5, 20, 5)
        editText.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        editText.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        editText.maxLines = 1
        val viewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        viewLayoutParams.gravity = Gravity.CENTER
        viewLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.edit_text_margin_top)
        viewLayoutParams.bottomMargin = resources.getDimensionPixelSize(R.dimen.terms_margin_side)
        editText.layoutParams = viewLayoutParams
        val viewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_regular)
        editText.typeface = viewTypeface
        editText.background = ResourcesCompat.getDrawable(resources, R.drawable.profile_edit_shape, null)
        return editText
    }

    private fun validateTheEditTexts():Boolean{
        var validate = false
        when (notifyNumber == 0){
            true -> {
                validate = true
            }
            false -> {
                val emptyText = Validator.traverseEditTexts(notification_linear_layout)
                //if null then it didn't find any edit text without text
                if (emptyText == null){
                    validate = true
                }
            }
        }
        return validate
    }

    private fun passEntriesToVariable(){
        CreateTradeActivity.createTradeRequest.hasNotifications = true
        for (i in 1 until notifyNumber){
            val email = view!!.findViewById<EditText>(notifyEmailNumber.plus(i)).text.toString()
            val firstName = view!!.findViewById<EditText>(notifyFirstNameNumber.plus(i)).text.toString()
            val lastName = view!!.findViewById<EditText>(notifyLastNameNumber.plus(i)).text.toString()

            val notificationMember = NotificationGang(null, email, firstName, lastName, null, null, null)
            CreateTradeActivity.notificationGang.add(notificationMember)
        }
    }

    override fun onResume() {
        super.onResume()
        btn_back.setOnClickListener(this)
        btn_next.setOnClickListener(this)
    }

    override fun onPause() {
        super.onPause()
        btn_back.setOnClickListener(null)
        btn_next.setOnClickListener(null)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnTradeNotificationListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnTradeBeneficiaryListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onClick(v: View?) {
        when (v?.id){
            R.id.btn_back -> {
                if (mListener != null){
                    mListener?.onBackButtonPressed(this)
                }
            }
            R.id.btn_next -> {
                if (validateTheEditTexts()){
                    if (mListener != null){
                        if (notifyNumber != 0){
                            passEntriesToVariable()
                        }
                        mListener?.onNextPressed(this)
                    }
                }else{
                    Toast.makeText(activity, R.string.login_empty_text, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnTradeNotificationListener {
        fun onNextPressed(fragment: Fragment)

        fun onBackButtonPressed(fragment: Fragment)
    }
}
