package com.example.nit_appdev_002.escrowapplication.miscobjects

data class InsertUserImage (
        var id:Int,
        var image:String
)