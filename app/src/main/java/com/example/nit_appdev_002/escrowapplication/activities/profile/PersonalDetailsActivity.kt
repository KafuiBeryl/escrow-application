package com.example.nit_appdev_002.escrowapplication.activities.profile

import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.authentication.LoginActivity
import com.example.nit_appdev_002.escrowapplication.apicalls.OtherNetworkCalls
import com.example.nit_appdev_002.escrowapplication.apicalls.RetrofitInstance
import com.example.nit_appdev_002.escrowapplication.apiobjects.PersonalDetailsRequest
import com.example.nit_appdev_002.escrowapplication.apiobjects.ProfileResult
import com.example.nit_appdev_002.escrowapplication.utilities.*
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.viewmodels.MiscViewModel
import com.example.nit_appdev_002.escrowapplication.viewmodels.UserViewModel
import com.github.omadahealth.lollipin.lib.PinCompatActivity
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_personal_details.*
import kotlinx.android.synthetic.main.please_wait.*
import kotlinx.android.synthetic.main.toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PersonalDetailsActivity : PinCompatActivity(){

    companion object {
        @JvmStatic
        private var selectedIdentificationType:String? = null

        @JvmStatic
        private var selectedOccupation:String? = null

        @JvmStatic
        private lateinit var mUserViewModel: UserViewModel

        @JvmStatic
        private lateinit var miscViewModel: MiscViewModel
    }
    private lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personal_details)
        setSupportActionBar(custom_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        //instantiate ViewModel object
        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        //instantiate ViewModel object
        miscViewModel = ViewModelProviders.of(this).get(MiscViewModel::class.java)

        prefs = PreferenceHelper.defaultPrefs(this)

    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        setUpSpinners()
    }

    private fun setUpSpinners(){
        please_wait_group.visibility = View.GONE

        profile_email_txt.isEnabled = false

        spinner_identification.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedIdentificationType = parent?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedIdentificationType = null
            }
        }

        spinner_occupation.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedOccupation = parent?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedOccupation = null
            }
        }

        val identityArray = ArrayList<String>()
        val adapter = ArrayAdapter(this, R.layout.simple_spinner_item, identityArray)
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_identification.adapter = adapter

        val occupationArray = ArrayList<String>()
        val jobAdapter = ArrayAdapter(this, R.layout.simple_spinner_item, occupationArray)
        jobAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_occupation.adapter = jobAdapter

        miscViewModel.getIdentificationTypes().observe(this, Observer {
            types -> if (types != null){
            for (type in types){
                identityArray.add(type)
            }
            adapter.notifyDataSetChanged()
        }
        })

        miscViewModel.getOccupations().observe(this, Observer {
            jobs ->
            if (jobs != null){
                for (job in jobs){
                    occupationArray.add(job)
                }
                jobAdapter.notifyDataSetChanged()
            }
        })



        profile_personal_submit_btn.setOnClickListener {
            personal_linear_layout.visibility = View.GONE
            please_wait_group.visibility = View.VISIBLE
            validateEntry()
        }

        setValues(adapter, jobAdapter)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == PIN_FAILED || resultCode == PIN_FORGOT){
            mUserViewModel.logoutThings()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    private fun validateEntry(){
        if (TextUtils.isEmpty(profile_last_name_txt.text) || TextUtils.isEmpty(profile_first_name_txt.text)
        || TextUtils.isEmpty(profile_mobile_dial_code.text) || TextUtils.isEmpty(profile_mobile_number.text)
        || TextUtils.isEmpty(profile_home_dial_code.text) || TextUtils.isEmpty(profile_home_number.text)
        || selectedIdentificationType == null || selectedOccupation == null || TextUtils.isEmpty(profile_id_number.text)
        || TextUtils.isEmpty(profile_citizen_txt.text)){
            personal_linear_layout.visibility = View.VISIBLE
            please_wait_group.visibility = View.GONE
            Toast.makeText(this, R.string.login_empty_text, Toast.LENGTH_LONG).show()
        }else{
            postRequest()
        }
    }

    private fun postRequest(){
        val firstName = profile_first_name_txt.text.toString()
        val lastName = profile_last_name_txt.text.toString()
        val mobileDialCode = profile_mobile_dial_code.text.toString()
        val mobileNumber = profile_mobile_number.text.toString()
        val homeDialCode = profile_home_dial_code.text.toString()
        val homeNumber = profile_home_number.text.toString()
        val identityType = selectedIdentificationType!!
        val identityNumber = profile_id_number.text.toString()
        val citizen = profile_citizen_txt.text.toString()
        val occupation = selectedOccupation!!

        val personalDetailsRequest = PersonalDetailsRequest(firstName, lastName, mobileDialCode,
                mobileNumber, homeDialCode, homeNumber, identityType, identityNumber, citizen, occupation)
        retrofitCall(personalDetailsRequest)
    }

    private fun retrofitCall(personalDetailsRequest: PersonalDetailsRequest){
//        val prefs = PreferenceHelper.defaultPrefs(this)
        val token:String? = prefs[PREF_TOKEN]
        if (token != null){
            val service: OtherNetworkCalls = RetrofitInstance.getRetrofitInstance()!!.create(OtherNetworkCalls::class.java)
            val tokenString = "Bearer $token"
            val call = service.postPersonalDetails(tokenString, personalDetailsRequest)
            call.enqueue(object : Callback<ProfileResult>{
                override fun onResponse(call: Call<ProfileResult>?, response: Response<ProfileResult>?) {
                    if (response?.body() != null){
                        val result = response.body() as ProfileResult
                        val user = result.user
                        user.userOccupation = result.userOccupation
                        mUserViewModel.updateUser(user)
                        personal_linear_layout.visibility = View.VISIBLE
                        please_wait_group.visibility = View.GONE
                        startActivity(Intent(this@PersonalDetailsActivity, ProfileActivity::class.java))
                        finish()
                    }else if (response?.errorBody() != null){
                        val gson = GsonBuilder().create()
                        val errorResult:ProfileResult = gson.fromJson<ProfileResult>(response.errorBody()?.string(), ProfileResult::class.java)
                        personal_linear_layout.visibility = View.VISIBLE
                        please_wait_group.visibility = View.GONE
                        showDialogueBox(errorResult.error)
                    }
                }

                override fun onFailure(call: Call<ProfileResult>?, t: Throwable?) {
                    if (t?.message != null){
                        personal_linear_layout.visibility = View.VISIBLE
                        please_wait_group.visibility = View.GONE
                        showDialogueBox(t.message.toString())
                    }
                }
            })
        }
    }

    private fun showDialogueBox(errorMessage:String?){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.alert_title)
                .setMessage(errorMessage)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setNeutralButton(android.R.string.ok, DialogInterface.OnClickListener {
                    dialog, which ->
                    dialog.cancel()
                })
        val dialog = builder.create()
        dialog.show()
    }

    private fun setValues(adapter:ArrayAdapter<String>, jobAdapter:ArrayAdapter<String>){
//        val prefs = PreferenceHelper.defaultPrefs(this)
        //get the user id from the shared preferences
        val userId:Int? = prefs[PREF_USER_ID]
        if (userId != null){
            mUserViewModel.getUser(userId).observe(this, Observer {
                user ->
                profile_email_txt.text = Editable.Factory.getInstance().newEditable(user?.email)
                profile_first_name_txt.text = Editable.Factory.getInstance().newEditable(user?.firstName ?: "")
                profile_last_name_txt.text = Editable.Factory.getInstance().newEditable(user?.lastName ?: "")
                profile_mobile_number.text = Editable.Factory.getInstance().newEditable(user?.mobileNumber ?: "")
                profile_home_number.text = Editable.Factory.getInstance().newEditable(user?.homeNumber ?: "")
                if (user?.identification_type != null){
                    val identificationType = user.identification_type
                    val position = adapter.getPosition(identificationType!!.type)
                    spinner_identification.setSelection(position)
                }

                if (user?.userOccupation != null){
                    val job = user.userOccupation
                    val position = jobAdapter.getPosition(job!!.occupation)
                    spinner_occupation.setSelection(position)
                }

                profile_id_number.text = Editable.Factory.getInstance().newEditable(user?.identificationNumber ?: "")
//                profile_citizen_txt.text = Editable.Factory.getInstance().newEditable(user?.citizenship ?: "")
            })
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


}