package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "bank")
data class Bank(
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "bankId")
        @SerializedName("id")
        var bankId:Int,

        @ColumnInfo(name = "bank_code")
        @SerializedName("bank_code")
        var bankCode:String,

        @ColumnInfo(name = "bank_name")
        @SerializedName("bank_name")
        var bankName:String
)