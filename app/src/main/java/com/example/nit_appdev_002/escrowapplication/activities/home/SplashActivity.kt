package com.example.nit_appdev_002.escrowapplication.activities.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.example.nit_appdev_002.escrowapplication.activities.authentication.RegisterActivity
import com.example.nit_appdev_002.escrowapplication.utilities.PREF_AGREE
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.github.omadahealth.lollipin.lib.managers.LockManager

class SplashActivity : AppCompatActivity() {

    override fun onResume() {
        super.onResume()

        val prefs = PreferenceHelper.defaultPrefs(this)

        //get any value from prefs
        val agree: Boolean? = prefs[PREF_AGREE]

        if (agree==true){
            if (!LockManager.getInstance().appLock.isPasscodeSet){
                startActivity(Intent(this, RegisterActivity::class.java))
                finish()
            }else{
                startActivity(Intent(this, HomeActivity::class.java))
                finish()
            }
        }else{
            startActivity(Intent(this, StartActivity::class.java))
            finish()
        }
    }
}
