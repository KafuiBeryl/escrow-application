package com.example.nit_appdev_002.escrowapplication.adapters

import android.animation.AnimatorInflater
import android.animation.StateListAnimator
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.entities.UserTradeRoles

class ViewTradeRecyclerAdapter(context: Context) : RecyclerView.Adapter<ViewTradeRecyclerAdapter.TradeViewHolder>(){

    class TradeViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        var tradeName: TextView
        var tradeRole: TextView
        var context:Context
        var cardView: CardView
        init {
            tradeName = itemView.findViewById(R.id.trade_name)
            tradeRole = itemView.findViewById(R.id.trade_role)
            context = itemView.context
            cardView = itemView.findViewById(R.id.trade_card)

            cardView.setCardBackgroundColor(ResourcesCompat.getColor(itemView.resources, R.color.colorFader, null))
        }

        fun bind(userTradeRoles: UserTradeRoles){
            tradeName.text = userTradeRoles.tradeName
            val role = userTradeRoles.userRole
            tradeRole.text = role?.role

        }

        fun bindNull(){
            tradeName.text = context.resources.getString(R.string.no_trades)

        }
    }

    var onItemClick: (Int) -> Unit ={}
    private val mInflater: LayoutInflater
    private var mUserTrades:List<UserTradeRoles>? = null

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TradeViewHolder {
        val itemView = mInflater.inflate(R.layout.card_view_trade, parent, false)
        val holder = TradeViewHolder(itemView)

        /* Animation code from Kwaw*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val stateListAnimator: StateListAnimator = AnimatorInflater
                    .loadStateListAnimator(parent.context, R.animator.main_feed_elevate_click)
            holder.itemView.stateListAnimator = stateListAnimator
        }

        // add a click handler to ensure the CardView handles touch events
        // otherwise the animation won't work
        holder.itemView.setOnClickListener{
            onItemClick(holder.adapterPosition)
        }

        return holder
    }

    //set item to different holder if text is empty and
    //normal one if not
    // .
    override fun onBindViewHolder(holder: TradeViewHolder, position: Int) {
        val userTrade = mUserTrades?.get(position)
        if (userTrade != null){
            holder.bind(userTrade)
        }
    }

    fun setRoles(userTrades:List<UserTradeRoles>){
        mUserTrades = userTrades
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return mUserTrades?.size ?: 0
    }

    fun getItem(position: Int) : UserTradeRoles?{
        return mUserTrades?.get(position)
    }
}