package com.example.nit_appdev_002.escrowapplication.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.nit_appdev_002.escrowapplication.dao.MiscDao
import com.example.nit_appdev_002.escrowapplication.dao.TransactionDao
import com.example.nit_appdev_002.escrowapplication.dao.UserDao
import com.example.nit_appdev_002.escrowapplication.dao.UserTradeRoleDao
import com.example.nit_appdev_002.escrowapplication.entities.*
import com.example.nit_appdev_002.escrowapplication.utilities.DateTypeConverter

@TypeConverters(DateTypeConverter::class)
@Database(entities = [
    User::class,
    UserTradeRoles::class,
    Bank::class,
    Industry::class,
    FeeSchedule::class,
    CompanyCategory::class,
    BankAccountType::class,
    IdentificationType::class,
    AdminFeeAllocation::class,
    StandardFeeAllocation::class,
    FeeType::class,
    BeneficiaryType::class,
    Country::class,
    Trade::class,
    TradeProcess::class,
    TradeRole::class,
    TradeStatus::class,
    TradeMileStones::class,
    Beneficiary::class,
    DeliveryAddress::class,
    NotificationGang::class,
    Occupation::class], version = 2)
abstract class EscrowRoomDatabase : RoomDatabase(){
    abstract fun userDao() : UserDao
    abstract fun userTradeDao() : UserTradeRoleDao
    abstract fun miscDao(): MiscDao
    abstract fun transactionDao():TransactionDao

    companion object {
        @JvmStatic
        private var INSTANCE : EscrowRoomDatabase? = null

        //in order to create an instance of an abstract class the object: precursor has to be added
        //added only if u want to prepopulate the database via callback

        @JvmStatic
        fun getDatabase(context: Context):EscrowRoomDatabase?{
            if (INSTANCE == null){
                synchronized(EscrowRoomDatabase::class){
                    if (INSTANCE == null){
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                EscrowRoomDatabase::class.java, "escrow_db")
                                .fallbackToDestructiveMigration()
                                .build()
                    }
                }
            }
            return INSTANCE
        }

        fun destroyInstance(){
            INSTANCE = null
        }

    }
}