package com.example.nit_appdev_002.escrowapplication.activities.profile

import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.authentication.LoginActivity
import com.example.nit_appdev_002.escrowapplication.apicalls.OtherNetworkCalls
import com.example.nit_appdev_002.escrowapplication.apicalls.RetrofitInstance
import com.example.nit_appdev_002.escrowapplication.apiobjects.CompanyDetailsRequest
import com.example.nit_appdev_002.escrowapplication.apiobjects.ProfileResult
import com.example.nit_appdev_002.escrowapplication.utilities.*
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.set
import com.example.nit_appdev_002.escrowapplication.viewmodels.MiscViewModel
import com.example.nit_appdev_002.escrowapplication.viewmodels.UserViewModel
import com.github.omadahealth.lollipin.lib.PinCompatActivity
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_company_details.*
import kotlinx.android.synthetic.main.please_wait.*
import kotlinx.android.synthetic.main.toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CompanyDetailsActivity : PinCompatActivity() {

    companion object {
        @JvmStatic
        private lateinit var miscViewModel: MiscViewModel

        @JvmStatic
        private lateinit var mUserViewModel: UserViewModel

        @JvmStatic
        private var selectedCompanyCategory:String? = null
    }
    private lateinit var prefs:SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company_details)
        setSupportActionBar(custom_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        //instantiate ViewModel object
        miscViewModel = ViewModelProviders.of(this).get(MiscViewModel::class.java)
        //instantiate ViewModel object
        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        prefs = PreferenceHelper.defaultPrefs(this)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        setUpSpinnersAndListeners()
    }

    private fun setUpSpinnersAndListeners(){
        please_wait_group.visibility = View.GONE

        checkbox_company_account.setOnCheckedChangeListener { _, isChecked ->
            when(isChecked){
                true -> {
                    company_details_linear_layout.visibility = View.VISIBLE
                    prefs[PREF_IS_COMPANY] = true
                }
                false -> {
                    company_details_linear_layout.visibility = View.GONE
                    prefs[PREF_IS_COMPANY] = false
                }
            }
        }

        val companyAccount:Boolean? = prefs[PREF_IS_COMPANY]
        if (companyAccount == null){
            company_details_linear_layout.visibility = View.GONE
        }else if (companyAccount == true){
            checkbox_company_account.isChecked = true
            company_details_linear_layout.visibility = View.VISIBLE
        }else{
            checkbox_company_account.isChecked = false
            company_details_linear_layout.visibility = View.GONE
        }

        //create empty list and populate it in the observer
        val companyTypeArray = ArrayList<String>()
        val companyTypeAdapter = ArrayAdapter(this, R.layout.simple_spinner_item, companyTypeArray)
        companyTypeAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_company_type.adapter = companyTypeAdapter

        spinner_company_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                selectedCompanyCategory = parent.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedCompanyCategory = null
            }
        }

        miscViewModel.getCompanyCategories().observe(this, Observer {
            categories -> if (!categories!!.isEmpty()){
            for (category in categories){
                companyTypeArray.add(category)
            }
            companyTypeAdapter.notifyDataSetChanged()
        }
        })

        company_details_submit_btn.setOnClickListener {
            company_details_linear_layout.visibility = View.GONE
            please_wait_group.visibility = View.VISIBLE
            validateEntry()
        }

        setValues(companyTypeAdapter)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == PIN_FAILED || resultCode == PIN_FORGOT){
            mUserViewModel.logoutThings()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setValues(adapter:ArrayAdapter<String>){
//        val prefs = PreferenceHelper.defaultPrefs(this)
        //get the user id from the shared preferences
        val userId:Int? = prefs[PREF_USER_ID]
        if (userId != null){
            mUserViewModel.getUser(userId).observe(this, Observer {
                user ->
                txt_company_name.text = Editable.Factory.getInstance().newEditable(user?.companyName ?: "")
                txt_company_reg_number.text = Editable.Factory.getInstance().newEditable(user?.companyRegistrationNumber ?: "")
                txt_company_tin.text = Editable.Factory.getInstance().newEditable(user?.TIN ?: "")
                if (user?.company_type != null){
                    val companyCategory = user.company_type
                    val position = adapter.getPosition(companyCategory!!.category)
                    spinner_company_type.setSelection(position)
                }
            })
        }
    }

    private fun validateEntry(){
        if (TextUtils.isEmpty(txt_company_name.text) || TextUtils.isEmpty(txt_company_reg_number.text)
                || TextUtils.isEmpty(txt_company_tin.text) || selectedCompanyCategory == null){
            company_details_linear_layout.visibility = View.VISIBLE
            please_wait_group.visibility = View.GONE
            Toast.makeText(this, R.string.login_empty_text, Toast.LENGTH_LONG).show()
        }else{
            postRequest()
        }
    }

    private fun postRequest(){
        val companyName = txt_company_name.text.toString()
        val companyRegNumber = txt_company_reg_number.text.toString()
        val tin = txt_company_tin.text.toString()
        val companyType = selectedCompanyCategory!!

        val companyDetailsRequest = CompanyDetailsRequest(companyName, tin, companyRegNumber,
                companyType)
        retrofitCall(companyDetailsRequest)
    }

    private fun retrofitCall(companyDetailsRequest: CompanyDetailsRequest){
//        val prefs = PreferenceHelper.defaultPrefs(this)
        val token:String? = prefs[PREF_TOKEN]
        if (token != null){
            val service: OtherNetworkCalls = RetrofitInstance.getRetrofitInstance()!!.create(OtherNetworkCalls::class.java)
            val tokenString = "Bearer $token"
            val call = service.postCompanyDetails(tokenString, companyDetailsRequest)
            call.enqueue(object : Callback<ProfileResult> {
                override fun onResponse(call: Call<ProfileResult>?, response: Response<ProfileResult>?) {
                    if (response?.body() != null){
                        val result = response.body() as ProfileResult
                        mUserViewModel.updateUser(result.user)
                        company_details_linear_layout.visibility = View.VISIBLE
                        please_wait_group.visibility = View.GONE
                        startActivity(Intent(this@CompanyDetailsActivity, ProfileActivity::class.java))
                        finish()
                    }else if (response?.errorBody() != null){
                        val gson = GsonBuilder().create()
                        val errorResult: ProfileResult = gson.fromJson<ProfileResult>(response.errorBody()?.string(), ProfileResult::class.java)
                        company_details_linear_layout.visibility = View.VISIBLE
                        please_wait_group.visibility = View.GONE
                        showDialogueBox(errorResult.error)
                    }
                }

                override fun onFailure(call: Call<ProfileResult>?, t: Throwable?) {
                    if (t?.message != null){
                        company_details_linear_layout.visibility = View.VISIBLE
                        please_wait_group.visibility = View.GONE
                        showDialogueBox(t.message.toString())
                    }
                }
            })
        }
    }

    private fun showDialogueBox(errorMessage:String?){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.alert_title)
                .setMessage(errorMessage)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setNeutralButton(android.R.string.ok, DialogInterface.OnClickListener {
                    dialog, which ->
                    dialog.cancel()
                })
        val dialog = builder.create()
        dialog.show()
    }
}
