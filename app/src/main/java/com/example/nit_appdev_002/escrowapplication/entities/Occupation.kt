package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "occupations")
data class Occupation (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "occupationId")
        @SerializedName("id")
        var occupationId:Int,

        @ColumnInfo(name = "rank")
        @SerializedName("rank")
        var rank:Int,

        @ColumnInfo(name = "occupation")
        @SerializedName("occupation")
        var occupation:String
)