package com.example.nit_appdev_002.escrowapplication.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.example.nit_appdev_002.escrowapplication.fragments.edit.*

class EditTradePagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager),
        EditTradeDescriptionFragment.OnFragmentInteractionListener {

    private var mTradePartyFragment:EditTradePartiesFragment
    private var mTradeDescriptionFragment:EditTradeDescriptionFragment

    init {
        mTradePartyFragment = EditTradePartiesFragment()
        mTradeDescriptionFragment = EditTradeDescriptionFragment()
    }

    override fun getCount(): Int {
        return 6
    }

    override fun getItem(position: Int): Fragment {
        var fragment:Fragment? = null
        when(position){
            0 -> fragment = mTradePartyFragment
            1 -> fragment = mTradeDescriptionFragment
            2 -> fragment = EditTradeProgressFragment()
            3 -> fragment = EditTradeBeneficiariesFragment()
            4 -> fragment = EditTradeNotificationFragment()
            5 -> fragment = EditTradeDeliveryFragment()
        }
        return fragment!!
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun onBackButtonPressed(fragment: Fragment) {
        when (fragment){
            is EditTradeDescriptionFragment -> {
                mTradePartyFragment.onBackButtonPressed(EditTradeDescriptionFragment())
            }
            is EditTradeBeneficiariesFragment -> {
                mTradeDescriptionFragment.onBackButtonPressed(EditTradeBeneficiariesFragment())
            }
            is EditTradeProgressFragment -> {
                mTradeDescriptionFragment.onBackButtonPressed(EditTradeProgressFragment())
            }
        }
    }

    override fun onNextPressed(fragment: Fragment) {
        TODO("not necessary")
    }
}