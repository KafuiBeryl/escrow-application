package com.example.nit_appdev_002.escrowapplication.fragments.edit

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.trade.view.EditTradeActivity
import com.example.nit_appdev_002.escrowapplication.entities.FeeSchedule
import com.example.nit_appdev_002.escrowapplication.utilities.PREF_HAS_AGENT
import com.example.nit_appdev_002.escrowapplication.utilities.PREF_IS_PROCESS_PAYMENT
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.set
import com.example.nit_appdev_002.escrowapplication.utilities.Validator
import com.example.nit_appdev_002.escrowapplication.viewmodels.MiscViewModel
import kotlinx.android.synthetic.main.fragment_edit_trade_description.*
import kotlinx.android.synthetic.main.partial_agent_fee_description.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [EditTradeDescriptionFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 *
 */
class EditTradeDescriptionFragment : Fragment(), View.OnClickListener, EditTradeBeneficiariesFragment.OnFragmentInteractionListener,
        EditTradeProgressFragment.OnFragmentInteractionListener {
    companion object {
        @JvmStatic
        private var isAgentAdded:Boolean? = false

        @JvmStatic
        private var isProgressPayment:Boolean = false

        @JvmStatic
        private lateinit var miscViewModel: MiscViewModel

        @JvmStatic
        private var selectedIndustryClassification:String? = null

        @JvmStatic
        private var selectedAdminAllocation:String? = null

        @JvmStatic
        private var isBackPressed:Boolean = false

        @JvmStatic
        private var selectedAgentAllocation:String? = null

        @JvmStatic
        private var selectedAgentFeeType:String? = null
    }
    private var listener: OnFragmentInteractionListener? = null

    //declare admin allocation array
    private val adminAllocationArray = ArrayList<String>()

    //declare admin allocation array adapter
    private lateinit var adminAllocationAdapter: ArrayAdapter<String>

    //declare fee schedule array
    private val schedule = mutableListOf<FeeSchedule>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_trade_description, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //instantiate ViewModel object
        miscViewModel = ViewModelProviders.of(this).get(MiscViewModel::class.java)

        //set the adapter for the spinners
        val industryArray = ArrayList<String>()
        val industryAdapter = ArrayAdapter(activity!!.baseContext, R.layout.simple_spinner_item, industryArray)
        industryAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_industry_classification.adapter = industryAdapter


        adminAllocationAdapter = ArrayAdapter(activity!!.baseContext, R.layout.simple_spinner_item, adminAllocationArray)
        adminAllocationAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_admin_allocation.adapter = adminAllocationAdapter

        //set on click listeners to spinners
        spinner_admin_allocation.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedAdminAllocation = parent?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedAdminAllocation = null
            }
        }

        spinner_industry_classification.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedIndustryClassification = parent?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedIndustryClassification = null
            }
        }

        checkbox_is_progress.setOnCheckedChangeListener { _, isChecked ->
            when (isChecked){
                true -> {
                    isProgressPayment = true
                    progress_lock_linear_layout.visibility = View.GONE
                }
                false -> {
                    isProgressPayment = false
                    progress_lock_linear_layout.visibility = View.VISIBLE
                }
            }
        }

        miscViewModel.getIndustries().observe(this, Observer {
            industries -> if (!industries!!.isEmpty()){
            for (industry in industries){
                industryArray.add(industry)
            }
            industryAdapter.notifyDataSetChanged()
        }
        })

        miscViewModel.getFeeSchedule().observe(this, Observer {
            feeList ->
            if (!feeList!!.isEmpty()){
                for (item in feeList){
                    schedule.add(item)
                }
            }
        })

        if (!isBackPressed){
            setOldValues(industryAdapter, adminAllocationAdapter)
        }else{
            setNewValues(industryAdapter, adminAllocationAdapter)
        }
    }

    private fun setOldValues(industryAdapter:ArrayAdapter<String>, feeAllocationAdapter:ArrayAdapter<String>){
        val addedAlready = EditTradeActivity.selectedTrade!!
        trade_name_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.tradeName)
        val position = industryAdapter.getPosition(addedAlready.tradeIndustry?.classification)
        spinner_industry_classification.setSelection(position)
        trade_description_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.description)
        val allocationPosition = feeAllocationAdapter.getPosition(addedAlready.escrowFeeAllocation?.adminPayeeAllocation)
        spinner_admin_allocation.setSelection(allocationPosition)
//        checkbox_is_progress.isChecked = addedAlready.isProgress!!
//
//        if (!addedAlready.isProgress!!){
//            trade_amount_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.tradeAmount.toString())
//            delivery_time_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.deliveryTime)
//            inspection_time_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.inspectionTime)
//        }
        //set agent if exists
//        addAgentOnBackPressed()
    }

    private fun setNewValues(industryAdapter:ArrayAdapter<String>, feeAllocationAdapter:ArrayAdapter<String>){
        val addedAlready = EditTradeActivity.editTradeRequest
        trade_name_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.tradeName)
        val position = industryAdapter.getPosition(addedAlready.tradeCategory)
        spinner_industry_classification.setSelection(position)
        trade_description_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.description)
        val allocationPosition = feeAllocationAdapter.getPosition(addedAlready.escrowAllocation)
        spinner_admin_allocation.setSelection(allocationPosition)
//        checkbox_is_progress.isChecked = addedAlready.isProgressPayment
//
//        if (!addedAlready.isProgressPayment){
//            trade_amount_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.tradeAmount.toString())
//            delivery_time_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.deliveryTime)
//            inspection_time_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.inspectionPeriod)
//        }
        //set agent if exists
        addAgentOnBackPressed()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser){
            val context = activity?.applicationContext
            if (context != null){
                if (!isBackPressed){
                    val prefs = PreferenceHelper.defaultPrefs(context)
                    isAgentAdded = prefs[PREF_HAS_AGENT]!!
                    when (isAgentAdded) {
                        true -> {

                            //set the partial view
                            val stub = ViewStub(activity)
                            val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                            stub.layoutParams = secondLayoutParams
                            stub.layoutResource = R.layout.partial_agent_fee_description
                            agent_desc_linear_layout.addView(stub)
                            stub.inflate()

                            setAgentSpinners()
                            miscViewModel.getAgentAdminFeeAllocation().observe(this, Observer {
                                allocates ->
                                if (!allocates!!.isEmpty()){
                                    for (allocate in allocates){
                                        adminAllocationArray.add(allocate)
                                    }
                                    adminAllocationAdapter.notifyDataSetChanged()
                                }
                            })
                        }
                        false -> {
                            //remove agent partial view
                            agent_desc_linear_layout.removeAllViews()

                            miscViewModel.getAdminFeeAllocation().observe(this, Observer {
                                allocates ->
                                if (!allocates!!.isEmpty()){
                                    for (allocate in allocates){
                                        adminAllocationArray.add(allocate)
                                    }
                                    adminAllocationAdapter.notifyDataSetChanged()
                                }
                            })
                        }
                    }
                    val addedAlready = EditTradeActivity.selectedTrade!!
                    checkbox_is_progress.isChecked = addedAlready.isProgress!!

                    if (!addedAlready.isProgress!!){
                        trade_amount_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.tradeAmount.toString())
                        delivery_time_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.deliveryTime)
                        inspection_time_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.inspectionTime)
                    }
                }else{
                    val addedAlready = EditTradeActivity.editTradeRequest
                    if (!addedAlready.isProgressPayment){
                        trade_amount_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.tradeAmount.toString())
                        delivery_time_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.deliveryTime)
                        inspection_time_txt.text = Editable.Factory.getInstance().newEditable(addedAlready.inspectionPeriod)
                    }
                }
            }
        }
    }

    private fun addAgentOnBackPressed(){
        val context = activity?.applicationContext
        if (context != null){
            val prefs = PreferenceHelper.defaultPrefs(context)
            isAgentAdded = prefs[PREF_HAS_AGENT]!!
            adminAllocationArray.clear()
            when (isAgentAdded) {
                true -> {

                    //set the partial view
                    val stub = ViewStub(activity)
                    val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    stub.layoutParams = secondLayoutParams
                    stub.layoutResource = R.layout.partial_agent_fee_description
                    agent_desc_linear_layout.addView(stub)
                    stub.inflate()

                    setAgentSpinners()
                    miscViewModel.getAgentAdminFeeAllocation().observe(this, Observer {
                        allocates ->
                        if (!allocates!!.isEmpty()){
                            for (allocate in allocates){
                                adminAllocationArray.add(allocate)
                            }
                            adminAllocationAdapter.notifyDataSetChanged()
                        }
                    })
                }
                false -> {
                    //remove agent partial view
                    agent_desc_linear_layout.removeAllViews()

                    miscViewModel.getAdminFeeAllocation().observe(this, Observer {
                        allocates ->
                        if (!allocates!!.isEmpty()){
                            for (allocate in allocates){
                                adminAllocationArray.add(allocate)
                            }
                            adminAllocationAdapter.notifyDataSetChanged()
                        }
                    })
                }
            }
        }
    }

    private fun setAgentSpinners(){
        val agentAllocationArray = ArrayList<String>()
        val agentAllocationAdapter = ArrayAdapter(activity!!.baseContext, R.layout.simple_spinner_item, agentAllocationArray)
        agentAllocationAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_agent_allocation.adapter = agentAllocationAdapter

        spinner_agent_allocation.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedAgentAllocation = parent?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedAgentAllocation = null
            }
        }

        miscViewModel.getStandardFeeAllocation().observe(this, Observer {
            allocates -> if (!allocates!!.isEmpty()){
            for (allocate in allocates){
                agentAllocationArray.add(allocate)
            }
            agentAllocationAdapter.notifyDataSetChanged()
        }
        })

        val agentFeeTypeArray = ArrayList<String>()
        val agentFeeTypeAdapter = ArrayAdapter(activity!!.baseContext, R.layout.simple_spinner_item, agentFeeTypeArray)
        agentFeeTypeAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_agent_fee_type.adapter = agentFeeTypeAdapter

        spinner_agent_fee_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedAgentFeeType = parent?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedAgentFeeType = null
            }
        }

        miscViewModel.getFeeTypes().observe(this, Observer {
            feeTypes -> if (!feeTypes!!.isEmpty()){
            for (feeType in feeTypes){
                agentFeeTypeArray.add(feeType)
            }
            agentFeeTypeAdapter.notifyDataSetChanged()
        }
        })

        if (isBackPressed){
            val addedAlready = EditTradeActivity.editTradeRequest
            txt_agent_fee.text = Editable.Factory.getInstance().newEditable(addedAlready.unCalculatedAgentFee.toString())
            val position = agentAllocationAdapter.getPosition(addedAlready.agentFeeAllocation)
            spinner_agent_allocation.setSelection(position)
            val secondPosition = agentFeeTypeAdapter.getPosition(addedAlready.agentFeeType)
            spinner_agent_fee_type.setSelection(secondPosition)
        }else{
            val addedAlready = EditTradeActivity.selectedTrade!!
            txt_agent_fee.text = Editable.Factory.getInstance().newEditable(addedAlready.unCalculatedAgentFee.toString())
            val position = agentAllocationAdapter.getPosition(addedAlready.agentFeeAllocation?.standardPayeeAllocation)
            spinner_agent_allocation.setSelection(position)
            val secondPosition = agentFeeTypeAdapter.getPosition(addedAlready.agentFeeType?.feeType)
            spinner_agent_fee_type.setSelection(secondPosition)
        }
    }

    override fun onResume() {
        super.onResume()
        btn_back.setOnClickListener(this)
        btn_next.setOnClickListener(this)


//        checkbox_is_progress.isChecked = false
    }

    override fun onPause() {
        super.onPause()
        btn_back.setOnClickListener(null)
        btn_next.setOnClickListener(null)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun calculateEscrowFee(amount: Long):Long{

        var feeSchedule: FeeSchedule? = null
        if (schedule.isNotEmpty()){
            feeSchedule = schedule.filter {
                amount >= it.from && amount <= it.to
            }.single()
        }
        val percentage = feeSchedule!!.percentage
        //percentage of amount in pesewas
        var escrowFee = ((amount * percentage)/100)

        if (escrowFee < 3000.00){
            escrowFee = 3000.00
        }

        return escrowFee.toLong()
    }

    private fun calculateAdminFeesPlusPayments(amount:Long){
        val feeAllocation = EditTradeActivity.editTradeRequest.escrowAllocation
        val agentEmail = EditTradeActivity.editTradeRequest.agentEmail
        val escrowFee = calculateEscrowFee(amount)
        EditTradeActivity.editTradeRequest.escrowFee = escrowFee
        when (feeAllocation){
            "Seller Pays" -> {
                val sellerFee = amount-escrowFee
                EditTradeActivity.editTradeRequest.sellerFee = sellerFee
                EditTradeActivity.editTradeRequest.buyerFee = amount
            }
            "Buyer Pays" -> {
                val buyerFee = amount+escrowFee
                EditTradeActivity.editTradeRequest.buyerFee = buyerFee
                EditTradeActivity.editTradeRequest.sellerFee = amount
            }
            "Agent Pays" -> {
                val agentFee = calculateAgentFeeNoDivision(amount)-escrowFee
                EditTradeActivity.editTradeRequest.agentFee = agentFee
                EditTradeActivity.editTradeRequest.buyerFee = amount
                EditTradeActivity.editTradeRequest.sellerFee = amount
            }
            "50/50 Split [Buyer/Agent]" -> {
                val agentFee = calculateAgentFeeNoDivision(amount)-(escrowFee/2)
                EditTradeActivity.editTradeRequest.agentFee = agentFee
                val buyerFee = amount+(escrowFee/2)
                EditTradeActivity.editTradeRequest.buyerFee = buyerFee
                EditTradeActivity.editTradeRequest.sellerFee = amount
            }
            "50/50 Split [Buyer/Seller]" -> {
                val buyerFee = amount+(escrowFee/2)
                EditTradeActivity.editTradeRequest.buyerFee = buyerFee
                val sellerFee = amount-(escrowFee/2)
                EditTradeActivity.editTradeRequest.sellerFee = sellerFee
            }
            "50/50 Split [Seller/Agent]" -> {
                val agentFee = calculateAgentFeeNoDivision(amount)-(escrowFee/2)
                EditTradeActivity.editTradeRequest.agentFee = agentFee
                val sellerFee = amount-(escrowFee/2)
                EditTradeActivity.editTradeRequest.sellerFee = sellerFee
                EditTradeActivity.editTradeRequest.buyerFee = amount
            }
            "3-Way Split [Buyer/Seller/Agent]" -> {
                val buyerFee = amount+(escrowFee/3)
                EditTradeActivity.editTradeRequest.buyerFee = buyerFee
                val sellerFee = amount-(escrowFee/3)
                EditTradeActivity.editTradeRequest.sellerFee = sellerFee
                val agentFee = calculateAgentFeeNoDivision(amount)-(escrowFee/3)
                EditTradeActivity.editTradeRequest.agentFee = agentFee
            }
        }

        //calculate agent deductions and additions after the base values have been set
        if (agentEmail != null){
            setAgentFeeAllocation(amount)
        }
    }

    private fun setAgentFeeAllocation(amount: Long){
        val agentFeeAllocation = EditTradeActivity.editTradeRequest.agentFeeAllocation
        //if agent fee hasn't been set above then set it
        if (EditTradeActivity.editTradeRequest.agentFee == 0L){
            EditTradeActivity.editTradeRequest.agentFee = calculateAgentFeeNoDivision(amount)
        }
        when (agentFeeAllocation){
            "Seller Pays" -> {
                EditTradeActivity.editTradeRequest.sellerFee = EditTradeActivity.editTradeRequest.sellerFee-calculateAgentFeeNoDivision(amount)
            }
            "Buyer Pays" -> {
                EditTradeActivity.editTradeRequest.buyerFee = EditTradeActivity.editTradeRequest.buyerFee+calculateAgentFeeNoDivision(amount)
            }
            "50/50 Split [Buyer/Seller]" -> {
                EditTradeActivity.editTradeRequest.buyerFee = EditTradeActivity.editTradeRequest.buyerFee+calculateAgentFee5050Division(amount)
                EditTradeActivity.editTradeRequest.sellerFee = EditTradeActivity.editTradeRequest.sellerFee-calculateAgentFee5050Division(amount)
            }
        }
    }

    private fun calculateAgentFee5050Division(tradeAmount: Long) : Long{
        val agentFeeType = EditTradeActivity.editTradeRequest.agentFeeType
        val agentFeeUnedited = EditTradeActivity.editTradeRequest.unCalculatedAgentFee
        var agentFeeEdited:Long = 0
        when (agentFeeType){
            "Fixed Fee" -> {
                agentFeeEdited = agentFeeUnedited/2
            }
            "Percentage (%)" -> {
                //get cedi amounts from pesewas
                //and actual number from the 100 multiplication
                val agentFee = agentFeeUnedited.toDouble()/100
                val amount = tradeAmount.toDouble()/100
                //get percentage of amount
                val percentageOfAmount = (amount*agentFee)/100
                //divide amount into 2 for 50/50 division
                val shareAmount = percentageOfAmount/2
                //multiply by hundred again to get pesewas
                agentFeeEdited = (shareAmount*100).toLong()
            }
        }
        return agentFeeEdited
    }

    private fun calculateAgentFeeNoDivision(tradeAmount: Long) : Long{
        val agentFeeType = EditTradeActivity.editTradeRequest.agentFeeType
        val agentFeeUnedited = EditTradeActivity.editTradeRequest.unCalculatedAgentFee
        var agentFeeEdited:Long = 0
        when (agentFeeType){
            "Fixed Fee" -> {
                agentFeeEdited = agentFeeUnedited
            }
            "Percentage (%)" -> {
                //get cedi amounts from pesewas
                //and actual number from the 100 multiplication
                val agentFee = agentFeeUnedited.toDouble()/100
                val amount = tradeAmount.toDouble()/100
                //get percentage of amount
                val percentageOfAmount = (amount*agentFee)/100
                //multiply by hundred again to get pesewas
                agentFeeEdited = (percentageOfAmount*100).toLong()
            }
        }
        return agentFeeEdited
    }

    private fun validateTheEditTexts():Boolean{
        var validate = false
        val emptyText = Validator.traverseEditTexts(description_linear_layout)
        //if null then it didn't find any edit text without text
        if (emptyText == null){
            when (isAgentAdded){
                true -> {
                    if (selectedIndustryClassification != null &&
                            selectedAdminAllocation != null && selectedAgentAllocation != null &&
                            selectedAgentFeeType != null){
                        validate = true
                    }
                }
                false -> {
                    if (selectedIndustryClassification != null &&
                            selectedAdminAllocation != null){
                        validate = true
                    }
                }
            }
        }
        return validate
    }

    private fun passEntriesToVariable(){
        EditTradeActivity.editTradeRequest.currencyUsed = "GHS"
        //set has progress to value
        EditTradeActivity.editTradeRequest.isProgressPayment = isProgressPayment
        when (isAgentAdded){
            true -> {
                when (isProgressPayment){
                    true -> {
                        EditTradeActivity.editTradeRequest.tradeName = trade_name_txt.text.toString()
                        EditTradeActivity.editTradeRequest.description = trade_description_txt.text.toString()
                        //depending on the fee type, the agent fee is calculated
                        EditTradeActivity.editTradeRequest.unCalculatedAgentFee = ((txt_agent_fee.text.toString().toDouble() * 100).toLong())
                        EditTradeActivity.editTradeRequest.tradeCategory = selectedIndustryClassification
                        EditTradeActivity.editTradeRequest.escrowAllocation = selectedAdminAllocation
                        EditTradeActivity.editTradeRequest.agentFeeAllocation = selectedAgentAllocation
                        EditTradeActivity.editTradeRequest.agentFeeType = selectedAgentFeeType

                        //set has progress to true
//                        EditTradeActivity.editTradeRequest.isProgressPayment = true
                    }
                    false -> {
                        EditTradeActivity.editTradeRequest.tradeName = trade_name_txt.text.toString()
                        EditTradeActivity.editTradeRequest.description = trade_description_txt.text.toString()
                        //multiplying by hundred because the values in the database are in pesewas
                        EditTradeActivity.editTradeRequest.tradeAmount = ((trade_amount_txt.text.toString().toDouble() * 100).toLong())
                        EditTradeActivity.editTradeRequest.deliveryTime = delivery_time_txt.text.toString()
                        EditTradeActivity.editTradeRequest.inspectionPeriod = inspection_time_txt.text.toString()
                        //depending on the fee type, the agent fee is calculated
                        EditTradeActivity.editTradeRequest.unCalculatedAgentFee = ((txt_agent_fee.text.toString().toDouble() * 100).toLong())
                        EditTradeActivity.editTradeRequest.tradeCategory = selectedIndustryClassification
                        EditTradeActivity.editTradeRequest.escrowAllocation = selectedAdminAllocation
                        EditTradeActivity.editTradeRequest.agentFeeAllocation = selectedAgentAllocation
                        EditTradeActivity.editTradeRequest.agentFeeType = selectedAgentFeeType

                        //calculate individual fees
                        calculateAdminFeesPlusPayments((trade_amount_txt.text.toString().toDouble() * 100).toLong())
                    }
                }
            }
            false -> {
                when (isProgressPayment){
                    true -> {
                        EditTradeActivity.editTradeRequest.tradeName = trade_name_txt.text.toString()
                        EditTradeActivity.editTradeRequest.description = trade_description_txt.text.toString()
                        EditTradeActivity.editTradeRequest.tradeCategory = selectedIndustryClassification
                        EditTradeActivity.editTradeRequest.escrowAllocation = selectedAdminAllocation

                        //set has progress to true
//                        EditTradeActivity.editTradeRequest.isProgressPayment = true
                    }
                    false -> {
                        EditTradeActivity.editTradeRequest.tradeName = trade_name_txt.text.toString()
                        EditTradeActivity.editTradeRequest.description = trade_description_txt.text.toString()
                        //multiplying by hundred because the values in the database are in pesewas
                        EditTradeActivity.editTradeRequest.tradeAmount = ((trade_amount_txt.text.toString().toDouble() * 100).toLong())
                        EditTradeActivity.editTradeRequest.deliveryTime = delivery_time_txt.text.toString()
                        EditTradeActivity.editTradeRequest.inspectionPeriod = inspection_time_txt.text.toString()
                        EditTradeActivity.editTradeRequest.tradeCategory = selectedIndustryClassification
                        EditTradeActivity.editTradeRequest.escrowAllocation = selectedAdminAllocation

                        //calculate individual fees
                        calculateAdminFeesPlusPayments((trade_amount_txt.text.toString().toDouble() * 100).toLong())
                    }
                }
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id){
            R.id.btn_back -> {
                if (listener != null){
                    adminAllocationArray.clear()
                    listener?.onBackButtonPressed(this)
                }
            }
            R.id.btn_next -> {
                if (validateTheEditTexts()){
                    val prefs = PreferenceHelper.defaultPrefs(activity!!.baseContext)
                    prefs[PREF_IS_PROCESS_PAYMENT] = isProgressPayment
                    passEntriesToVariable()
                    listener?.onNextPressed(this)
                }else{
                    Toast.makeText(activity, R.string.login_empty_text, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onNextPressed(fragment: Fragment) {
        TODO("not necessary")
    }

    override fun onBackButtonPressed(fragment: Fragment) {
        when (fragment){
            is EditTradeBeneficiariesFragment -> {
                isBackPressed = true
                Log.i("bene go bak", isBackPressed.toString())
            }
            is EditTradeProgressFragment -> {
                isBackPressed = true
                Log.i("pregress go bak", isBackPressed.toString())
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        fun onBackButtonPressed(fragment: Fragment)
        fun onNextPressed(fragment: Fragment)
    }

}
