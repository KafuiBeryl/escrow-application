package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "standard_fee_allocation")
data class StandardFeeAllocation (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "standardAllocationId")
        @SerializedName("id")
        var standardAllocationId:Int,

        @ColumnInfo(name = "standard_payee_allocation")
        @SerializedName("payee_allocation")
        var standardPayeeAllocation:String
)