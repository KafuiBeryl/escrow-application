package com.example.nit_appdev_002.escrowapplication.utilities

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    fun fromMillisToTimeString(millis: Long) : String {
        val format = SimpleDateFormat("EEE, d MMM yyyy hh:mm a", Locale.getDefault())
        return format.format(millis)
    }
}