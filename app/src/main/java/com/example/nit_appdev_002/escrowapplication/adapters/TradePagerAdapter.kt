package com.example.nit_appdev_002.escrowapplication.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.example.nit_appdev_002.escrowapplication.fragments.*

//the @JvmOverloads is so that default value parameters in the constructor can be implemented
class TradePagerAdapter constructor(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager),
        TradeRoleFragment.OnTradeRoleListener, TradeDescriptionFragment.OnTradeDescriptionListener, TradeBeneficiaryFragment.OnTradeBeneficiaryListener, TradeProgressFragment.OnTradeProgressListener {

//    private var mFragmentManager:FragmentManager
//    private var mContext : Context?

    private var mTradePartyFragment : TradePartyFragment
    private var mTradeDescriptionFragment : TradeDescriptionFragment

//    private var fragmentBundle:Bundle?

    init {
//        mFragmentManager = fragmentManager
//        fragmentBundle = bundle
//        mContext = context
        mTradePartyFragment = TradePartyFragment()
        mTradeDescriptionFragment = TradeDescriptionFragment()
    }

    override fun getItem(position: Int) : Fragment {
        var fragment: Fragment? = null
        when(position){
            0 -> fragment = TradeRoleFragment()
            1 -> fragment = mTradePartyFragment
            2 -> fragment = mTradeDescriptionFragment
            3 -> fragment = TradeProgressFragment()
            4 -> fragment = TradeBeneficiaryFragment()
            5 -> fragment = TradeNotificationFragment()
            6 -> fragment = TradeDeliveryFragment()

        }

        return fragment!!
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getCount() : Int {
//        val prefs = PreferenceHelper.defaultPrefs(mContext!!)
//        val isProcess:Boolean? = prefs[PREF_IS_PROCESS_PAYMENT]
//        var count = 0
//        when (isProcess){
//            true -> count = 7
//            false -> count = 7
//        }
        return 7
    }

    override fun onNextPressed(fragment: Fragment) {
        TODO("not necessary")
    }

    override fun onBackButtonPressed(fragment: Fragment) {
        when (fragment){
            is TradeDescriptionFragment -> {
                mTradePartyFragment.onBackButtonPressed(TradeDescriptionFragment())
            }
            is TradeBeneficiaryFragment -> {
                mTradeDescriptionFragment.onBackButtonPressed(TradeBeneficiaryFragment())
            }
            is TradeProgressFragment -> {
                mTradeDescriptionFragment.onBackButtonPressed(TradeProgressFragment())
            }
        }
    }
}