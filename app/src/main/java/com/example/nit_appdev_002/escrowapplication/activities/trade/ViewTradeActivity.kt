package com.example.nit_appdev_002.escrowapplication.activities.trade

import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.activities.authentication.LoginActivity
import com.example.nit_appdev_002.escrowapplication.activities.trade.view.TradeDetailsActivity
import com.example.nit_appdev_002.escrowapplication.adapters.ViewTradeRecyclerAdapter
import com.example.nit_appdev_002.escrowapplication.apicalls.OtherNetworkCalls
import com.example.nit_appdev_002.escrowapplication.apicalls.RetrofitInstance
import com.example.nit_appdev_002.escrowapplication.apiobjects.GetInvolvedTradesResult
import com.example.nit_appdev_002.escrowapplication.utilities.*
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.viewmodels.TransactionViewModel
import com.example.nit_appdev_002.escrowapplication.viewmodels.UserViewModel
import com.github.omadahealth.lollipin.lib.PinCompatActivity
import com.google.android.material.navigation.NavigationView
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_view_trade.*
import kotlinx.android.synthetic.main.content_view_trade.*
import kotlinx.android.synthetic.main.nav_header_home.*
import kotlinx.android.synthetic.main.toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ViewTradeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    companion object {
        @JvmStatic
        private lateinit var mUserViewModel: UserViewModel

        @JvmStatic
        private lateinit var mTransactionViewModel: TransactionViewModel
    }
    private var id:Int? = null
    private lateinit var viewTradeAdapter: ViewTradeRecyclerAdapter
    private var token:String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_trade)
        setSupportActionBar(custom_toolbar)

        val toggle = ActionBarDrawerToggle(
                this, view_trade_drawer_layout, custom_toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        view_trade_drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        //initialize the view model
        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        mTransactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel::class.java)
        setCountText()
        getAndSetUserTrades()
        val prefs = PreferenceHelper.defaultPrefs(this)
        id = prefs[PREF_USER_ID]
        token = prefs[PREF_TOKEN]

        getUserInvolvedTrades()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        setDrawerValues()
    }

    private fun setDrawerValues(){
        mUserViewModel.getUser(id!!).observe(this, Observer {
            user->
            if (user != null){
                val string = "${user.firstName} ${user.lastName}"
                if (user.imagePath != null){
                    Glide.with(this)
                            .load(Base64.decode(user.imagePath, Base64.URL_SAFE))
                            .apply(RequestOptions()
                                    .fitCenter()
                                    .placeholder(R.drawable.profile_placeholder))
                            .into(drawer_imageView)
                }
                drawer_main_textView.text = string
                drawer_sub_textView.text = user.email
            }
        })
    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (resultCode == PIN_FAILED || resultCode == PIN_FORGOT){
//            mUserViewModel.logoutThings()
//            startActivity(Intent(this, LoginActivity::class.java))
//            finish()
//        }
//    }

    private fun getAndSetUserTrades(){
        //assign adapter and layout manager to the recycler_view
        viewTradeAdapter = ViewTradeRecyclerAdapter(this)
        recycle_view.adapter = viewTradeAdapter
        recycle_view.layoutManager = LinearLayoutManager(this)
        mUserViewModel.getAllUserTradeRoles().observe(this, Observer {
            if (it != null){
                viewTradeAdapter.setRoles(it)
            }
        })

        viewTradeAdapter.onItemClick = {position ->
            val item = viewTradeAdapter.getItem(position)
            if (item != null){
                val tradeId = item.tradeId
                val intent = Intent(this, TradeDetailsActivity::class.java)
                intent.putExtra(INTENT_SELECTED_TRADE_ID, tradeId)
                startActivity(intent)
            }
        }

    }

    private fun setCountText(){
        mUserViewModel.countUserTradeRoles().observe(this, Observer {
            if (it !=null){
                if (it == 0){
                    trade_count_text.text = resources.getString(R.string.no_trades)
                }else{
                    val intro = " $it trade(s) so far"
                    trade_count_text.text = intro
                }
            }
        })
    }

    private fun getUserInvolvedTrades(){
        if(token != null){
            val service: OtherNetworkCalls = RetrofitInstance.getRetrofitInstance()!!.create(OtherNetworkCalls::class.java)
            val tokenString = "Bearer $token"
            val call = service.getUserInvolvedTrades(tokenString)
            call.enqueue(object : Callback<GetInvolvedTradesResult>{
                override fun onResponse(call: Call<GetInvolvedTradesResult>, response: Response<GetInvolvedTradesResult>) {
                    if (response.body() != null){
                        val result = response.body() as GetInvolvedTradesResult
                        if (result.userTradeRoles != null){
                            viewTradeAdapter.setRoles(result.userTradeRoles!!)
                            mUserViewModel.insertUserTradeRoles(result.userTradeRoles)
                        }
                        mTransactionViewModel.insertTrades(result.userInvitedTrades)
                    }else if (response.errorBody() != null){
                        val gson = GsonBuilder().create()
                        val errorResult:GetInvolvedTradesResult = gson.fromJson<GetInvolvedTradesResult>(response.errorBody()?.string(), GetInvolvedTradesResult::class.java)
                        Log.e("ViewTradesRequest", errorResult.error)
                        unauthorisedToast(errorResult.error)
                    }
                }

                override fun onFailure(call: Call<GetInvolvedTradesResult>, t: Throwable) {
                    Log.e("HomeRequestError", t.message.toString())
                    unauthorisedToast(t.message.toString())
                }
            })
        }
    }

    private fun unauthorisedToast(error:String?){
        Toast.makeText(this, "$error ...", Toast.LENGTH_SHORT).show()
    }

    override fun onBackPressed() {
        if (view_trade_drawer_layout.isDrawerOpen(GravityCompat.START)) {
            view_trade_drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                // Handle the camera action
            }
            R.id.nav_new_trade -> {

            }
            R.id.nav_view_trade -> {

            }
            R.id.nav_profile -> {

            }
            R.id.nav_personal -> {

            }
            R.id.nav_billing -> {

            }
        }

        view_trade_drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
