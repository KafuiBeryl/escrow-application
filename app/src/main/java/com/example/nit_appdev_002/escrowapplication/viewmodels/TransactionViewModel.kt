package com.example.nit_appdev_002.escrowapplication.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.nit_appdev_002.escrowapplication.entities.*
import com.example.nit_appdev_002.escrowapplication.repositories.TransactionRepository

class TransactionViewModel(application: Application) : AndroidViewModel(application) {


    private var mTransactionRepository:TransactionRepository

    init {
        mTransactionRepository = TransactionRepository(application)
    }

//    fun logoutThings(){
//        mTransactionRepository.logoutThings()
//    }

    fun nukeExtras(){
        mTransactionRepository.nukeExtras()
    }

    fun getSingleTrade(id:Int): LiveData<Trade> {
        return mTransactionRepository.getSingleTrade(id)
    }

    fun getTrades():LiveData<List<Trade>>{
        return mTransactionRepository.getTrades()
    }

    fun getTradeMilestones(tradeId:Int):LiveData<List<TradeMileStones>>{
        return mTransactionRepository.getTradeMilestones(tradeId)
    }

    fun getTradeNotificationGang(tradeId:Int):LiveData<List<NotificationGang>>{
        return mTransactionRepository.getTradeNotificationGang(tradeId)
    }

    fun getTradBeneficiaries(tradeId:Int):LiveData<List<Beneficiary>>{
        return mTransactionRepository.getTradBeneficiaries(tradeId)
    }

    fun getTradeDeliveryAddress(tradeId:Int):LiveData<DeliveryAddress>{
        return mTransactionRepository.getTradeDeliveryAddress(tradeId)
    }

    fun updateTrade(trade: Trade?){
        if (trade != null){
            mTransactionRepository.updateTrade(trade)
        }
    }

    fun insertTrades(trades : List<Trade>?){
        if (trades != null){
            mTransactionRepository.insertTrade(trades)
        }
    }

    fun insertTradesMilestones(mileStones: List<TradeMileStones>?){
        if (mileStones != null){
            mTransactionRepository.insertTradeMileStones(mileStones)
        }
    }

    fun updateTradeMilestones(mileStones: TradeMileStones?){
        if (mileStones != null){
            mTransactionRepository.updateTradeMileStone(mileStones)
        }
    }

    fun insertTradeNotifications(notificationGang: List<NotificationGang>?){
        if (notificationGang != null){
            mTransactionRepository.insertTradeNotifications(notificationGang)
        }
    }

    fun updateTradeNotification(notificationGang: NotificationGang?){
        if (notificationGang != null){
            mTransactionRepository.updateTradeNotification(notificationGang)
        }
    }

    fun insertTradeBeneficiaries(beneficiaries: List<Beneficiary>?){
        if (beneficiaries != null){
            mTransactionRepository.insertTradeBeneficiaries(beneficiaries)
        }
    }

    fun updateTradeBeneficiaries(beneficiary: Beneficiary?){
        if (beneficiary != null){
            mTransactionRepository.updateTradeBeneficiaries(beneficiary)
        }
    }

    fun insertTradeDeliveryAddress(deliveryAddress: DeliveryAddress?){
        if (deliveryAddress != null){
            mTransactionRepository.insertTradeDeliveryAddress(deliveryAddress)
        }
    }

    fun updateTradeDeliveryAddress(deliveryAddress: DeliveryAddress?){
        if (deliveryAddress != null){
            mTransactionRepository.updateTradeDeliveryAddress(deliveryAddress)
        }
    }
}