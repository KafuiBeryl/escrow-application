package com.example.nit_appdev_002.escrowapplication.utilities

import android.content.Context
import com.example.nit_appdev_002.escrowapplication.entities.Message
import com.example.nit_appdev_002.escrowapplication.entities.User
import com.example.nit_appdev_002.escrowapplication.recyclerview.item.OtherChatItem
import com.example.nit_appdev_002.escrowapplication.recyclerview.item.OwnChatItem
import com.xwray.groupie.kotlinandroidextensions.Item

object PusherUtil {
    fun addChatMessagesListener(context: Context, onListen:(List<Item>) -> Unit,
                                messages: List<Message>, user: User){
        val items = mutableListOf<Item>()
        for (message in messages){
            if (message.user.contentEquals("${user.firstName} ${user.lastName}")){
                items.add(OwnChatItem(message, context))
            }else{
                items.add(OtherChatItem(message, context))
            }
        }
        onListen(items)
    }
}