package com.example.nit_appdev_002.escrowapplication.activities.trade.create

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.viewmodels.MiscViewModel
import kotlinx.android.synthetic.main.activity_trade_beneficiaries.*
import kotlinx.android.synthetic.main.toolbar.*

class TradeBeneficiariesActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    companion object {
        @JvmStatic
        private lateinit var miscViewModel: MiscViewModel

        @JvmStatic
        private var benefitNumber: Int = 0

        @JvmStatic
        private var benefitEmailNumber: Int = 100

        @JvmStatic
        private var benefitConfirmEmailNumber: Int = 200

        @JvmStatic
        private var benefitAmountNumber: Int = 300

        @JvmStatic
        private var benefitTypeNumber: Int = 400

        @JvmStatic
        private var benefitFeeTypeNumber: Int = 500

        @JvmStatic
        private var benefitPayeeNumber: Int = 600

        @JvmStatic
        private var selectedFirstBeneficiaryType: String? = null

        @JvmStatic
        private var selectedFirstBeneficiaryFeeAllocation: String? = null

        @JvmStatic
        private var selectedFirstBeneficiaryFeeType: String? = null

        @JvmStatic
        private var selectedSecondBeneficiaryType: String? = null

        @JvmStatic
        private var selectedSecondBeneficiaryFeeAllocation: String? = null

        @JvmStatic
        private var selectedSecondBeneficiaryFeeType: String? = null

        @JvmStatic
        private var selectedThirdBeneficiaryType: String? = null

        @JvmStatic
        private var selectedThirdBeneficiaryFeeAllocation: String? = null

        @JvmStatic
        private var selectedThirdBeneficiaryFeeType: String? = null

        @JvmStatic
        private var selectedFourthBeneficiaryType: String? = null

        @JvmStatic
        private var selectedFourthBeneficiaryFeeAllocation: String? = null

        @JvmStatic
        private var selectedFourthBeneficiaryFeeType: String? = null

        @JvmStatic
        private var selectedFifthBeneficiaryType: String? = null

        @JvmStatic
        private var selectedFifthBeneficiaryFeeAllocation: String? = null

        @JvmStatic
        private var selectedFifthBeneficiaryFeeType: String? = null

        @JvmStatic
        private val beneficiaryTypeArray = ArrayList<String>()

        @JvmStatic
        private val beneficiaryFeeTypeArray = ArrayList<String>()

        @JvmStatic
        private val beneficiaryFeeAllocationArray = ArrayList<String>()


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trade_beneficiaries)
        setSupportActionBar(custom_toolbar)

        //instantiate ViewModel object
        miscViewModel = ViewModelProviders.of(this).get(MiscViewModel::class.java)

        setSpinnerData()

        //set click listener to button
        btn_add_beneficiary.setOnClickListener {
            addLinearLayout()
        }

        btn_back.setOnClickListener {
            finish()
        }

        btn_next.setOnClickListener {
            startActivity(Intent(this, TradeProgressActivity::class.java))
        }
    }

    private fun setSpinnerData(){
        miscViewModel.getFeeTypes().observe(this, Observer {
            feeTypes ->
            if (!feeTypes!!.isEmpty()){
                for (feeType in feeTypes){
                    beneficiaryFeeTypeArray.add(feeType)
                }
            }
        })

        miscViewModel.getBeneficiaryTypes().observe(this, Observer {
            types ->
            if (!types!!.isEmpty()){
                for (type in types){
                    beneficiaryTypeArray.add(type)
                }
            }
        })

        miscViewModel.getStandardFeeAllocation().observe(this, Observer {
            types ->
            if (!types!!.isEmpty()){
                for (type in types){
                    beneficiaryFeeAllocationArray.add(type)
                }
            }
        })
    }

    private fun addLinearLayout(){
        if (benefitNumber < 5){
            //basically a = a+b
            benefitNumber =+ 1

            //craft the UI in code. This should be good
            val bigLinearLayout = LinearLayout(this)
            val linearLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
            bigLinearLayout.layoutParams = linearLayoutParams
            bigLinearLayout.orientation = LinearLayout.VERTICAL

            val emailTextView = createBigTextView()
            emailTextView.text = resources.getString(R.string.register_email)
            emailTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val emailEditText = createBigEditText()
            emailEditText.id = benefitEmailNumber + benefitNumber

            val confirmEmailTextView = createBigTextView()
            confirmEmailTextView.text = resources.getString(R.string.trade_confirm_email)
            confirmEmailTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val confirmEmailEditText = createBigEditText()
            confirmEmailEditText.id = benefitConfirmEmailNumber + benefitNumber

            val firstSmallLinearLayout = LinearLayout(this)
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            firstSmallLinearLayout.layoutParams = layoutParams
            firstSmallLinearLayout.orientation = LinearLayout.HORIZONTAL

            val benefitTypeTextView = createSmallTextView()
            benefitTypeTextView.text = resources.getString(R.string.trade_beneficiary_type)
            benefitTypeTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val benefitPayeeTextView = createSmallTextView()
            benefitPayeeTextView.text = resources.getString(R.string.trade_beneficiary_fee_allocation)
            benefitPayeeTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            firstSmallLinearLayout.addView(benefitTypeTextView, 0)
            firstSmallLinearLayout.addView(benefitPayeeTextView, 1)

            val firstSmallEditLinearLayout = LinearLayout(this)
            val secondLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            secondLayoutParams.gravity = Gravity.CENTER
            firstSmallEditLinearLayout.layoutParams = secondLayoutParams
            firstSmallEditLinearLayout.orientation = LinearLayout.HORIZONTAL

            val benefitTypeSpinner = createSmallSpinner()
            benefitTypeSpinner.id = benefitTypeNumber + benefitNumber
//            benefitTypeSpinner.drawingCacheBackgroundColor = ResourcesCompat.getColor(resources, R.color.colorPrimary, null)
            //set spinner adapter
            val benefitTypeAdapter = ArrayAdapter(this, R.layout.simple_spinner_item, beneficiaryTypeArray)
            benefitTypeAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
            benefitTypeSpinner.adapter = benefitTypeAdapter
            //set on item selected listener to the fragment
            // since the listener has been attached to the fragment
            benefitTypeSpinner.onItemSelectedListener = this

            val benefitPayeeSpinner = createSmallSpinner()
            benefitPayeeSpinner.id = benefitPayeeNumber + benefitNumber
            //set spinner adapter
            val benefitPayeeAdapter = ArrayAdapter(this, R.layout.simple_spinner_item, beneficiaryFeeAllocationArray)
            benefitPayeeAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
            benefitPayeeSpinner.adapter = benefitPayeeAdapter
            //set on item selected listener to the fragment
            // since the listener has been attached to the fragment
            benefitPayeeSpinner.onItemSelectedListener = this

            firstSmallEditLinearLayout.addView(benefitTypeSpinner, 0)
            firstSmallEditLinearLayout.addView(benefitPayeeSpinner, 1)

            val secondSmallLinearLayout = LinearLayout(this)
            secondSmallLinearLayout.layoutParams = layoutParams
            secondSmallLinearLayout.orientation = LinearLayout.HORIZONTAL

            val benefitAmountTextView = createSmallTextView()
            benefitAmountTextView.text = resources.getString(R.string.trade_beneficiary_amount)
            benefitAmountTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            val benefitFeeTypeTextView = createSmallTextView()
            benefitFeeTypeTextView.text = resources.getString(R.string.trade_fee_type)
            benefitFeeTypeTextView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

            secondSmallLinearLayout.addView(benefitAmountTextView, 0)
            secondSmallLinearLayout.addView(benefitFeeTypeTextView, 1)

            val secondSmallEditLinearLayout = LinearLayout(this)
            secondSmallEditLinearLayout.layoutParams = secondLayoutParams
            secondSmallEditLinearLayout.orientation = LinearLayout.HORIZONTAL

            val benefitAmountEditText = createSmallEditText()
            benefitAmountEditText.id = benefitAmountNumber + benefitNumber

            val benefitFeeTypeSpinner = createSmallSpinner()
            benefitFeeTypeSpinner.id = benefitFeeTypeNumber + benefitNumber
            //set spinner adapter
            val benefitFeeTypeAdapter = ArrayAdapter(this, R.layout.simple_spinner_item, beneficiaryFeeTypeArray)
            benefitFeeTypeAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
            benefitFeeTypeSpinner.adapter = benefitFeeTypeAdapter
            //set on item selected listener to the fragment
            // since the listener has been attached to the fragment
            benefitFeeTypeSpinner.onItemSelectedListener = this

            secondSmallEditLinearLayout.addView(benefitAmountEditText, 0)
            secondSmallEditLinearLayout.addView(benefitFeeTypeSpinner, 1)

            bigLinearLayout.addView(emailTextView)
            bigLinearLayout.addView(emailEditText)
            bigLinearLayout.addView(confirmEmailTextView)
            bigLinearLayout.addView(confirmEmailEditText)
            bigLinearLayout.addView(firstSmallLinearLayout)
            bigLinearLayout.addView(firstSmallEditLinearLayout)
            bigLinearLayout.addView(secondSmallLinearLayout)
            bigLinearLayout.addView(secondSmallEditLinearLayout)

            val removeButton = createRemoveBeneficiaryButton()

            bigLinearLayout.addView(removeButton)

            beneficiary_linear_layout.addView(bigLinearLayout, 0)

//            val stub = ViewStub(activity)
//            stub.layoutParams = linearLayoutParams
//            stub.layoutResource = R.layout.partial_first_beneficiary
//            beneficiary_linear_layout.addView(stub)
//            stub.inflate()

        }else{
            btn_add_beneficiary.visibility = View.GONE
            Toast.makeText(this, "thats too many", Toast.LENGTH_LONG).show()
        }

    }

    private fun createRemoveBeneficiaryButton(): Button {
        val button = Button(this, null, android.R.attr.borderlessButtonStyle)
        button.setTextColor(ResourcesCompat.getColor(resources, android.R.color.holo_red_dark, null))
        button.textSize = 15f
        val textViewTypeface = ResourcesCompat.getFont(this, R.font.montserrat_semibold)
        button.typeface = textViewTypeface
        button.setAllCaps(false)
        button.gravity = Gravity.END
        val buttonLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        buttonLayoutParams.gravity = Gravity.END
//        buttonLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.terms_small_margin)
        button.layoutParams = buttonLayoutParams
        button.text = resources.getString(R.string.trade_beneficiary_remove_btn_text)
        button.setOnClickListener {
            view ->
            // same as subtracting 1
            benefitNumber =- 1
            beneficiary_linear_layout.removeView(view.parent as View)
        }
        return button
    }

    private fun createBigTextView(): TextView {
        //craft email textview
        val textView = TextView(this)
        textView.setPadding(10, 0, 10, 0)
//        textView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        if (Build.VERSION.SDK_INT < 23){
            textView.setTextAppearance(this, android.R.style.TextAppearance_Small)
        }else{
            textView.setTextAppearance(android.R.style.TextAppearance_Small)
        }
        val textViewTypeface = ResourcesCompat.getFont(this, R.font.montserrat_semibold)
        textView.typeface = textViewTypeface
        val textViewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        textViewLayoutParams.bottomMargin = 5
        textView.layoutParams = textViewLayoutParams
        textView.setAllCaps(true)
        return textView
    }

    private fun createSmallTextView(): TextView {
        //craft email text view
        val textView = TextView(this)
        textView.setPadding(10, 0, 10, 0)
//        textView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        if (Build.VERSION.SDK_INT < 23){
            textView.setTextAppearance(this, android.R.style.TextAppearance_Small)
        }else{
            textView.setTextAppearance(android.R.style.TextAppearance_Small)
        }
        val textViewTypeface = ResourcesCompat.getFont(this, R.font.montserrat_semibold)
        textView.typeface = textViewTypeface
        val textViewLayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        textViewLayoutParams.weight = 1f
        textViewLayoutParams.bottomMargin = 5
        textView.layoutParams = textViewLayoutParams
        textView.setAllCaps(true)
        return textView
    }

    private fun createBigEditText(): EditText {
        //craft edit Text
        val editText = EditText(this)
        editText.setPadding(20, 5, 20, 5)
        editText.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        editText.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        editText.maxLines = 1
        val viewLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        viewLayoutParams.gravity = Gravity.CENTER
        viewLayoutParams.bottomMargin = resources.getDimensionPixelSize(R.dimen.terms_margin_side)
        editText.layoutParams = viewLayoutParams
//        val viewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_regular)
//        editText.typeface = viewTypeface
        editText.background = ResourcesCompat.getDrawable(resources, R.drawable.profile_edit_shape, null)
        return editText
    }

    private fun createSmallEditText(): EditText {
        //craft edit Text
        val editText = EditText(this)
        editText.setPadding(20, 5, 20, 5)
        editText.setTextColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))
        editText.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        editText.maxLines = 1
        val viewLayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        viewLayoutParams.weight = 1f
        viewLayoutParams.gravity = Gravity.CENTER
        viewLayoutParams.marginEnd = resources.getDimensionPixelSize(R.dimen.terms_margin)
        viewLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.edit_text_margin_top)
        viewLayoutParams.bottomMargin = resources.getDimensionPixelSize(R.dimen.terms_margin)
        editText.layoutParams = viewLayoutParams
//        val viewTypeface = ResourcesCompat.getFont(activity!!.applicationContext, R.font.montserrat_regular)
//        editText.typeface = viewTypeface
        editText.background = ResourcesCompat.getDrawable(resources, R.drawable.profile_edit_shape, null)
        return editText
    }

    private fun createSmallSpinner(): Spinner {
        val spinner = Spinner(this)
        spinner.setPadding(5, 5, 5, 5)
        val viewLayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        viewLayoutParams.weight = 1f
        viewLayoutParams.gravity = Gravity.CENTER
        viewLayoutParams.marginEnd = 15
        viewLayoutParams.marginStart = 15
        viewLayoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.edit_text_margin_top)
        viewLayoutParams.bottomMargin = resources.getDimensionPixelSize(R.dimen.terms_margin)
        spinner.layoutParams = viewLayoutParams
        return spinner
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
        when (id){
            benefitTypeNumber.toLong()+1 -> selectedFirstBeneficiaryType = parent.getItemAtPosition(position).toString()
            benefitTypeNumber.toLong()+2 -> selectedSecondBeneficiaryType = parent.getItemAtPosition(position).toString()
            benefitTypeNumber.toLong()+3 -> selectedThirdBeneficiaryType = parent.getItemAtPosition(position).toString()
            benefitTypeNumber.toLong()+4 -> selectedFourthBeneficiaryType = parent.getItemAtPosition(position).toString()
            benefitTypeNumber.toLong()+5 -> selectedFifthBeneficiaryType = parent.getItemAtPosition(position).toString()

            benefitFeeTypeNumber.toLong()+1 -> selectedFirstBeneficiaryFeeType = parent.getItemAtPosition(position).toString()
            benefitFeeTypeNumber.toLong()+2 -> selectedSecondBeneficiaryFeeType = parent.getItemAtPosition(position).toString()
            benefitFeeTypeNumber.toLong()+3 -> selectedThirdBeneficiaryFeeType = parent.getItemAtPosition(position).toString()
            benefitFeeTypeNumber.toLong()+4 -> selectedFourthBeneficiaryFeeType = parent.getItemAtPosition(position).toString()
            benefitFeeTypeNumber.toLong()+5 -> selectedFifthBeneficiaryFeeType = parent.getItemAtPosition(position).toString()

            benefitPayeeNumber.toLong()+1 -> selectedFirstBeneficiaryFeeAllocation = parent.getItemAtPosition(position).toString()
            benefitPayeeNumber.toLong()+2 -> selectedSecondBeneficiaryFeeAllocation = parent.getItemAtPosition(position).toString()
            benefitPayeeNumber.toLong()+3 -> selectedThirdBeneficiaryFeeAllocation = parent.getItemAtPosition(position).toString()
            benefitPayeeNumber.toLong()+4 -> selectedFourthBeneficiaryFeeAllocation = parent.getItemAtPosition(position).toString()
            benefitPayeeNumber.toLong()+5 -> selectedFifthBeneficiaryFeeAllocation = parent.getItemAtPosition(position).toString()

        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented")
    }
}
