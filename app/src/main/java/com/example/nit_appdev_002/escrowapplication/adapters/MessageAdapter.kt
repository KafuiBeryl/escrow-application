package com.example.nit_appdev_002.escrowapplication.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.nit_appdev_002.escrowapplication.entities.Message
import com.example.nit_appdev_002.escrowapplication.utilities.DateUtils
import kotlinx.android.synthetic.main.item_other_message.view.*
import kotlinx.android.synthetic.main.item_own_message.view.*

//class MessageAdapter (val context: Context) : RecyclerView.Adapter<MessageViewHolder>() {
//
//    inner class MyMessageViewHolder (view: View) : MessageViewHolder(view) {
//        private var messageText: TextView = view.txtMyMessage
//        private var timeText: TextView = view.txtMyMessageTime
//
//        override fun bind(message: Message) {
//            messageText.text = message.message
//            timeText.text = DateUtils.fromMillisToTimeString(message.time)
//        }
//    }
//
//    inner class OtherMessageViewHolder (view: View) : MessageViewHolder(view) {
//        private var messageText: TextView = view.txtOtherMessage
//        private var userText: TextView = view.txtOtherUser
//        private var timeText: TextView = view.txtOtherMessageTime
//
//        override fun bind(message: Message) {
//            messageText.text = message.message
//            userText.text = message.user
//            timeText.text = DateUtils.fromMillisToTimeString(message.time)
//        }
//    }
//}
//
//    open class MessageViewHolder (view: View) : RecyclerView.ViewHolder(view) {
//        open fun bind(message:Message) {}
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
//
//    }
//}