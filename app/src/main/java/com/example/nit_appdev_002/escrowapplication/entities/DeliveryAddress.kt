package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "delivery_address")
data class DeliveryAddress (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "id")
        @SerializedName("id")
        var id:Int?,

        @ColumnInfo(name = "trade_id")
        @SerializedName("trade_id")
        var tradeId:Int?,

        @ColumnInfo(name = "house_number")
        @SerializedName("house_number")
        var houseNumber:String,

        @ColumnInfo(name = "street_line_1")
        @SerializedName("street_line_1")
        var streetLine1:String,

        @ColumnInfo(name = "street_line_2")
        @SerializedName("street_line_2")
        var streetLine2: String?,

        @ColumnInfo(name = "suburb")
        @SerializedName("suburb")
        var suburb:String,

        @ColumnInfo(name = "town_or_city")
        @SerializedName("town_or_city")
        var townOrCity:String,

        @ColumnInfo(name = "country_id")
        @SerializedName("country_id")
        var countryId:Int,

        @ColumnInfo(name = "digital_address")
        @SerializedName("digital_address")
        var digitalAddress:String,

        @ColumnInfo(name = "comments")
        @SerializedName("comments")
        var comments:String,

        @ColumnInfo(name = "created_at")
        @SerializedName("created_at")
        var createdDate: Date?,

        @ColumnInfo(name = "updated_at")
        @SerializedName("updated_at")
        var updatedDate: Date?
)