package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.example.nit_appdev_002.escrowapplication.entities.Occupation
import com.example.nit_appdev_002.escrowapplication.entities.User
import com.google.gson.annotations.SerializedName

data class ProfileResult (
        @SerializedName("user")
        var user:User,
        @SerializedName("user_occupation")
        var userOccupation: Occupation?,
        @SerializedName("error")
        var error:String?
)