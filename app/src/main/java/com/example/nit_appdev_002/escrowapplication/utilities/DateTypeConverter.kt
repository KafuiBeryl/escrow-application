package com.example.nit_appdev_002.escrowapplication.utilities

import androidx.room.TypeConverter
import java.util.*

object DateTypeConverter {
    @TypeConverter
    @JvmStatic
    fun fromTimestamp(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    @JvmStatic
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }
}