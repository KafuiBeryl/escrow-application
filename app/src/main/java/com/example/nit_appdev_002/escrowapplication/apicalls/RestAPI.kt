package com.example.nit_appdev_002.escrowapplication.apicalls

import com.example.nit_appdev_002.escrowapplication.apiobjects.LoginRequest
import com.example.nit_appdev_002.escrowapplication.apiobjects.LoginResult
import com.example.nit_appdev_002.escrowapplication.apiobjects.RegisterResult
import com.example.nit_appdev_002.escrowapplication.utilities.BASE_URL
import com.google.gson.GsonBuilder
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type

class RestAPI {
    private val escrowApiService:EscrowApiService

    init {
        val nullOnEmptyConverterFactory = object : Converter.Factory() {
//            fun converterFactory() = this
            override fun responseBodyConverter(type: Type, annotations: Array<out Annotation>, retrofit: Retrofit): Converter<ResponseBody, *>? {
                val nextResponseBodyConverter = retrofit.nextResponseBodyConverter<Any>(this, type, annotations)
                return Converter<ResponseBody, Any> {
                    if (it.contentLength() != 0L) {
                        nextResponseBodyConverter.convert(it)
                    } else {
                        null
                    }
                }
            }
        }

        val gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .setLenient()
                .create()

        val retrofit = Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(nullOnEmptyConverterFactory)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

        escrowApiService = retrofit.create(EscrowApiService::class.java)
    }

    fun register(email:String, password:String, confirmPassword:String):Call<RegisterResult>{
        return escrowApiService.register(email, password, confirmPassword)
    }


    fun login(login:LoginRequest) : Call<LoginResult>{
        return escrowApiService.login(login)
    }

//    fun getMiscData(token:String):Call<HomeResult>{
//        return escrowApiService.getMiscInfo(token)
//    }
}