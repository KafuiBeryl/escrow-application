package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.example.nit_appdev_002.escrowapplication.entities.Trade
import com.example.nit_appdev_002.escrowapplication.entities.UserTradeRoles
import com.google.gson.annotations.SerializedName

data class CreateTradeResult (
        @SerializedName("trade_id")
        var tradeId:Int,
        @SerializedName("user_trades")
        var trades:List<Trade>,
        @SerializedName("user_trade_roles")
        var userTradeRoles:List<UserTradeRoles>,
        @SerializedName("error")
        var error:String?
)