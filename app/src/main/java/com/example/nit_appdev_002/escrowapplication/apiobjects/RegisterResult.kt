package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.example.nit_appdev_002.escrowapplication.entities.User
import com.example.nit_appdev_002.escrowapplication.entities.UserTradeRoles
import com.google.gson.annotations.SerializedName
import java.util.*

data class RegisterResult (
        @SerializedName("token")
        var token:String,
        @SerializedName("token_expiry_date")
        var tokenExpiryDate:Date,
        @SerializedName("user")
        var user:User?,
        @SerializedName("user_trade_roles")
        var userTradeRoles:List<UserTradeRoles>?,
        @SerializedName("error")
        var error:String?

)