package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.example.nit_appdev_002.escrowapplication.entities.*
import com.google.gson.annotations.SerializedName

data class HomeResult (
        @SerializedName("banks")
        var banks:List<Bank>,

        @SerializedName("industries")
        var industries:List<Industry>,

        @SerializedName("company_type")
        var companyCategories:List<CompanyCategory>,

        @SerializedName("feeSchedule")
        var feeSchedule: List<FeeSchedule>,

        @SerializedName("bankAccountTypes")
        var bankAccountTypes:List<BankAccountType>,

        @SerializedName("identificationTypes")
        var identificationTypes: List<IdentificationType>,

        @SerializedName("feeType")
        var feeTypes: List<FeeType>,

        @SerializedName("standardFeeAllocation")
        var standardFeeAllocation: List<StandardFeeAllocation>,

        @SerializedName("adminFeeAllocation")
        var adminFeeAllocation: List<AdminFeeAllocation>,

        @SerializedName("beneficiaryType")
        var beneficiaryType: List<BeneficiaryType>,

        @SerializedName("occupations")
        var occupations: List<Occupation>,

        @SerializedName("error")
        var error:String?
)