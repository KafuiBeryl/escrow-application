package com.example.nit_appdev_002.escrowapplication.fragments.payment

import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.apicalls.OtherNetworkCalls
import com.example.nit_appdev_002.escrowapplication.apicalls.RetrofitInstance
import com.example.nit_appdev_002.escrowapplication.apiobjects.BankCardRequest
import com.example.nit_appdev_002.escrowapplication.apiobjects.PaymentResult
import com.example.nit_appdev_002.escrowapplication.utilities.*
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.viewmodels.TransactionViewModel
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.fragment_card_payment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [CardPaymentFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 *
 */
class CardPaymentFragment : Fragment(), PaymentTypeFragment.OnFragmentInteractionListener {

    companion object {
        @JvmStatic
        private var selectedCardType:String? = null

        @JvmStatic
        private lateinit var mTransactionViewModel: TransactionViewModel
    }

    private var currentTradeId:Int? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_card_payment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //initialize the view model
        mTransactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel::class.java)

        val prefs = PreferenceHelper.defaultPrefs(activity!!)
        currentTradeId = prefs[PREF_TRADE_SELECTED]

        mTransactionViewModel.getSingleTrade(currentTradeId!!).observe(this, Observer {
            trade ->
            if (trade != null){
                val amount = trade.tradeAmount
                val amountString = GeneralHelper.formatAmountToString(amount)
                val finalString = "Amount being paid is $amountString"
                txt_trade_amount.text = finalString
            }
        })

        spinner_card_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedCardType = parent?.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedCardType = null
            }
        }

        val adapter = ArrayAdapter.createFromResource(activity, R.array.card_array, R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        spinner_card_type.adapter = adapter

        btn_submit.setOnClickListener {
            val linearLayout = it.parent as ViewGroup
            if (validateEditTexts(linearLayout)){
                sendPayment()
            }else{
                Toast.makeText(activity, R.string.trade_empty_text, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun validateEditTexts(viewGroup: ViewGroup):Boolean{
        var validate = false
        val emptyText = Validator.traverseEditTexts(viewGroup)
        if (emptyText == null){
            validate = true
        }
        return validate
    }

    private fun passVariables():BankCardRequest{
        val description = "Card payment for trade with id $currentTradeId"
        val bankCardRequest = BankCardRequest(currentTradeId.toString(),
                description, txt_card_number.text.toString(), selectedCardType!!,
                txt_cvv_number.text.toString(), txt_expiry_month.text.toString(),
                txt_expiry_year.text.toString())
        return bankCardRequest
    }

    private fun sendPayment(){
        val prefs = PreferenceHelper.defaultPrefs(activity!!)
        val token:String? = prefs[PREF_TOKEN]
        if (token != null){
            val service: OtherNetworkCalls = RetrofitInstance.getRetrofitInstance()!!.create(OtherNetworkCalls::class.java)
            val tokenString = "Bearer $token"
            val request = passVariables()
            val replyIntent = Intent()
            val call = service.postSendBankCard(tokenString, request)
            call.enqueue(object : Callback<PaymentResult>{
                override fun onResponse(call: Call<PaymentResult>, response: Response<PaymentResult>) {
                    if (response.body() != null){
                        val result = response.body() as PaymentResult
                        replyIntent.putExtra(INTENT_PAYMENT_REPLY, result.message)
                        activity?.setResult(RESULT_OK, replyIntent)
                    }else if (response.errorBody() != null){
                        val gson = GsonBuilder().create()
                        val errorResult: PaymentResult = gson.fromJson<PaymentResult>(response.errorBody()?.string(), PaymentResult::class.java)
                        replyIntent.putExtra(INTENT_PAYMENT_REPLY, errorResult.error)
                        activity?.setResult(RESULT_CANCELED, replyIntent)
                    }
                    activity?.finish()
                }

                override fun onFailure(call: Call<PaymentResult>, t: Throwable) {
                    if (t.message != null){
                        replyIntent.putExtra(INTENT_PAYMENT_REPLY, t.message.toString())
                        activity?.setResult(RESULT_CANCELED, replyIntent)
                    }
                    activity?.finish()
                }
            })
        }
    }

    override fun onNextPressed(fragment: Fragment, selectedMode: String) {
        val selected = selectedMode
    }
}
