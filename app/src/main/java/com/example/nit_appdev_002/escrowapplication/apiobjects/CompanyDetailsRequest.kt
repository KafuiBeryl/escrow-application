package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.google.gson.annotations.SerializedName

data class CompanyDetailsRequest (
        @SerializedName("company_name")
        var companyName:String,

        @SerializedName("tax_identification_number")
        var taxIN:String,

        @SerializedName("company_registration")
        var companyRegistration:String,

        @SerializedName("company")
        var companyCategory:String
)