package com.example.nit_appdev_002.escrowapplication.fragments.payment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.apicalls.OtherNetworkCalls
import com.example.nit_appdev_002.escrowapplication.apicalls.RetrofitInstance
import com.example.nit_appdev_002.escrowapplication.apiobjects.MobileMoneyRequest
import com.example.nit_appdev_002.escrowapplication.apiobjects.PaymentResult
import com.example.nit_appdev_002.escrowapplication.apiobjects.VodafoneCashRequest
import com.example.nit_appdev_002.escrowapplication.utilities.*
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.viewmodels.TransactionViewModel
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.fragment_mobile_money.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MobileMoneyFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 *
 */
class MobileMoneyFragment : Fragment(), PaymentTypeFragment.OnFragmentInteractionListener {

    companion object {
        @JvmStatic
        private lateinit var selectedNetwork:String

        @JvmStatic
        private lateinit var mTransactionViewModel: TransactionViewModel
    }
    private var currentTradeId:Int? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mobile_money, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txt_voucher_number.visibility = View.GONE
        txt_voucher_text.visibility = View.GONE

        //initialize the view model
        mTransactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel::class.java)

        val prefs = PreferenceHelper.defaultPrefs(activity!!)
        currentTradeId = prefs[PREF_TRADE_SELECTED]

        mTransactionViewModel.getSingleTrade(currentTradeId!!).observe(this, Observer {
            trade ->
            if (trade != null){
                val amount = trade.tradeAmount
                val amountString = GeneralHelper.formatAmountToString(amount)
                val finalString = "Amount being paid is $amountString"
                txt_trade_amount.text = finalString
            }
        })

        btn_submit.setOnClickListener {
            val linearLayout = it.parent as ViewGroup
            if (validateEditTexts(linearLayout)){
                val token:String? = prefs[PREF_TOKEN]
                if (token != null){
                    val tokenString = "Bearer $token"
                    when (selectedNetwork){
                        "Vodafone" -> {
                            sendVodafoneCashRequest(tokenString)
                        }
                        else -> {
                            sendMobileMoneyRequest(tokenString)
                        }
                    }
                }
            }else{
                Toast.makeText(activity, R.string.trade_empty_text, Toast.LENGTH_LONG).show()
            }
        }
    }


    override fun onResume() {
        super.onResume()
        when(selectedNetwork){
            "Vodafone" -> {
                txt_voucher_number.visibility = View.VISIBLE
                txt_voucher_text.visibility = View.VISIBLE
            }
            else -> {
                txt_voucher_number.visibility = View.GONE
                txt_voucher_text.visibility = View.GONE
            }
        }
    }

    private fun passVodafoneVariables():VodafoneCashRequest{
        val description = "Vodafone cash payment for trade with id $currentTradeId"
        val vodaRequest = VodafoneCashRequest(currentTradeId.toString(), description,
                txt_subscriber_number.text.toString(), selectedNetwork, txt_voucher_number.text.toString())
        return vodaRequest
    }

    private fun passMobileMoneyVariables():MobileMoneyRequest{
        val description = "Vodafone cash payment for trade with id $currentTradeId"
        val vodaRequest = MobileMoneyRequest(currentTradeId.toString(), description,
                txt_subscriber_number.text.toString(), selectedNetwork)
        return vodaRequest
    }

    private fun sendVodafoneCashRequest(token:String){
        val service: OtherNetworkCalls = RetrofitInstance.getRetrofitInstance()!!.create(OtherNetworkCalls::class.java)
        val request = passVodafoneVariables()
        val replyIntent = Intent()
        val call = service.postSendVodafoneCash(token, request)
        call.enqueue(object : Callback<PaymentResult>{
            override fun onResponse(call: Call<PaymentResult>, response: Response<PaymentResult>) {
                if (response.body() != null){
                    val result = response.body() as PaymentResult
                    replyIntent.putExtra(INTENT_PAYMENT_REPLY, result.message)
                    activity?.setResult(Activity.RESULT_OK, replyIntent)
                }else if (response.errorBody() != null){
                    val gson = GsonBuilder().create()
                    val errorResult: PaymentResult = gson.fromJson<PaymentResult>(response.errorBody()?.string(), PaymentResult::class.java)
                    replyIntent.putExtra(INTENT_PAYMENT_REPLY, errorResult.error)
                    activity?.setResult(Activity.RESULT_CANCELED, replyIntent)
                }
                activity?.finish()
            }

            override fun onFailure(call: Call<PaymentResult>, t: Throwable) {
                if (t.message != null){
                    replyIntent.putExtra(INTENT_PAYMENT_REPLY, t.message.toString())
                    activity?.setResult(Activity.RESULT_CANCELED, replyIntent)
                }
                activity?.finish()
            }
        })
    }

    private fun sendMobileMoneyRequest(token:String){
        val service: OtherNetworkCalls = RetrofitInstance.getRetrofitInstance()!!.create(OtherNetworkCalls::class.java)
        val request = passMobileMoneyVariables()
        val replyIntent = Intent()
        val call = service.postSendMobileMoney(token, request)
        call.enqueue(object : Callback<PaymentResult>{
            override fun onResponse(call: Call<PaymentResult>, response: Response<PaymentResult>) {
                if (response.body() != null){
                    val result = response.body() as PaymentResult
                    replyIntent.putExtra(INTENT_PAYMENT_REPLY, result.message)
                    activity?.setResult(Activity.RESULT_OK, replyIntent)
                }else if (response.errorBody() != null){
                    val gson = GsonBuilder().create()
                    val errorResult: PaymentResult = gson.fromJson<PaymentResult>(response.errorBody()?.string(), PaymentResult::class.java)
                    replyIntent.putExtra(INTENT_PAYMENT_REPLY, errorResult.error)
                    activity?.setResult(Activity.RESULT_CANCELED, replyIntent)
                }
                activity?.finish()
            }

            override fun onFailure(call: Call<PaymentResult>, t: Throwable) {
                if (t.message != null){
                    replyIntent.putExtra(INTENT_PAYMENT_REPLY, t.message.toString())
                    activity?.setResult(Activity.RESULT_CANCELED, replyIntent)
                }
                activity?.finish()
            }
        })

    }

    private fun validateEditTexts(viewGroup: ViewGroup):Boolean{
        var validate = false
        val emptyText = Validator.traverseEditTexts(viewGroup)
        if (emptyText == null){
            validate = true
        }
        return validate
    }

    override fun onNextPressed(fragment: Fragment, selectedMode: String) {
        when (fragment){
            is PaymentTypeFragment -> {
                selectedNetwork = selectedMode
            }
        }
    }
}
