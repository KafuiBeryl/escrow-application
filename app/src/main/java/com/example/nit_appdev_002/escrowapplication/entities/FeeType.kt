package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "fee_type")
data class FeeType (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "fee_type_id")
        @SerializedName("id")
        var feeTypeId:Int,

        @ColumnInfo(name = "fee_type")
        @SerializedName("fee_type")
        var feeType:String
)