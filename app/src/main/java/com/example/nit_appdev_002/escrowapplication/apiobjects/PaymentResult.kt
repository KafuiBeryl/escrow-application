package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.google.gson.annotations.SerializedName

data class PaymentResult (
        @SerializedName("message")
        var message:String?,
        @SerializedName("error")
        var error:String?
)