package com.example.nit_appdev_002.escrowapplication.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.nit_appdev_002.escrowapplication.entities.UserTradeRoles

@Dao
interface UserTradeRoleDao {
    @Insert(onConflict = REPLACE)
    fun insert(role: UserTradeRoles)

    @Query("select count(id) from user_trade_roles")
    fun countUserTradeRoles():LiveData<Int>

    @Query("select * from user_trade_roles order by id asc")
    fun getAllUserTradeRoles(): LiveData<List<UserTradeRoles>>

    @Query("delete from user")
    fun nukeTable()
}