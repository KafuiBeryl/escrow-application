package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.google.gson.annotations.SerializedName

data class CreateTradeRequest (
        @SerializedName("user_role_name")
        var userRoleName:String?,
        @SerializedName("seller_email")
        var sellerEmail:String?,
        @SerializedName("buyer_email")
        var buyerEmail:String?,
        @SerializedName("trade_name")
        var tradeName:String?,
        @SerializedName("description")
        var description:String?,
        @SerializedName("trade_amount")
        var tradeAmount:Long = 0,
        @SerializedName("delivery_time")
        var deliveryTime:String?,
        @SerializedName("inspection_period")
        var inspectionPeriod:String?,
        @SerializedName("isDelivery")
        var isDelivery:Boolean,
        @SerializedName("is_progress_payment")
        var isProgressPayment:Boolean,
        @SerializedName("hasBeneficiary")
        var hasBeneficiary:Boolean,
        @SerializedName("isConfidential")
        var isConfidential:Boolean,
        @SerializedName("hasDocuments")
        var hasDocuments:Boolean,
        @SerializedName("hasNotifications")
        var hasNotifications:Boolean,
        @SerializedName("escrow_fee")
        var escrowFee:Long = 0,
        @SerializedName("escrow_allocation")
        var escrowAllocation:String?,
        @SerializedName("agent_email")
        var agentEmail:String?,
        @SerializedName("agent_pay_type")
        var agentFeeType:String?,
        @SerializedName("agent_allocation")
        var agentFeeAllocation:String?,
        @SerializedName("pre_agent_fee")
        var unCalculatedAgentFee:Long = 0,
        @SerializedName("agent_fee")
        var agentFee:Long = 0,
        @SerializedName("buyer_fee")
        var buyerFee:Long = 0,
        @SerializedName("seller_fee")
        var sellerFee:Long = 0,
        @SerializedName("currency_used")
        var currencyUsed:String?,
        @SerializedName("current_trade_category")
        var tradeCategory:String?
)