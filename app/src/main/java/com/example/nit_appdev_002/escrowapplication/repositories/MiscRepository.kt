package com.example.nit_appdev_002.escrowapplication.repositories

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.nit_appdev_002.escrowapplication.dao.MiscDao
import com.example.nit_appdev_002.escrowapplication.database.EscrowRoomDatabase
import com.example.nit_appdev_002.escrowapplication.entities.*

class MiscRepository(application: Application) {
    private var  miscDao: MiscDao

    init {
        val db = EscrowRoomDatabase.getDatabase(application)!!
        miscDao = db.miscDao()
    }

    fun insertBanks(banks:List<Bank>){
        InsertBanksAsync(miscDao).execute(banks)
    }

    fun insertCompanyCategories(companyCategories: List<CompanyCategory>){
        InsertCompanyCategoriesAsync(miscDao).execute(companyCategories)
    }

    fun insertIndustries(industries:List<Industry>){
        InsertIndustriesAsync(miscDao).execute(industries)
    }

    fun insertFeeSchedule(feeSchedule: List<FeeSchedule>){
        InsertFeeScheduleAsync(miscDao).execute(feeSchedule)
    }

    fun insertIdentityTypes(types:List<IdentificationType>){
        InsertIdentityTypeAsync(miscDao).execute(types)
    }

    fun insertBankAccountTypes(types:List<BankAccountType>){
        InsertAccountTypeAsync(miscDao).execute(types)
    }

    fun insertAdminFeeAllocation(types:List<AdminFeeAllocation>){
        InsertAdminFeeAllocationAsync(miscDao).execute(types)
    }

    fun insertStandardFeeAllocation(types:List<StandardFeeAllocation>){
        InsertStandardFeeAllocationAsync(miscDao).execute(types)
    }

    fun insertFeeType(types:List<FeeType>){
        InsertFeeTypeAsync(miscDao).execute(types)
    }

    fun insertBeneficiaryType(types:List<BeneficiaryType>){
        InsertBeneficiaryTypeAsync(miscDao).execute(types)
    }

    fun insertOccupation(types:List<Occupation>){
        InsertOccupationsAsync(miscDao).execute(types)
    }

    fun getBanks(): LiveData<List<String>> {
        return miscDao.getBankNames()
    }

    fun getFeeSchedule(): LiveData<List<FeeSchedule>>{
        return miscDao.getFeeSchedule()
    }

    fun getCompanyCategories(): LiveData<List<String>>{
        return  miscDao.getCompanyCategories()
    }

    fun getIndustries(): LiveData<List<String>>{
        return miscDao.getIndustries()
    }

    fun getBankAccountTypes(): LiveData<List<String>>{
        return  miscDao.getBankAccountTypes()
    }

    fun getIdentificationTypes(): LiveData<List<String>>{
        return miscDao.getIdentificationTypes()
    }

    fun getAgentAdminFeeAllocation(): LiveData<List<String>>{
        return miscDao.getAdminFeeAllocationAgent()
    }

    fun getAdminFeeAllocation(): LiveData<List<String>>{
        return  miscDao.getAdminFeeAllocation()
    }

    fun getStandardFeeAllocation(): LiveData<List<String>>{
        return miscDao.getStandardFeeAllocation()
    }

    fun getFeeType(): LiveData<List<String>>{
        return miscDao.getFeeType()
    }

    fun getBeneficiaryType(): LiveData<List<String>>{
        return miscDao.getBeneficiaryType()
    }

    fun getOccupations(): LiveData<List<String>>{
        return miscDao.getOccupations()
    }

    fun logOut(){
        LogoutMiscAsync(miscDao).execute()
    }

    class LogoutMiscAsync(dao: MiscDao): AsyncTask<Void, Void, Void>(){
        private var miscDao:MiscDao
        init {
            miscDao = dao
        }
        override fun doInBackground(vararg p0: Void?): Void? {
            miscDao.nukeBanksTable()
            miscDao.nukeCompanyCategoryTable()
            miscDao.nukeFeeScheduleTable()
            miscDao.nukeIndustryTable()
            miscDao.nukeIdentificationTypeTable()
            miscDao.nukeBankAccountTypeTable()
            miscDao.nukeStandardAllocationTable()
            miscDao.nukeAdminAllocationTable()
            miscDao.nukeFeeTypeTable()
            miscDao.nukeBeneficiaryTypeTable()
            miscDao.nukeOccupationTable()
            return null
        }
    }

    class InsertOccupationsAsync(dao:MiscDao): AsyncTask<List<Occupation>, Void, Void>(){
        private var miscDao:MiscDao
        init {
            miscDao = dao
        }

        override fun doInBackground(vararg params: List<Occupation>): Void? {
            for(occupation in params[0]){
                miscDao.insertOccupations(occupation)
            }
            return null
        }
    }

    class InsertBanksAsync(dao:MiscDao): AsyncTask<List<Bank>, Void, Void>(){
        private var miscDao:MiscDao
        init {
            miscDao = dao
        }

        override fun doInBackground(vararg params: List<Bank>): Void? {
            for(bank in params[0]){
                miscDao.insertBanks(bank)
            }
            return null
        }
    }

    class InsertCompanyCategoriesAsync(dao:MiscDao): AsyncTask<List<CompanyCategory>, Void, Void>(){
        private var miscDao:MiscDao
        init {
            miscDao = dao
        }

        override fun doInBackground(vararg params: List<CompanyCategory>): Void? {
            for(category in params[0]){
                miscDao.insertCompanyCats(category)
            }
            return null
        }
    }

    class InsertIndustriesAsync(dao:MiscDao): AsyncTask<List<Industry>, Void, Void>(){
        private var miscDao:MiscDao
        init {
            miscDao = dao
        }

        override fun doInBackground(vararg params: List<Industry>): Void? {
            for(industry in params[0]){
                miscDao.insertIndustries(industry)
            }
            return null
        }
    }

    class InsertFeeScheduleAsync(dao:MiscDao): AsyncTask<List<FeeSchedule>, Void, Void>(){
        private var miscDao:MiscDao
        init {
            miscDao = dao
        }

        override fun doInBackground(vararg params: List<FeeSchedule>): Void? {
            for(feeSchedule in params[0]){
                miscDao.insertFeeSchedule(feeSchedule)
            }
            return null
        }
    }

    class InsertIdentityTypeAsync(dao:MiscDao): AsyncTask<List<IdentificationType>, Void, Void>(){
        private var miscDao:MiscDao
        init {
            miscDao = dao
        }

        override fun doInBackground(vararg params: List<IdentificationType>): Void? {
            for(identificationType in params[0]){
                miscDao.insertIdentificationTypes(identificationType)
            }
            return null
        }
    }

    class InsertAccountTypeAsync(dao:MiscDao): AsyncTask<List<BankAccountType>, Void, Void>(){
        private var miscDao:MiscDao
        init {
            miscDao = dao
        }

        override fun doInBackground(vararg params: List<BankAccountType>): Void? {
            for(bankAccountType in params[0]){
                miscDao.insertBankAccountTypes(bankAccountType)
            }
            return null
        }
    }

    class InsertAdminFeeAllocationAsync(dao:MiscDao): AsyncTask<List<AdminFeeAllocation>, Void, Void>(){
        private var miscDao:MiscDao
        init {
            miscDao = dao
        }

        override fun doInBackground(vararg params: List<AdminFeeAllocation>): Void? {
            for(adminFeeAllocation in params[0]){
                miscDao.insertAdminFeeAllocation(adminFeeAllocation)
            }
            return null
        }
    }

    class InsertStandardFeeAllocationAsync(dao:MiscDao): AsyncTask<List<StandardFeeAllocation>, Void, Void>(){
        private var miscDao:MiscDao
        init {
            miscDao = dao
        }

        override fun doInBackground(vararg params: List<StandardFeeAllocation>): Void? {
            for(standardFeeAllocation in params[0]){
                miscDao.insertStandardFeeAllocation(standardFeeAllocation)
            }
            return null
        }
    }

    class InsertFeeTypeAsync(dao:MiscDao): AsyncTask<List<FeeType>, Void, Void>(){
        private var miscDao:MiscDao
        init {
            miscDao = dao
        }

        override fun doInBackground(vararg params: List<FeeType>): Void? {
            for(feeType in params[0]){
                miscDao.insertFeeType(feeType)
            }
            return null
        }
    }

    class InsertBeneficiaryTypeAsync(dao:MiscDao): AsyncTask<List<BeneficiaryType>, Void, Void>(){
        private var miscDao:MiscDao
        init {
            miscDao = dao
        }

        override fun doInBackground(vararg params: List<BeneficiaryType>): Void? {
            for(beneficiaryType in params[0]){
                miscDao.insertBeneficiaryType(beneficiaryType)
            }
            return null
        }
    }
}