package com.example.nit_appdev_002.escrowapplication.apicalls

import com.example.nit_appdev_002.escrowapplication.apiobjects.*
import com.example.nit_appdev_002.escrowapplication.entities.Message
import com.example.nit_appdev_002.escrowapplication.utilities.*
import retrofit2.Call
import retrofit2.http.*

interface OtherNetworkCalls {

    @POST(API_LOGIN)
    fun login(@Body login: LoginRequest) : Call<LoginResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET(API_GET_MISC)
    fun getMiscInfo(@Header("Authorization") token:String) : Call<HomeResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_SEND_PERSONAL_DETAILS)
    fun postPersonalDetails(@Header("Authorization") token:String, @Body personalDetailsRequest: PersonalDetailsRequest) : Call<ProfileResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_SEND_PERSONAL_DETAILS)
    fun postBankingDetails(@Header("Authorization") token:String, @Body bankingDetailsRequest: BankingDetailsRequest) : Call<ProfileResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_SEND_PERSONAL_DETAILS)
    fun postBillingDetails(@Header("Authorization") token:String, @Body billingDetailsRequest: BillingDetailsRequest) : Call<ProfileResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_SEND_PERSONAL_DETAILS)
    fun postCompanyDetails(@Header("Authorization") token:String, @Body companyDetailsRequest: CompanyDetailsRequest) : Call<ProfileResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_START_BASIC_TRADE)
    fun postInitializeTrade(@Header("Authorization") token:String, @Body createTradeRequest: CreateTradeRequest) : Call<CreateTradeResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_EDIT_TRADE)
    fun postEditTrade(@Header("Authorization") token:String, @Body editTradeRequest: EditTradeRequest) : Call<EditTradeResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_ADD_TRADE_EXTRAS)
    fun postAddTradeExtras(@Header("Authorization") token:String, @Body createTradeExtrasRequest: CreateTradeExtrasRequest) : Call<CreateTradeExtrasResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_EDIT_TRADE_EXTRAS)
    fun postEditTradeExtras(@Header("Authorization") token:String, @Body createTradeExtrasRequest: CreateTradeExtrasRequest) : Call<CreateTradeExtrasResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_PAY_VIA_MOBILE_MONEY)
    fun postSendVodafoneCash(@Header("Authorization") token:String, @Body vodafoneCashRequest: VodafoneCashRequest) : Call<PaymentResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_PAY_VIA_MOBILE_MONEY)
    fun postSendMobileMoney(@Header("Authorization") token:String, @Body mobileMoneyRequest: MobileMoneyRequest) : Call<PaymentResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_PAY_VIA_BANK_CARD)
    fun postSendBankCard(@Header("Authorization") token:String, @Body bankCardRequest: BankCardRequest) : Call<PaymentResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_GET_SINGLE_TRADE)
    fun getTradeById(@Header("Authorization") token:String, @Body getSingleTradeRequest: GetSingleTradeRequest) : Call<TradeResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_GET_SINGLE_TRADE_EXTRAS)
    fun getTradeExtras(@Header("Authorization") token:String, @Body getSingleTradeRequest: GetSingleTradeRequest) : Call<CreateTradeExtrasResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_CHAT_SEND_MESSAGE)
    fun postMessage(@Header("Authorization") token:String, @Body body: Message): Call<Void>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_UPLOAD_IMAGE)
    fun postUploadImage(@Header("Authorization") token:String, @Body uploadImageRequest: UploadImageRequest): Call<UploadImageResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_SEND_TRADE_INVITATION)
    fun postSendTradeInvitation(@Header("Authorization") token:String, @Body invitationRequest: InvitationRequest): Call<InvitationResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST(API_SEND_USER_TRADE_DECISION)
    fun postSendUserTradeDecision(@Header("Authorization") token:String, @Body decisionRequest: DecisionRequest): Call<DecisionResult>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET(API_GET_USER_INVOLVED_TRADES)
    fun getUserInvolvedTrades(@Header("Authorization") token:String) : Call<GetInvolvedTradesResult>
}