package com.example.nit_appdev_002.escrowapplication.apiobjects

import com.google.gson.annotations.SerializedName

data class BankingDetailsRequest (
        @SerializedName("bank_name")
        var bankName:String,

        @SerializedName("account_number")
        var accountNumber:String,

        @SerializedName("account_name")
        var accountName:String,

        @SerializedName("bank_branch")
        var bankBranch:String,

        @SerializedName("account")
        var accountType:String
)