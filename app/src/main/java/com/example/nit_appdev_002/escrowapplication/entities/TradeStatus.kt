package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "trade_status")
data class TradeStatus (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "tradeStatusId")
        @SerializedName("id")
        var tradeStatusId:Int,

        @ColumnInfo(name = "trade_status")
        @SerializedName("trade_status")
        var tradeStatus:String
)