package com.example.nit_appdev_002.escrowapplication.activities.chat

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nit_appdev_002.escrowapplication.R
import com.example.nit_appdev_002.escrowapplication.apicalls.OtherNetworkCalls
import com.example.nit_appdev_002.escrowapplication.apicalls.RetrofitInstance
import com.example.nit_appdev_002.escrowapplication.entities.Message
import com.example.nit_appdev_002.escrowapplication.entities.User
import com.example.nit_appdev_002.escrowapplication.utilities.*
import com.example.nit_appdev_002.escrowapplication.utilities.PreferenceHelper.get
import com.example.nit_appdev_002.escrowapplication.viewmodels.UserViewModel
import com.google.gson.GsonBuilder
import com.pusher.client.Pusher
import com.pusher.client.PusherOptions
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Section
import com.xwray.groupie.ViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.content_chat_landing.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ChatLandingActivity : AppCompatActivity() {
    private lateinit var user : User
    private lateinit var currentChannelId: String
    private lateinit var currentEventId: String
    private var token: String? = null
    private var shouldInitRecyclerView = true
    private lateinit var messageSection: Section
    private lateinit var mUserViewModel: UserViewModel
    private val adapter = GroupAdapter<ViewHolder>()
//    private lateinit var mTransactionViewModel: TransactionViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_landing)
        setSupportActionBar(custom_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        //instantiate ViewModel object
        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        val prefs = PreferenceHelper.defaultPrefs(this)
        val userId:Int? = prefs[PREF_USER_ID]
        val currentTradeId:Int? = prefs[PREF_TRADE_SELECTED]
        token = prefs[PREF_TOKEN]
        mUserViewModel.getUser(userId!!).observe(this, Observer {
            user = it
        })
        currentChannelId = "$currentTradeId"+"trade_negotiation_chat"
        currentEventId = "new_message"

        recycler_view_messages.adapter = adapter

        imageView_send.setOnClickListener {
            if (editText_message.text!!.isNotEmpty()){
                sendMessage()
            }else{
                Toast.makeText(this, "Message should not be empty", Toast.LENGTH_SHORT).show()
            }
        }

        fab_send_image.setOnClickListener {

        }
        setUpPusher()
    }

    private fun sendMessage(){
        val message = Message(
                "${user.firstName} ${user.lastName}",
                editText_message.text.toString(),
                null,
                Calendar.getInstance().get(Calendar.MILLISECOND).toLong()
        )

        val service = RetrofitInstance.getRetrofitInstance()!!.create(OtherNetworkCalls::class.java)
        val tokenString = "Bearer $token"
        val call = service.postMessage(tokenString, message)
        call.enqueue(object : Callback<Void>{
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                resetInput()
                if (!response.isSuccessful){
                    Log.e(ERRORTAG, response.code().toString())
                    Toast.makeText(applicationContext,"Response was not successful", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                resetInput()
                Log.e(ERRORTAG, t.toString())
                Toast.makeText(applicationContext,"Error when calling the service", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun resetInput(){
        // Clean text box
        editText_message.text?.clear()

        // Hide keyboard
        val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    //Finally, you need to set up a Pusher instance to listen for messages and add them to the RecyclerView when one is received
    private fun setUpPusher(){
        val options = PusherOptions()
        options.setCluster(PUSHER_CLUSTER)
        val pusher = Pusher(PUSHER_APP_KEY, options)
        doAsync {
            val channel = pusher.subscribe(currentChannelId)
            channel.bind(currentEventId){channelName, eventName, data ->
                val gSon = GsonBuilder().create()
                val messageReceived = gSon.fromJson<Message>(data, Message::class.java)
                val messages = mutableListOf<Message>()
                messages.add(messageReceived)
                uiThread {
                    PusherUtil.addChatMessagesListener(it, it::updateRecyclerView,
                            messages, user)
                }
            }
        }
        pusher.connect()
    }

    private fun updateRecyclerView(messages: List<Item>){
        fun init(){
//            recycler_view_messages.apply {
//                layoutManager = LinearLayoutManager(this@ChatLandingActivity)
//                adapter = this@ChatLandingActivity.adapter.apply {
//                    messageSection = Section(messages)
//                    this.add(messageSection)
//                }
//            }
            messageSection = Section(messages)
            adapter.add(messageSection)
            adapter.notifyDataSetChanged()
            shouldInitRecyclerView = false
        }

        fun updateItems() = messageSection.update(messages)

        if (shouldInitRecyclerView){
            init()
        }else{
            updateItems()
        }
        recycler_view_messages.scrollToPosition(recycler_view_messages.adapter!!.itemCount - 1)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
