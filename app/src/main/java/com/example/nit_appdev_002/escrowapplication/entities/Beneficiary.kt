package com.example.nit_appdev_002.escrowapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "beneficiary")
data class Beneficiary (
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "id")
        @SerializedName("id")
        var id:Int,

        @ColumnInfo(name = "email")
        @SerializedName("email")
        var email:String,

        @ColumnInfo(name = "trade_id")
        @SerializedName("trade_id")
        var trade_id:Int,

//        @ColumnInfo(name = "beneficiary_type")
        @SerializedName("beneficiary_type")
        @Embedded
        var beneficiaryType:BeneficiaryType,

//        @ColumnInfo(name = "step_number")
        @SerializedName("fee_type")
        @Embedded
        var beneficiaryFeeType: FeeType,

        @ColumnInfo(name = "pre_beneficiary_fee")
        @SerializedName("pre_beneficiary_fee")
        var unCalculatedBeneficiaryFee:Long,

        @ColumnInfo(name = "beneficiary_fee")
        @SerializedName("beneficiary_fee")
        var beneficiaryFee:Long,

//        @ColumnInfo(name = "beneficiary_payee")
        @SerializedName("standard_payee_allocation")
        @Embedded
        var beneficiaryPayee:StandardFeeAllocation,

        @ColumnInfo(name = "created_at")
        @SerializedName("created_at")
        var createdDate: Date,

        @ColumnInfo(name = "updated_at")
        @SerializedName("updated_at")
        var updatedDate: Date
)